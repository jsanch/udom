#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
Integration test for the 'neo_hookean_model' node.

"""

import rospy
import unittest
import rostest
import std_msgs.msg
import udom_modeling_msgs.msg

PKG = 'udom_deformation_modeling'


class TestNeoHookeanModel(unittest.TestCase):
    def setUp(self):
        """
        Sets up the test fixture before exercising it.

        """
        # params
        self.result_mesh = None
        self.wait_for_result_mesh = None
        self.result_energy = None
        self.wait_for_result_energy = None

        # publishers
        self.event_out = rospy.Publisher(
            '~event_out', std_msgs.msg.String, latch=True)
        self.force_info = rospy.Publisher(
            '~force_info', std_msgs.msg.Float32MultiArray, queue_size=1)

        # subscribers
        self.mesh_output = rospy.Subscriber(
            '~mesh_output', udom_modeling_msgs.msg.Mesh, self.result_callback)
        self.energy_output = rospy.Subscriber(
            '~energy_output', std_msgs.msg.Float32, self.result_energy_cb)

    def tearDown(self):
        """
        Deconstructs the test fixture after testing it.

        """
        self.mesh_output.unregister()
        self.energy_output.unregister()
        self.event_out.unregister()
        self.force_info.unregister()

    def test_deformation_model_node(self):
        """
        Verifies the node's interface is correct.
        Note: this is not a functionality test.

        """
        force_info = std_msgs.msg.Float32MultiArray()
        force_info.data = [
            1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        while not (self.wait_for_result_mesh and self.wait_for_result_energy):
            self.force_info.publish(force_info)
            self.event_out.publish('e_start')

        self.assertIsInstance(self.result_mesh, udom_modeling_msgs.msg.Mesh)
        self.assertIsInstance(self.result_energy, std_msgs.msg.Float32)

    def result_callback(self, msg):
        self.result_mesh = msg
        self.wait_for_result_mesh = True

    def result_energy_cb(self, msg):
        self.result_energy = msg
        self.wait_for_result_energy = True


if __name__ == '__main__':
    rospy.init_node('neo_hookean_model_test')
    rostest.rosrun(PKG, 'neo_hookean_model_test', TestNeoHookeanModel)
