/*
 * Copyright 2016 Université Clermont Auvergne (UCA)
 *
 * Author: Jose Sanchez
 *
 */

#include <string>
#include <vector>
#include <set>
#include <udom_deformation_modeling/isotropic_hyperelastic_fem_ros.h>


IsotropicHyperelasticFemRos::IsotropicHyperelasticFemRos(
    const std::string &input_filename, const std::vector<int> &constrained_nodes,
    double timestep, double damping_mass, double damping_stiffness) :
    timestep_(timestep), damping_mass_(damping_mass), damping_stiffness_(damping_stiffness)
{
    volumetric_mesh_ = std::unique_ptr<VolumetricMesh>(VolumetricMeshLoader::load(
        input_filename.c_str()));

    if (volumetric_mesh_ == NULL)
        throw std::runtime_error("Failed to load mesh.");

    TetMesh *tet_mesh_;
    if (volumetric_mesh_->getElementType() == VolumetricMesh::TET) {
        tet_mesh_ = static_cast<TetMesh*>(volumetric_mesh_.get());
    }
    else
    {
        throw std::runtime_error("Not a tet mesh.");
    }

    degrees_of_freedom_ =  3 * tet_mesh_->getNumVertices();
    constrained_nodes_ = constrained_nodes;

    // Get mesh information.
    volumetric_mesh_->exportMeshGeometry(
        &num_vertices, &vertices, &num_elements, &elements_per_vertex, &elements);

    // Create material
    NeoHookeanIsotropicMaterial *isotropic_material_ = new
        NeoHookeanIsotropicMaterial(tet_mesh_);

    // Deformable model and force model.
    // TODO: Add option for gravity
    IsotropicHyperelasticFEM *deformable_model_ = new
        IsotropicHyperelasticFEM(tet_mesh_, isotropic_material_);
    force_model_.reset(new IsotropicHyperelasticFEMForceModel(deformable_model_));

    // Create consistent (non-lumped) mass matrix.
    SparseMatrix *mass_matrix_;
    GenerateMassMatrix::computeMassMatrix(tet_mesh_, &mass_matrix_, true);

    // Initialize the integrator.
    implicit_integrator_.reset(new ImplicitBackwardEulerSparse
    (
            degrees_of_freedom_, timestep_, mass_matrix_, force_model_.get(),
            constrained_nodes_.size(), constrained_nodes_.data(), damping_mass_,
            damping_stiffness_));
}

IsotropicHyperelasticFemRos::~IsotropicHyperelasticFemRos()
{
}

void IsotropicHyperelasticFemRos::apply_forces(std::vector<double> &forces)
{
    implicit_integrator_->SetExternalForcesToZero();
    implicit_integrator_->SetExternalForces(forces.data());
    implicit_integrator_->DoTimestep();
}

void IsotropicHyperelasticFemRos::get_displacements(std::vector<double> &displacements)
{
    implicit_integrator_->GetqState(displacements.data());
}

void IsotropicHyperelasticFemRos::extract_points(
    std::vector<int> &indices, visualization_msgs::Marker &points, int index_start)
{
    std::vector<double> vertices_vec(vertices, vertices + (num_vertices * 3));
    std::set<int> done_vertices;

    for (auto ii : indices)
    {
        int vertex = (ii - index_start) / 3;
        // Test if the index is unique (since each index has three degrees of freedom).
        if (done_vertices.find(vertex) == done_vertices.end()) {
            done_vertices.insert(vertex);
            geometry_msgs::Point point;

            int index = vertex * 3;
            point.x = vertices_vec[index];
            point.y = vertices_vec[index + 1];
            point.z = vertices_vec[index + 2];

            points.points.push_back(point);
        }
    }
}

void IsotropicHyperelasticFemRos::init_mesh(udom_modeling_msgs::Mesh &mesh)
{
    std::vector<double> vertices_vec(vertices, vertices + (num_vertices * 3));
    for (std::size_t ii = 0; ii != num_vertices; ++ii) {
        int index = ii * 3;
        geometry_msgs::Point point;

        point.x = vertices_vec[index];
        point.y = vertices_vec[index + 1];
        point.z = vertices_vec[index + 2];

        mesh.vertices.push_back(point);
    }

    std::vector<int> elements_vec(elements, elements + num_elements * elements_per_vertex);
    for (std::size_t ii = 0; ii != num_elements; ++ii) {
        int index = ii * elements_per_vertex;
        udom_modeling_msgs::MeshTetrahedron tetrahedra;

        tetrahedra.vertex_indices[0] = elements_vec[index];
        tetrahedra.vertex_indices[1] = elements_vec[index + 1];
        tetrahedra.vertex_indices[2] = elements_vec[index + 2];
        tetrahedra.vertex_indices[3] = elements_vec[index + 3];

        mesh.tetrahedra.push_back(tetrahedra);
    }
}

void IsotropicHyperelasticFemRos::reset_mesh(udom_modeling_msgs::Mesh &mesh)
{
    implicit_integrator_->ResetToRest();
    implicit_integrator_->SetState(new double[num_vertices * 3]());

    std::vector<double> vertices_vec(vertices, vertices + (num_vertices * 3));
    for (std::size_t ii = 0; ii != num_vertices; ++ii) {
        int index = ii * 3;

        mesh.vertices.at(ii).x = vertices_vec.at(index);
        mesh.vertices.at(ii).y = vertices_vec.at(index + 1);
        mesh.vertices.at(ii).z = vertices_vec.at(index + 2);
    }
}

void IsotropicHyperelasticFemRos::update_mesh(udom_modeling_msgs::Mesh &mesh, std::vector<double> &displacements)
{
    std::vector<double> vertices_vec(vertices, vertices + (num_vertices * 3));
    for (std::size_t ii = 0; ii != displacements.size() / 3; ++ii) {
        int index = ii * 3;

        mesh.vertices.at(ii).x = vertices_vec.at(index) + displacements.at(index);
        mesh.vertices.at(ii).y = vertices_vec.at(index + 1) + displacements.at(index + 1);
        mesh.vertices.at(ii).z = vertices_vec.at(index + 2) + displacements.at(index + 2);
    }
}

double IsotropicHyperelasticFemRos::compute_energy(std::vector<double> &displacements)
{
    return force_model_->GetElasticEnergy(displacements.data());
}

void IsotropicHyperelasticFemRos::compute_internal_forces(
    std::vector<double> &displacements, std::vector<double> &forces)
{

    force_model_->GetInternalForce(displacements.data(), forces.data());
}

void IsotropicHyperelasticFemRos::compute_stiffness_matrix(
    std::vector<double> &displacements)
{
    SparseMatrix *stiffness_matrix;
    force_model_->GetTangentStiffnessMatrix(displacements.data(), stiffness_matrix);
    std::cout << "Stiffness matrix entries: " << std::endl;
    std::cout << stiffness_matrix->GetNumEntries();
}
