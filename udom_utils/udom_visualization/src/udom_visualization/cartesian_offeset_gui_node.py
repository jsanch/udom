#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
This node publishes a Cartesian offset message based on the controls of a GUI.

**Output(s):**

  * `cartesian_offset`: A Cartesian offset message.

    - *type:* `udom_common_msgs/CartesianOffset`

**Parameter(s):**
  * `loop_rate`: Node cycle rate (in Hz).

"""

import math
import threading
import Tkinter
import rospy
import udom_common_msgs.msg


# In meters.
LINEAR_RESOLUTION = 0.005
MAX_POS_X = 1.0
MIN_POS_X = -1.0
MAX_POS_Y = 1.0
MIN_POS_Y = -1.0
MAX_POS_Z = 1.0
MIN_POS_Z = -1.0

# In degrees.
ANGULAR_RESOLUTION = 1.0
MAX_ANGLE = 180
MIN_ANGLE = -180


offset = udom_common_msgs.msg.CartesianOffset()

global lock
lock = threading.Lock()


def create_window():
    """
    Creates a GUI window to publish a Cartesian offset.

    """
    master = Tkinter.Tk()

    # Linear offset
    linear_x = Tkinter.Scale(
        master, command=update_position_x, from_=MAX_POS_X, to=MIN_POS_X,
        resolution=LINEAR_RESOLUTION, label="X")
    linear_y = Tkinter.Scale(
        master, command=update_position_y, from_=MAX_POS_Y, to=MIN_POS_Y,
        resolution=LINEAR_RESOLUTION, label="Y")
    linear_z = Tkinter.Scale(
        master, command=update_position_z, from_=MAX_POS_Z, to=MIN_POS_Z,
        resolution=LINEAR_RESOLUTION, label="Z")

    linear_x.grid(row=0, column=0)
    linear_y.grid(row=0, column=1)
    linear_z.grid(row=0, column=2)

    linear_x.set(0.0)
    linear_x.set(0.0)
    linear_x.set(0.0)

    # Angular offset
    roll = Tkinter.Scale(
        master, command=update_roll, from_=MAX_ANGLE, to=MIN_ANGLE,
        resolution=ANGULAR_RESOLUTION, label="Roll")
    pitch = Tkinter.Scale(
        master, command=update_pitch, from_=MAX_ANGLE, to=MIN_ANGLE,
        resolution=ANGULAR_RESOLUTION, label="Pitch")
    yaw = Tkinter.Scale(
        master, command=update_yaw, from_=MAX_ANGLE, to=MIN_ANGLE,
        resolution=ANGULAR_RESOLUTION, label="Yaw")

    roll.grid(row=1, column=0)
    pitch.grid(row=1, column=1)
    yaw.grid(row=1, column=2)

    roll.set(0.0)
    pitch.set(0.0)
    yaw.set(0.0)

    master.title("Cartesian offset GUI")
    master.mainloop()
    rospy.signal_shutdown("GUI closed")


def update_position_x(slider):
    """
    Sets slider as the linear offset in the X axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    offset.linear.x = float(slider)
    lock.release()


def update_position_y(slider):
    """
    Sets slider as the linear offset in the Y axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    offset.linear.y = float(slider)
    lock.release()


def update_position_z(slider):
    """
    Sets slider as the linear offset in the Z axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    offset.linear.z = float(slider)
    lock.release()


def update_roll(slider):
    """
    Sets slider as the angle offset in the X axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    offset.angular.x = math.radians(float(slider))
    lock.release()


def update_pitch(slider):
    """
    Sets slider as the angle offset in the Y axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    offset.angular.y = math.radians(float(slider))
    lock.release()


def update_yaw(slider):
    """
    Sets slider as the angle offset in the Z axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    offset.angular.z = math.radians(float(slider))
    lock.release()


def publish_offset():
    """
    Publishes the Cartesian offset.

    """
    # Node cycle rate (in Hz).
    loop_rate = rospy.Rate(rospy.get_param('~loop_rate', 10))

    # Publishers
    pub_cartesian_offset = rospy.Publisher(
        '~cartesian_offset', udom_common_msgs.msg.CartesianOffset, queue_size=10)

    while not rospy.is_shutdown():
        pub_cartesian_offset.publish(offset)
        loop_rate.sleep()


def main():
    rospy.init_node('cartesian_offset_gui')

    import thread
    try:
        thread.start_new_thread(create_window, tuple())

        publish_offset()
    except rospy.ROSInterruptException:
        pass
