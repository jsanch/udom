#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
This node publishes a std_msgs/Float32MultiArray message based on the controls of a GUI.

**Output(s):**
  * `nodal_forces`: A message containing the forces to be applied to the nodes on a mesh.
    - *type:* `std_msgs/Float32MultiArray`

**Parameter(s):**
  * `loop_rate`: Node cycle rate (in Hz).
  * `number_of_nodes`: Number of nodes the mesh has.
  * `contact_nodes`: Indices for the nodes where the force ought to be applied.

"""

import threading
import Tkinter
import numpy as np
import rospy
import std_msgs.msg

# In Newtons
FORCE_RESOLUTION = 0.1
FORCE_SCALE = 1
MAX_FORCE = 1
MIN_FORCE = -1

force_array = std_msgs.msg.Float32MultiArray()
event = std_msgs.msg.String()
contact_nodes = [1, 2]

global lock
lock = threading.Lock()


def create_window():
    """
    Creates a GUI window to publish a pose.

    """
    master = Tkinter.Tk()

    # Force magnitude for node 1.
    force_1_x = Tkinter.Scale(
        master, command=update_force_1_x, from_=MAX_FORCE, to=MIN_FORCE,
        resolution=FORCE_RESOLUTION, label="Fx")
    force_1_y = Tkinter.Scale(
        master, command=update_force_1_y, from_=MAX_FORCE, to=MIN_FORCE,
        resolution=FORCE_RESOLUTION, label="Fy")
    force_1_z = Tkinter.Scale(
        master, command=update_force_1_z, from_=MAX_FORCE, to=MIN_FORCE,
        resolution=FORCE_RESOLUTION, label="Fz")
    force_1_x.grid(row=0, column=1)
    force_1_y.grid(row=0, column=2)
    force_1_z.grid(row=0, column=3)

    # Force magnitude for node 2.
    force_2_x = Tkinter.Scale(
        master, command=update_force_2_x, from_=MAX_FORCE, to=MIN_FORCE,
        resolution=FORCE_RESOLUTION, label="Fx")
    force_2_y = Tkinter.Scale(
        master, command=update_force_2_y, from_=MAX_FORCE, to=MIN_FORCE,
        resolution=FORCE_RESOLUTION, label="Fy")
    force_2_z = Tkinter.Scale(
        master, command=update_force_2_z, from_=MAX_FORCE, to=MIN_FORCE,
        resolution=FORCE_RESOLUTION, label="Fz")

    force_2_x.grid(row=1, column=1)
    force_2_y.grid(row=1, column=2)
    force_2_z.grid(row=1, column=3)

    # Event buttons.
    start_button = Tkinter.Button(master, command=start_cb, text="START")
    stop_button = Tkinter.Button(master, command=stop_cb, text="STOP")
    reset_button = Tkinter.Button(master, command=reset_cb, text="RESET")
    start_button.grid(row=2, column=1)
    stop_button.grid(row=2, column=2)
    reset_button.grid(row=2, column=3)

    # Labels.
    node_1_label = Tkinter.Label(
        master, text="Node {}".format(contact_nodes[0]))
    node_2_label = Tkinter.Label(
        master, text="Node {}".format(contact_nodes[1]))
    node_1_label.grid(row=0, column=0)
    node_2_label.grid(row=1, column=0)

    master.title("Nodal forces mock-up")
    master.mainloop()
    rospy.signal_shutdown("GUI closed")


def update_force_1_x(slider):
    """
    Sets slider as the force magnitude in the X axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    force_array.data[contact_nodes[0] * 3] = float(slider) * FORCE_SCALE
    lock.release()


def update_force_1_y(slider):
    """
    Sets slider as the force magnitude in the Y axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    force_array.data[(contact_nodes[0] * 3) + 1] = float(slider) * FORCE_SCALE
    lock.release()


def update_force_1_z(slider):
    """
    Sets slider as the force magnitude in the Z axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    force_array.data[(contact_nodes[0] * 3) + 2] = float(slider) * FORCE_SCALE
    lock.release()


def update_force_2_x(slider):
    """
    Sets slider as the force magnitude in the X axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    force_array.data[contact_nodes[1] * 3] = float(slider) * FORCE_SCALE
    lock.release()


def update_force_2_y(slider):
    """
    Sets slider as the force magnitude in the Y axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    force_array.data[(contact_nodes[1] * 3) + 1] = float(slider) * FORCE_SCALE
    lock.release()


def update_force_2_z(slider):
    """
    Sets slider as the force magnitude in the Z axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    force_array.data[(contact_nodes[1] * 3) + 2] = float(slider) * FORCE_SCALE
    lock.release()


def start_cb():
    """
    Updates the event to 'e_start'.

    """
    global lock
    lock.acquire()
    event.data = 'e_start'
    lock.release()


def stop_cb():
    """
    Updates the event to 'e_stop'.

    """
    global lock
    lock.acquire()
    event.data = 'e_stop'
    lock.release()


def reset_cb():
    """
    Updates the event to 'e_reset'.

    """
    global lock
    lock.acquire()
    event.data = 'e_reset'
    lock.release()


def publish_nodal_forces():
    """
    Publishes the nodal forces.

    """
    # Node cycle rate (in Hz).
    loop_rate = rospy.Rate(rospy.get_param('~loop_rate', 100))

    # Number of nodes of the mesh.
    number_of_nodes = rospy.get_param('~number_of_nodes', None)
    assert number_of_nodes is not None, "'number_of_nodes' must be specified."

    # Nodes on which the forces will be applied.
    nodes = rospy.get_param('~contact_nodes', [])
    assert len(nodes) == 2, "The number of 'contact_nodes' is restricted to 2."
    contact_nodes[0] = nodes[0]
    contact_nodes[1] = nodes[1]

    # Publishers
    pub_force_array = rospy.Publisher(
        '~force_array', std_msgs.msg.Float32MultiArray, queue_size=10)
    pub_event_out = rospy.Publisher('~event_out', std_msgs.msg.String, queue_size=10)

    force_array.data = np.zeros(number_of_nodes * 3)

    while not rospy.is_shutdown():
        pub_force_array.publish(force_array)
        if event.data:
            pub_event_out.publish(event)
        event.data = ""
        loop_rate.sleep()


def main():
    rospy.init_node('nodal_forces_mock_up')

    import thread
    try:
        thread.start_new_thread(create_window, tuple())

        publish_nodal_forces()
    except rospy.ROSInterruptException:
        pass
