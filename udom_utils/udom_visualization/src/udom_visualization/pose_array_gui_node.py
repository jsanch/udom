#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
This node publishes the (five) poses as a PoseArray message.

**Output(s):**
  * `pose_array`: The poses generated by the GUI controls.

    - *type:* `geometry_msgs/PoseArray`

**Parameter(s):**
  * `loop_rate`: Node cycle rate (in Hz).

  * `reference_frame`: The reference frame where the poses are specified.

"""

import threading
import Tkinter
import rospy
import std_msgs.msg
import geometry_msgs.msg


# In meters.
POSITION_RESOLUTION = 0.001
MAX_POS_X = 0.1
MIN_POS_X = -0.1
MAX_POS_Y = 0.0
MIN_POS_Y = -0.2
MAX_POS_Z = 0.2
MIN_POS_Z = 0.0
SCALE = 1.0

pose_array = geometry_msgs.msg.PoseArray()
pose_array.poses = [
    geometry_msgs.msg.Pose(), geometry_msgs.msg.Pose(), geometry_msgs.msg.Pose(),
    geometry_msgs.msg.Pose(), geometry_msgs.msg.Pose()]
centroid_pose = geometry_msgs.msg.PoseStamped()
event = std_msgs.msg.String()

global lock
lock = threading.Lock()


def create_window():
    """
    Creates a GUI window to publish the five poses.

    """
    master = Tkinter.Tk()

    # First pose.
    ff_position_x = Tkinter.Scale(
        master, command=update_position_x_ff, from_=MAX_POS_X, to=MIN_POS_X,
        resolution=POSITION_RESOLUTION, label="X")
    ff_position_y = Tkinter.Scale(
        master, command=update_position_y_ff, from_=MAX_POS_Y, to=MIN_POS_Y,
        resolution=POSITION_RESOLUTION, label="Y")
    ff_position_z = Tkinter.Scale(
        master, command=update_position_z_ff, from_=MAX_POS_Z, to=MIN_POS_Z,
        resolution=POSITION_RESOLUTION, label="Z")
    ff_label = Tkinter.Label(master, text="Pose 1")

    ff_position_x.set(0.036)
    ff_position_y.set(-0.093)
    ff_position_z.set(0.09)

    ff_label.grid(row=0, column=0)
    ff_position_x.grid(row=0, column=1)
    ff_position_y.grid(row=0, column=2)
    ff_position_z.grid(row=0, column=3)

    # Second pose.
    mf_position_x = Tkinter.Scale(
        master, command=update_position_x_mf, from_=MAX_POS_X, to=MIN_POS_X,
        resolution=POSITION_RESOLUTION, label="X")
    mf_position_y = Tkinter.Scale(
        master, command=update_position_y_mf, from_=MAX_POS_Y, to=MIN_POS_Y,
        resolution=POSITION_RESOLUTION, label="Y")
    mf_position_z = Tkinter.Scale(
        master, command=update_position_z_mf, from_=MAX_POS_Z, to=MIN_POS_Z,
        resolution=POSITION_RESOLUTION, label="Z")
    mf_label = Tkinter.Label(master, text="Pose 2")

    mf_position_x.set(0.015)
    mf_position_y.set(-0.093)
    mf_position_z.set(0.08)

    mf_label.grid(row=1, column=0)
    mf_position_x.grid(row=1, column=1)
    mf_position_y.grid(row=1, column=2)
    mf_position_z.grid(row=1, column=3)

    # Third pose.
    rf_position_x = Tkinter.Scale(
        master, command=update_position_x_rf, from_=MAX_POS_X, to=MIN_POS_X,
        resolution=POSITION_RESOLUTION, label="X")
    rf_position_y = Tkinter.Scale(
        master, command=update_position_y_rf, from_=MAX_POS_Y, to=MIN_POS_Y,
        resolution=POSITION_RESOLUTION, label="Y")
    rf_position_z = Tkinter.Scale(
        master, command=update_position_z_rf, from_=MAX_POS_Z, to=MIN_POS_Z,
        resolution=POSITION_RESOLUTION, label="Z")
    rf_label = Tkinter.Label(master, text="Pose 3")

    rf_position_x.set(-0.015)
    rf_position_y.set(-0.092)
    rf_position_z.set(0.075)

    rf_label.grid(row=2, column=0)
    rf_position_x.grid(row=2, column=1)
    rf_position_y.grid(row=2, column=2)
    rf_position_z.grid(row=2, column=3)

    # Fourth pose.
    lf_position_x = Tkinter.Scale(
        master, command=update_position_x_lf, from_=MAX_POS_X, to=MIN_POS_X,
        resolution=POSITION_RESOLUTION, label="X")
    lf_position_y = Tkinter.Scale(
        master, command=update_position_y_lf, from_=MAX_POS_Y, to=MIN_POS_Y,
        resolution=POSITION_RESOLUTION, label="Y")
    lf_position_z = Tkinter.Scale(
        master, command=update_position_z_lf, from_=MAX_POS_Z, to=MIN_POS_Z,
        resolution=POSITION_RESOLUTION, label="Z")
    lf_label = Tkinter.Label(master, text="Pose 4")

    lf_position_x.set(-0.031)
    lf_position_y.set(-0.093)
    lf_position_z.set(0.039)

    lf_label.grid(row=3, column=0)
    lf_position_x.grid(row=3, column=1)
    lf_position_y.grid(row=3, column=2)
    lf_position_z.grid(row=3, column=3)

    # Fifth pose.
    th_position_x = Tkinter.Scale(
        master, command=update_position_x_th, from_=MAX_POS_X, to=MIN_POS_X,
        resolution=POSITION_RESOLUTION, label="X")
    th_position_y = Tkinter.Scale(
        master, command=update_position_y_th, from_=MAX_POS_Y, to=MIN_POS_Y,
        resolution=POSITION_RESOLUTION, label="Y")
    th_position_z = Tkinter.Scale(
        master, command=update_position_z_th, from_=MAX_POS_Z, to=MIN_POS_Z,
        resolution=POSITION_RESOLUTION, label="Z")
    th_label = Tkinter.Label(master, text="Pose 5")

    th_position_x.set(0.024)
    th_position_y.set(-0.093)
    th_position_z.set(0.046)

    th_label.grid(row=4, column=0)
    th_position_x.grid(row=4, column=1)
    th_position_y.grid(row=4, column=2)
    th_position_z.grid(row=4, column=3)

    # Centroid pose.
    centroid_position_x = Tkinter.Scale(
        master, command=update_position_x_centroid, from_=MAX_POS_X, to=MIN_POS_X,
        resolution=POSITION_RESOLUTION, label="X")
    centroid_position_y = Tkinter.Scale(
        master, command=update_position_y_centroid, from_=MAX_POS_Y, to=MIN_POS_Y,
        resolution=POSITION_RESOLUTION, label="Y")
    centroid_position_z = Tkinter.Scale(
        master, command=update_position_z_centroid, from_=MAX_POS_Z, to=MIN_POS_Z,
        resolution=POSITION_RESOLUTION, label="Z")
    centroid_label = Tkinter.Label(master, text="Centroid pose")

    centroid_position_x.set(0.0)
    centroid_position_y.set(-0.065)
    centroid_position_z.set(0.04)

    centroid_label.grid(row=5, column=0)
    centroid_position_x.grid(row=5, column=1)
    centroid_position_y.grid(row=5, column=2)
    centroid_position_z.grid(row=5, column=3)

    # Event buttons.
    start_button = Tkinter.Button(master, command=start_cb, text="START")
    stop_button = Tkinter.Button(master, command=stop_cb, text="STOP")
    reset_button = Tkinter.Button(master, command=reset_cb, text="RESET")
    start_button.grid(row=1, column=4)
    stop_button.grid(row=2, column=4)
    reset_button.grid(row=3, column=4)

    master.title("Pose array configuration")
    master.mainloop()
    rospy.signal_shutdown("GUI closed")


def update_position_x_ff(slider):
    """
    Sets slider as the position in the X axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    pose_array.poses[0].position.x = float(slider) * SCALE
    lock.release()


def update_position_y_ff(slider):
    """
    Sets slider as the position in the Y axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    pose_array.poses[0].position.y = float(slider) * SCALE
    lock.release()


def update_position_z_ff(slider):
    """
    Sets slider as the position in the Z axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    pose_array.poses[0].position.z = float(slider) * SCALE
    lock.release()


def update_position_x_mf(slider):
    """
    Sets slider as the position in the X axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    pose_array.poses[1].position.x = float(slider) * SCALE
    lock.release()


def update_position_y_mf(slider):
    """
    Sets slider as the position in the Y axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    pose_array.poses[1].position.y = float(slider) * SCALE
    lock.release()


def update_position_z_mf(slider):
    """
    Sets slider as the position in the Z axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    pose_array.poses[1].position.z = float(slider) * SCALE
    lock.release()


def update_position_x_rf(slider):
    """
    Sets slider as the position in the X axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    pose_array.poses[2].position.x = float(slider) * SCALE
    lock.release()


def update_position_y_rf(slider):
    """
    Sets slider as the position in the Y axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    pose_array.poses[2].position.y = float(slider) * SCALE
    lock.release()


def update_position_z_rf(slider):
    """
    Sets slider as the position in the Z axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    pose_array.poses[2].position.z = float(slider) * SCALE
    lock.release()


def update_position_x_lf(slider):
    """
    Sets slider as the position in the X axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    pose_array.poses[3].position.x = float(slider) * SCALE
    lock.release()


def update_position_y_lf(slider):
    """
    Sets slider as the position in the Y axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    pose_array.poses[3].position.y = float(slider) * SCALE
    lock.release()


def update_position_z_lf(slider):
    """
    Sets slider as the position in the Z axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    pose_array.poses[3].position.z = float(slider) * SCALE
    lock.release()


def update_position_x_th(slider):
    """
    Sets slider as the position in the X axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    pose_array.poses[4].position.x = float(slider) * SCALE
    lock.release()


def update_position_y_th(slider):
    """
    Sets slider as the position in the Y axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    pose_array.poses[4].position.y = float(slider) * SCALE
    lock.release()


def update_position_z_th(slider):
    """
    Sets slider as the position in the Z axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    pose_array.poses[4].position.z = float(slider) * SCALE
    lock.release()


def update_position_x_centroid(slider):
    """
    Sets slider as the position in the X axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    centroid_pose.pose.position.x = float(slider) * SCALE
    lock.release()


def update_position_y_centroid(slider):
    """
    Sets slider as the position in the Y axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    centroid_pose.pose.position.y = float(slider) * SCALE
    lock.release()


def update_position_z_centroid(slider):
    """
    Sets slider as the position in the Z axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    centroid_pose.pose.position.z = float(slider) * SCALE
    lock.release()


def start_cb():
    """
    Updates the event to 'e_start'.

    """
    global lock
    lock.acquire()
    event.data = 'e_start'
    lock.release()


def stop_cb():
    """
    Updates the event to 'e_stop'.

    """
    global lock
    lock.acquire()
    event.data = 'e_stop'
    lock.release()


def reset_cb():
    """
    Updates the event to 'e_reset'.

    """
    global lock
    lock.acquire()
    event.data = 'e_reset'
    lock.release()


def publish_poses():
    """
    Publishes the target poses.

    """
    # Node cycle rate (in Hz).
    loop_rate = rospy.Rate(rospy.get_param('~loop_rate', 10))

    reference_frame = rospy.get_param('~reference_frame', 'rh_palm')

    # Publishers
    pub_pose_array = rospy.Publisher(
        '~pose_array', geometry_msgs.msg.PoseArray, queue_size=10)
    pub_centroid_pose = rospy.Publisher(
        '~centroid_pose', geometry_msgs.msg.PoseStamped, queue_size=10)

    pub_event_out = rospy.Publisher('~event_out', std_msgs.msg.String, queue_size=10)

    # We assume the poses are in the base frame of each finger.
    pose_array.header.frame_id = reference_frame
    centroid_pose.header.frame_id = reference_frame

    # The orientation should not change.
    pose_array.poses[0].orientation.w = 1.0
    pose_array.poses[1].orientation.w = 1.0
    pose_array.poses[2].orientation.w = 1.0
    pose_array.poses[3].orientation.w = 1.0
    pose_array.poses[4].orientation.w = 1.0
    centroid_pose.pose.orientation.w = 1.0

    while not rospy.is_shutdown():
        pose_array.header.stamp = rospy.Time.now()
        centroid_pose.header.stamp = rospy.Time.now()

        pub_pose_array.publish(pose_array)
        # pub_centroid_pose.publish(centroid_pose)
        if event.data:
            pub_event_out.publish(event)
        event.data = ""
        loop_rate.sleep()


def main():
    rospy.init_node('pose_array_gui')

    import thread
    try:
        thread.start_new_thread(create_window, tuple())

        publish_poses()
    except rospy.ROSInterruptException:
        pass
