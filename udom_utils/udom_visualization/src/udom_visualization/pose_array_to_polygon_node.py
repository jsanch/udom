#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
This node converts a PoseArray message into a PolygonStamped message,
in order to be visualized (e.g. in RViz).

**Input(s):**

  * `pose_array`: Pose array to be transformed.

    - *type:* `geometry_msgs/PoseArray`

**Output(s):**

  * `polygon`: Polygon to be visualized.

    - *type:* `geometry_msgs/PolygonStamped`


**Parameter(s):**
  * `loop_rate`: Node cycle rate (in Hz).

"""

import rospy
import geometry_msgs.msg


class PoseArrayToPolygonNode(object):
    """
    Converts a PoseArray message into a PolygonStamped message,
    in order to be visualized (e.g. in RViz).

    """
    def __init__(self):
        """
        Instantiates a node.

        :return: Node to transform a PoseArray message into a PolygonStamped message.
        :rtype: PoseArrayToPolygonNode

        """
        # Params
        self.pose_array = None

        # Node cycle rate (in Hz).
        self.loop_rate = rospy.Rate(rospy.get_param('~loop_rate', 10))

        # Publishers
        self.polygon = rospy.Publisher(
            "~polygon", geometry_msgs.msg.PolygonStamped, queue_size=10)

        # Subscribers
        rospy.Subscriber(
            '~pose_array', geometry_msgs.msg.PoseArray, self.pose_array_cb)

    def pose_array_cb(self, msg):
        """
        Obtains the pose array information.

        :param msg: Pose array.
        :type msg: geometry_msgs.msg.PoseArray

        """
        self.pose_array = msg

    def start(self):
        """
        Starts the node.

        """
        rospy.loginfo("Ready to start...")

        while not rospy.is_shutdown():
            if self.pose_array is not None:
                polygon = geometry_msgs.msg.PolygonStamped()
                polygon.header.stamp = rospy.Time.now()

                polygon.polygon.points = []
                for pose in self.pose_array.poses:
                    polygon.polygon.points.append(pose.position)

                polygon.header.frame_id = self.pose_array.header.frame_id
                self.polygon.publish(polygon)

                self.pose_array = None

            self.loop_rate.sleep()


def main():
    rospy.init_node("pose_array_to_polygon_node", anonymous=True)
    pose_array_to_polygon_node = PoseArrayToPolygonNode()
    pose_array_to_polygon_node.start()
