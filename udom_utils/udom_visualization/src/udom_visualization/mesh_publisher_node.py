#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
This node publishes the configuration of a mesh based on .ele and .node files.

**Output(s):**
  * `mesh`: The published mesh.
    - *type:* `udom_modeling_msgs/Mesh`

**Parameter(s):**
  * `loop_rate`: Node cycle rate (in Hz).
  * `zero_index`: Whether the elements index start from zero (false means they start from one).
  * `mesh_config`: The name of the folder containing a .ele and a .node file with the same
    name as the directory, which describe the configuration of the mesh. The path is assumed
    to be the `config` directory of this package.

"""

import numpy as np
import csv
import rospy
import rospkg
import geometry_msgs.msg
import udom_modeling_msgs.msg


class MeshPublisherNode(object):
    """
    Publishes a mesh based on a given configuration file.

    """
    def __init__(self):
        """
        Instantiates a mesh publisher node.

        :return: Node to publish a mesh.
        :rtype: MeshPublisherNode

        """
        ros_pack = rospkg.RosPack()
        package_path = ros_pack.get_path('udom_visualization')
        self.mesh_file = rospy.get_param('~mesh_config')
        self.config_path = package_path + '/config/' + self.mesh_file + '/'

        # Params
        self.mesh = udom_modeling_msgs.msg.Mesh()

        # Node cycle rate (in Hz).
        self.loop_rate = rospy.Rate(rospy.get_param('~loop_rate', 100))

        # Whether the elements index start from zero (false means they start from one).
        self.zero_index = rospy.get_param('~zero_index', True)

        # Mesh configuration files
        self.elements_file = self.config_path + self.mesh_file + '.ele'
        self.nodes_file = self.config_path + self.mesh_file + '.node'

        # Publishers
        self.mesh_publisher = rospy.Publisher(
            "~mesh", udom_modeling_msgs.msg.Mesh, queue_size=10)

    def start(self):
        """
        Starts the node.

        """
        rospy.loginfo("Ready to start...")
        self.upload_mesh()

        while not rospy.is_shutdown():
            self.mesh_publisher.publish(self.mesh)
            self.loop_rate.sleep()

    def upload_mesh(self):
        """
        Uploads the configuration of the mesh from the .ele and .nodes files and
        fills the mesh message.

        """
        # Process elements
        with open(self.elements_file) as csv_file:
            reader = csv.reader(csv_file, delimiter=' ')
            # Skip header
            next(reader)
            for row in reader:
                # Skip empty lines
                if not ''.join(row).strip():
                    continue
                # Get the values for the element columns and ignore the rest.
                rows = np.array(map(int, row[1:5]))
                if not self.zero_index:
                    rows -= 1

                tetrahedra = udom_modeling_msgs.msg.MeshTetrahedron(rows)
                self.mesh.tetrahedra.append(tetrahedra)

        # Process nodes
        with open(self.nodes_file) as csv_file:
            reader = csv.reader(csv_file, delimiter=' ')
            # Skip header
            next(reader)
            for row in reader:
                # Skip empty lines
                if not ''.join(row).strip():
                    continue
                rows = np.array(map(float, row[-3:]))
                point = geometry_msgs.msg.Point(x=rows[0], y=rows[1], z=rows[2])
                self.mesh.vertices.append(point)


def main():
    rospy.init_node("mesh_publisher_node", anonymous=True)
    mesh_publisher_node = MeshPublisherNode()
    mesh_publisher_node.start()
