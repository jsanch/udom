#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
This node publishes the (five) poses as targets for the Shadow Cartesian controller.

**Output(s):**
  * `finger_target`: The target for each finger of the hand as a pose (5x).

    - *type:* `geometry_msgs/PointStamped`

**Parameter(s):**
  * `loop_rate`: Node cycle rate (in Hz).

  * `reference_frame`: The reference frame where the target positions are specified.

"""

import threading
import Tkinter
import rospy
import std_msgs.msg
import geometry_msgs.msg


# In meters.
POSITION_RESOLUTION = 0.005
MAX_POS_X = 0.1
MIN_POS_X = -0.1
MAX_POS_Y = 0.0
MIN_POS_Y = -0.2
MAX_POS_Z = 0.2
MIN_POS_Z = 0.0
SCALE = 1.0

ff_pose = geometry_msgs.msg.PoseStamped()
mf_pose = geometry_msgs.msg.PoseStamped()
rf_pose = geometry_msgs.msg.PoseStamped()
lf_pose = geometry_msgs.msg.PoseStamped()
th_pose = geometry_msgs.msg.PoseStamped()
event = std_msgs.msg.String()

global lock
lock = threading.Lock()


def create_window():
    """
    Creates a GUI window to publish the five target poses.

    """
    master = Tkinter.Tk()

    # First finger.
    ff_position_x = Tkinter.Scale(
        master, command=update_position_x_ff, from_=MAX_POS_X, to=MIN_POS_X,
        resolution=POSITION_RESOLUTION, label="X")
    ff_position_y = Tkinter.Scale(
        master, command=update_position_y_ff, from_=MAX_POS_Y, to=MIN_POS_Y,
        resolution=POSITION_RESOLUTION, label="Y")
    ff_position_z = Tkinter.Scale(
        master, command=update_position_z_ff, from_=MAX_POS_Z, to=MIN_POS_Z,
        resolution=POSITION_RESOLUTION, label="Z")
    ff_label = Tkinter.Label(master, text="First finger (FF)")

    ff_position_x.set(0.04)
    ff_position_y.set(-0.005)
    ff_position_z.set(0.19)

    ff_label.grid(row=0, column=0)
    ff_position_x.grid(row=0, column=1)
    ff_position_y.grid(row=0, column=2)
    ff_position_z.grid(row=0, column=3)

    # Middle finger.
    mf_position_x = Tkinter.Scale(
        master, command=update_position_x_mf, from_=MAX_POS_X, to=MIN_POS_X,
        resolution=POSITION_RESOLUTION, label="X")
    mf_position_y = Tkinter.Scale(
        master, command=update_position_y_mf, from_=MAX_POS_Y, to=MIN_POS_Y,
        resolution=POSITION_RESOLUTION, label="Y")
    mf_position_z = Tkinter.Scale(
        master, command=update_position_z_mf, from_=MAX_POS_Z, to=MIN_POS_Z,
        resolution=POSITION_RESOLUTION, label="Z")
    mf_label = Tkinter.Label(master, text="Middle finger (MF)")

    mf_position_x.set(0.01)
    mf_position_y.set(-0.005)
    mf_position_z.set(0.195)

    mf_label.grid(row=1, column=0)
    mf_position_x.grid(row=1, column=1)
    mf_position_y.grid(row=1, column=2)
    mf_position_z.grid(row=1, column=3)

    # Ring finger.
    rf_position_x = Tkinter.Scale(
        master, command=update_position_x_rf, from_=MAX_POS_X, to=MIN_POS_X,
        resolution=POSITION_RESOLUTION, label="X")
    rf_position_y = Tkinter.Scale(
        master, command=update_position_y_rf, from_=MAX_POS_Y, to=MIN_POS_Y,
        resolution=POSITION_RESOLUTION, label="Y")
    rf_position_z = Tkinter.Scale(
        master, command=update_position_z_rf, from_=MAX_POS_Z, to=MIN_POS_Z,
        resolution=POSITION_RESOLUTION, label="Z")
    rf_label = Tkinter.Label(master, text="Ring finger (RF)")

    rf_position_x.set(-0.015)
    rf_position_y.set(-0.01)
    rf_position_z.set(0.19)

    rf_label.grid(row=2, column=0)
    rf_position_x.grid(row=2, column=1)
    rf_position_y.grid(row=2, column=2)
    rf_position_z.grid(row=2, column=3)

    # Little finger.
    lf_position_x = Tkinter.Scale(
        master, command=update_position_x_lf, from_=MAX_POS_X, to=MIN_POS_X,
        resolution=POSITION_RESOLUTION, label="X")
    lf_position_y = Tkinter.Scale(
        master, command=update_position_y_lf, from_=MAX_POS_Y, to=MIN_POS_Y,
        resolution=POSITION_RESOLUTION, label="Y")
    lf_position_z = Tkinter.Scale(
        master, command=update_position_z_lf, from_=MAX_POS_Z, to=MIN_POS_Z,
        resolution=POSITION_RESOLUTION, label="Z")
    lf_label = Tkinter.Label(master, text="Little finger (LF)")

    lf_position_x.set(-0.045)
    lf_position_y.set(-0.01)
    lf_position_z.set(0.18)

    lf_label.grid(row=3, column=0)
    lf_position_x.grid(row=3, column=1)
    lf_position_y.grid(row=3, column=2)
    lf_position_z.grid(row=3, column=3)

    # Thumb.
    th_position_x = Tkinter.Scale(
        master, command=update_position_x_th, from_=MAX_POS_X, to=MIN_POS_X,
        resolution=POSITION_RESOLUTION, label="X")
    th_position_y = Tkinter.Scale(
        master, command=update_position_y_th, from_=MAX_POS_Y, to=MIN_POS_Y,
        resolution=POSITION_RESOLUTION, label="Y")
    th_position_z = Tkinter.Scale(
        master, command=update_position_z_th, from_=MAX_POS_Z, to=MIN_POS_Z,
        resolution=POSITION_RESOLUTION, label="Z")
    th_label = Tkinter.Label(master, text="Thumb (TH)")

    th_position_x.set(0.09)
    th_position_y.set(-0.06)
    th_position_z.set(0.1)

    th_label.grid(row=4, column=0)
    th_position_x.grid(row=4, column=1)
    th_position_y.grid(row=4, column=2)
    th_position_z.grid(row=4, column=3)

    # Event buttons.
    start_button = Tkinter.Button(master, command=start_cb, text="START")
    stop_button = Tkinter.Button(master, command=stop_cb, text="STOP")
    reset_button = Tkinter.Button(master, command=reset_cb, text="RESET")
    start_button.grid(row=1, column=4)
    stop_button.grid(row=2, column=4)
    reset_button.grid(row=3, column=4)

    master.title("Target positions")
    master.mainloop()
    rospy.signal_shutdown("GUI closed")


def update_position_x_ff(slider):
    """
    Sets slider as the position in the X axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    ff_pose.pose.position.x = float(slider) * SCALE
    lock.release()


def update_position_y_ff(slider):
    """
    Sets slider as the position in the Y axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    ff_pose.pose.position.y = float(slider) * SCALE
    lock.release()


def update_position_z_ff(slider):
    """
    Sets slider as the position in the Z axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    ff_pose.pose.position.z = float(slider) * SCALE
    lock.release()


def update_position_x_mf(slider):
    """
    Sets slider as the position in the X axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    mf_pose.pose.position.x = float(slider) * SCALE
    lock.release()


def update_position_y_mf(slider):
    """
    Sets slider as the position in the Y axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    mf_pose.pose.position.y = float(slider) * SCALE
    lock.release()


def update_position_z_mf(slider):
    """
    Sets slider as the position in the Z axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    mf_pose.pose.position.z = float(slider) * SCALE
    lock.release()


def update_position_x_rf(slider):
    """
    Sets slider as the position in the X axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    rf_pose.pose.position.x = float(slider) * SCALE
    lock.release()


def update_position_y_rf(slider):
    """
    Sets slider as the position in the Y axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    rf_pose.pose.position.y = float(slider) * SCALE
    lock.release()


def update_position_z_rf(slider):
    """
    Sets slider as the position in the Z axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    rf_pose.pose.position.z = float(slider) * SCALE
    lock.release()


def update_position_x_lf(slider):
    """
    Sets slider as the position in the X axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    lf_pose.pose.position.x = float(slider) * SCALE
    lock.release()


def update_position_y_lf(slider):
    """
    Sets slider as the position in the Y axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    lf_pose.pose.position.y = float(slider) * SCALE
    lock.release()


def update_position_z_lf(slider):
    """
    Sets slider as the position in the Z axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    lf_pose.pose.position.z = float(slider) * SCALE
    lock.release()


def update_position_x_th(slider):
    """
    Sets slider as the position in the X axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    th_pose.pose.position.x = float(slider) * SCALE
    lock.release()


def update_position_y_th(slider):
    """
    Sets slider as the position in the Y axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    th_pose.pose.position.y = float(slider) * SCALE
    lock.release()


def update_position_z_th(slider):
    """
    Sets slider as the position in the Z axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    th_pose.pose.position.z = float(slider) * SCALE
    lock.release()


def start_cb():
    """
    Updates the event to 'e_start'.

    """
    global lock
    lock.acquire()
    event.data = 'e_start'
    lock.release()


def stop_cb():
    """
    Updates the event to 'e_stop'.

    """
    global lock
    lock.acquire()
    event.data = 'e_stop'
    lock.release()


def reset_cb():
    """
    Updates the event to 'e_reset'.

    """
    global lock
    lock.acquire()
    event.data = 'e_reset'
    lock.release()


def publish_poses():
    """
    Publishes the target poses.

    """
    # Node cycle rate (in Hz).
    loop_rate = rospy.Rate(rospy.get_param('~loop_rate', 10))

    reference_frame = rospy.get_param('~reference_frame', 'rh_palm')

    # Publishers
    pub_ff_pose = rospy.Publisher(
        '~ff_pose', geometry_msgs.msg.PoseStamped, queue_size=10)
    pub_mf_pose = rospy.Publisher(
        '~mf_pose', geometry_msgs.msg.PoseStamped, queue_size=10)
    pub_rf_pose = rospy.Publisher(
        '~rf_pose', geometry_msgs.msg.PoseStamped, queue_size=10)
    pub_lf_pose = rospy.Publisher(
        '~lf_pose', geometry_msgs.msg.PoseStamped, queue_size=10)
    pub_th_pose = rospy.Publisher(
        '~th_pose', geometry_msgs.msg.PoseStamped, queue_size=10)

    pub_event_out = rospy.Publisher('~event_out', std_msgs.msg.String, queue_size=10)

    # We assume the poses are in the base frame of each finger.
    ff_pose.header.frame_id = reference_frame
    mf_pose.header.frame_id = reference_frame
    rf_pose.header.frame_id = reference_frame
    lf_pose.header.frame_id = reference_frame
    th_pose.header.frame_id = reference_frame

    # The orientation should not change.
    ff_pose.pose.orientation.w = 1.0
    mf_pose.pose.orientation.w = 1.0
    rf_pose.pose.orientation.w = 1.0
    lf_pose.pose.orientation.w = 1.0
    th_pose.pose.orientation.w = 1.0

    while not rospy.is_shutdown():
        ff_pose.header.stamp = rospy.Time.now()
        mf_pose.header.stamp = rospy.Time.now()
        rf_pose.header.stamp = rospy.Time.now()
        lf_pose.header.stamp = rospy.Time.now()
        th_pose.header.stamp = rospy.Time.now()

        pub_ff_pose.publish(ff_pose)
        pub_mf_pose.publish(mf_pose)
        pub_rf_pose.publish(rf_pose)
        pub_lf_pose.publish(lf_pose)
        pub_th_pose.publish(th_pose)
        if event.data:
            pub_event_out.publish(event)
        event.data = ""
        loop_rate.sleep()


def main():
    rospy.init_node('target_poses_gui')

    import thread
    try:
        thread.start_new_thread(create_window, tuple())

        publish_poses()
    except rospy.ROSInterruptException:
        pass
