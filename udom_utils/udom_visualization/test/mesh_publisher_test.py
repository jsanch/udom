#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
Integration test for the 'mesh_publisher' node.

"""

import rospy
import unittest
import rostest
import geometry_msgs.msg
import udom_modeling_msgs.msg

PKG = 'udom_visualization'


class TestMeshPublisher(unittest.TestCase):
    def setUp(self):
        """
        Sets up the test fixture before exercising it.

        """
        # params
        self.result = None
        self.wait_for_result = None

        # subscribers
        self.component_output = rospy.Subscriber(
            '~component_output', udom_modeling_msgs.msg.Mesh, self.result_callback)

    def tearDown(self):
        """
        Deconstructs the test fixture after testing it.

        """
        self.component_output.unregister()

    def test_mesh_to_points_node(self):
        """
        Verifies the node's is able to correctly publish a mesh.

        """
        desired_mesh = udom_modeling_msgs.msg.Mesh()

        p_1 = geometry_msgs.msg.Point(0.0, 0.0, 0.0)
        p_2 = geometry_msgs.msg.Point(0.0, 0.0, 1.0)
        p_3 = geometry_msgs.msg.Point(0.0, 1.0, 0.0)
        p_4 = geometry_msgs.msg.Point(0.0, 1.0, 1.0)
        p_5 = geometry_msgs.msg.Point(1.0, 0.0, 0.0)
        p_6 = geometry_msgs.msg.Point(1.0, 0.0, 1.0)
        p_7 = geometry_msgs.msg.Point(1.0, 1.0, 0.0)
        p_8 = geometry_msgs.msg.Point(1.0, 1.0, 1.0)
        desired_mesh.vertices = [p_1, p_2, p_3, p_4, p_5, p_6, p_7, p_8]

        t_1 = udom_modeling_msgs.msg.MeshTetrahedron([0, 2, 1, 4])
        t_2 = udom_modeling_msgs.msg.MeshTetrahedron([6, 5, 1, 3])
        t_3 = udom_modeling_msgs.msg.MeshTetrahedron([6, 7, 5, 3])
        t_4 = udom_modeling_msgs.msg.MeshTetrahedron([3, 5, 1, 2])
        desired_mesh.tetrahedra = [t_1, t_2, t_3, t_4]

        while not self.wait_for_result:
            rospy.sleep(0.01)

        self.assertIsInstance(self.result, udom_modeling_msgs.msg.Mesh)

        for r_point, d_point in zip(self.result.vertices, desired_mesh.vertices):
            self.assertAlmostEqual(r_point.x, d_point.x)
            self.assertAlmostEqual(r_point.y, d_point.y)
            self.assertAlmostEqual(r_point.z, d_point.z)

        for r_tet, d_tet in zip(self.result.tetrahedra, desired_mesh.tetrahedra):
            self.assertEqual(r_tet, d_tet)

    def result_callback(self, msg):
        self.result = msg
        self.wait_for_result = True


if __name__ == '__main__':
    rospy.init_node('mesh_publisher_test')
    rostest.rosrun(PKG, 'mesh_publisher_test', TestMeshPublisher)
