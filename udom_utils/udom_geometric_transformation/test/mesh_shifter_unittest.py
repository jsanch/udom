#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
Unittest for the mesh_shifter_utils.py module.

"""

import numpy as np
import unittest
import rosunit
import std_msgs.msg
import geometry_msgs.msg
import udom_common_msgs.msg
import udom_modeling_msgs.msg
import udom_geometric_transformation.mesh_shifter_utils as utils

PKG = 'udom_geometric_transformation'


class TestMeshShifterUtils(unittest.TestCase):
    """
    Tests the mesh_shifter_utils.py module.

    """
    def test_shift_points_offset_x(self):
        """
        Tests that the function correctly shift a set of points by a linear offset in X.

        """
        offset = udom_common_msgs.msg.CartesianOffset()
        offset.linear.x = 0.5

        points = np.array([
            [0.5, 0.3, 0.9],     # p_1 (x, y, z)
            [0.0, 0.0, 1.0],     # p_2....
            [0.0, 1.0, 0.0],
            [0.0, 1.0, 1.0],
            [1.0, 0.0, 0.0]])

        desired = np.array([
            [1.0, 0.3, 0.9],
            [0.5, 0.0, 1.0],
            [0.5, 1.0, 0.0],
            [0.5, 1.0, 1.0],
            [1.5, 0.0, 0.0]])

        result = utils.shift_points(points.flatten().tolist(), offset)
        np.testing.assert_allclose(result, desired)

    def test_shift_points_offset(self):
        """
        Tests that the function correctly shift a set of points by a linear offset.

        """
        offset = udom_common_msgs.msg.CartesianOffset()
        offset.linear.x = 0.5
        offset.linear.y = 0.3
        offset.linear.z = 1.0

        points = np.array([
            [0.5, 0.3, 0.9],     # p_1 (x, y, z)
            [0.0, 0.0, 1.0],     # p_2....
            [0.0, 1.0, 0.0],
            [0.0, 1.0, 1.0],
            [1.0, 0.0, 0.0]])

        desired = np.array([
            [1.0, 0.6, 1.9],
            [0.5, 0.3, 2.0],
            [0.5, 1.3, 1.0],
            [0.5, 1.3, 2.0],
            [1.5, 0.3, 1.0]])

        result = utils.shift_points(points.flatten().tolist(), offset)
        np.testing.assert_allclose(result, desired)

    def test_compute_nodal_distance_x(self):
        """
        Tests that the function correctly computes the distances when there is an offset
        in the X direction between two meshes.

        """
        mesh = udom_modeling_msgs.msg.Mesh()
        mesh_position = udom_modeling_msgs.msg.Mesh()

        p_1 = geometry_msgs.msg.Point(0.0, 0.0, 0.0)
        p_2 = geometry_msgs.msg.Point(0.0, 0.0, 1.0)
        p_3 = geometry_msgs.msg.Point(0.0, 1.0, 0.0)
        p_4 = geometry_msgs.msg.Point(0.0, 1.0, 1.0)
        p_5 = geometry_msgs.msg.Point(1.0, 0.0, 0.0)
        p_6 = geometry_msgs.msg.Point(1.0, 0.0, 1.0)
        p_7 = geometry_msgs.msg.Point(1.0, 1.0, 0.0)
        p_8 = geometry_msgs.msg.Point(1.0, 1.0, 1.0)
        mesh.vertices = [p_1, p_2, p_3, p_4, p_5, p_6, p_7, p_8]

        p_1_shifted = geometry_msgs.msg.Point(0.5, 0.0, 0.0)
        p_2_shifted = geometry_msgs.msg.Point(0.5, 0.0, 1.0)
        p_3_shifted = geometry_msgs.msg.Point(0.5, 1.0, 0.0)
        p_4_shifted = geometry_msgs.msg.Point(0.5, 1.0, 1.0)
        p_5_shifted = geometry_msgs.msg.Point(1.5, 0.0, 0.0)
        p_6_shifted = geometry_msgs.msg.Point(1.5, 0.0, 1.0)
        p_7_shifted = geometry_msgs.msg.Point(1.5, 1.0, 0.0)
        p_8_shifted = geometry_msgs.msg.Point(1.5, 1.0, 1.0)
        mesh_position.vertices = [
            p_1_shifted, p_2_shifted, p_3_shifted, p_4_shifted,
            p_5_shifted, p_6_shifted, p_7_shifted, p_8_shifted]

        t_1 = udom_modeling_msgs.msg.MeshTetrahedron([3, 2, 4, 0])
        t_2 = udom_modeling_msgs.msg.MeshTetrahedron([3, 1, 4, 0])
        t_3 = udom_modeling_msgs.msg.MeshTetrahedron([3, 6, 2, 4])
        t_4 = udom_modeling_msgs.msg.MeshTetrahedron([3, 6, 7, 4])
        t_5 = udom_modeling_msgs.msg.MeshTetrahedron([3, 5, 1, 4])
        t_6 = udom_modeling_msgs.msg.MeshTetrahedron([3, 5, 7, 4])
        mesh.tetrahedra = [t_1, t_2, t_3, t_4, t_5, t_6]
        mesh_position.tetrahedra = [t_1, t_2, t_3, t_4, t_5, t_6]

        desired = std_msgs.msg.Float32MultiArray()
        desired.data = np.zeros(len(mesh.vertices) * 3)
        desired.data[0] = 0.5
        desired.data[3] = 0.5
        desired.data[6] = 0.5
        desired.data[9] = 0.5
        desired.data[12] = 0.5
        desired.data[15] = 0.5
        desired.data[18] = 0.5
        desired.data[21] = 0.5

        result = utils.compute_nodal_distance(mesh, mesh_position)
        np.testing.assert_allclose(result.data, desired.data)

    def test_compute_nodal_distance_x_nodes(self):
        """
        Tests that the function correctly computes the distances when there is an offset
        in the X direction between two meshes, when one mesh is a subset of the other.

        """
        mesh = udom_modeling_msgs.msg.Mesh()
        mesh_position = udom_modeling_msgs.msg.Mesh()

        p_1 = geometry_msgs.msg.Point(0.0, 0.0, 0.0)
        p_2 = geometry_msgs.msg.Point(0.0, 0.0, 1.0)
        p_3 = geometry_msgs.msg.Point(0.0, 1.0, 0.0)
        p_4 = geometry_msgs.msg.Point(0.0, 1.0, 1.0)
        p_5 = geometry_msgs.msg.Point(1.0, 0.0, 0.0)
        p_6 = geometry_msgs.msg.Point(1.0, 0.0, 1.0)
        p_7 = geometry_msgs.msg.Point(1.0, 1.0, 0.0)
        p_8 = geometry_msgs.msg.Point(1.0, 1.0, 1.0)
        mesh.vertices = [p_1, p_2, p_3, p_4, p_5, p_6, p_7, p_8]

        p_4_shifted = geometry_msgs.msg.Point(0.5, 1.0, 1.0)
        p_5_shifted = geometry_msgs.msg.Point(1.5, 0.0, 0.0)
        p_6_shifted = geometry_msgs.msg.Point(1.5, 0.0, 1.0)
        p_8_shifted = geometry_msgs.msg.Point(1.5, 1.0, 1.0)
        mesh_position.vertices = [p_4_shifted, p_5_shifted, p_6_shifted, p_8_shifted]

        contact_nodes = [3, 4, 5, 7]

        t_1 = udom_modeling_msgs.msg.MeshTetrahedron([3, 2, 4, 0])
        t_2 = udom_modeling_msgs.msg.MeshTetrahedron([3, 1, 4, 0])
        t_3 = udom_modeling_msgs.msg.MeshTetrahedron([3, 6, 2, 4])
        t_4 = udom_modeling_msgs.msg.MeshTetrahedron([3, 6, 7, 4])
        t_5 = udom_modeling_msgs.msg.MeshTetrahedron([3, 5, 1, 4])
        t_6 = udom_modeling_msgs.msg.MeshTetrahedron([3, 5, 7, 4])
        mesh.tetrahedra = [t_1, t_2, t_3, t_4, t_5, t_6]

        desired = std_msgs.msg.Float32MultiArray()
        desired.data = np.zeros(len(mesh.vertices) * 3)
        desired.data[9] = 0.5
        desired.data[12] = 0.5
        desired.data[15] = 0.5
        desired.data[21] = 0.5

        result = utils.compute_nodal_distance(mesh, mesh_position, nodes=contact_nodes)
        np.testing.assert_allclose(result.data, desired.data)

    def test_compute_nodal_distance_xyz_nodes(self):
        """
        Tests that the function correctly computes the distances when there is an offset
        in all directions between two meshes, when one mesh is a subset of the other.

        """
        mesh = udom_modeling_msgs.msg.Mesh()
        mesh_position = udom_modeling_msgs.msg.Mesh()

        p_1 = geometry_msgs.msg.Point(0.0, 0.0, 0.0)
        p_2 = geometry_msgs.msg.Point(0.0, 0.0, 1.0)
        p_3 = geometry_msgs.msg.Point(0.0, 1.0, 0.0)
        p_4 = geometry_msgs.msg.Point(0.0, 1.0, 1.0)
        p_5 = geometry_msgs.msg.Point(1.0, 0.0, 0.0)
        p_6 = geometry_msgs.msg.Point(1.0, 0.0, 1.0)
        p_7 = geometry_msgs.msg.Point(1.0, 1.0, 0.0)
        p_8 = geometry_msgs.msg.Point(1.0, 1.0, 1.0)
        mesh.vertices = [p_1, p_2, p_3, p_4, p_5, p_6, p_7, p_8]

        p_4_shifted = geometry_msgs.msg.Point(0.5, 0.5, 2.0)
        p_5_shifted = geometry_msgs.msg.Point(1.5, -0.5, 1.0)
        p_6_shifted = geometry_msgs.msg.Point(1.5, -0.5, 2.0)
        p_8_shifted = geometry_msgs.msg.Point(1.5, 0.5, 2.0)
        mesh_position.vertices = [p_4_shifted, p_5_shifted, p_6_shifted, p_8_shifted]

        contact_nodes = [3, 4, 5, 7]

        t_1 = udom_modeling_msgs.msg.MeshTetrahedron([3, 2, 4, 0])
        t_2 = udom_modeling_msgs.msg.MeshTetrahedron([3, 1, 4, 0])
        t_3 = udom_modeling_msgs.msg.MeshTetrahedron([3, 6, 2, 4])
        t_4 = udom_modeling_msgs.msg.MeshTetrahedron([3, 6, 7, 4])
        t_5 = udom_modeling_msgs.msg.MeshTetrahedron([3, 5, 1, 4])
        t_6 = udom_modeling_msgs.msg.MeshTetrahedron([3, 5, 7, 4])
        mesh.tetrahedra = [t_1, t_2, t_3, t_4, t_5, t_6]

        desired = std_msgs.msg.Float32MultiArray()
        desired.data = np.zeros(len(mesh.vertices) * 3)
        desired.data[9] = 0.5
        desired.data[10] = -0.5
        desired.data[11] = 1.0

        desired.data[12] = 0.5
        desired.data[13] = -0.5
        desired.data[14] = 1.0

        desired.data[15] = 0.5
        desired.data[16] = -0.5
        desired.data[17] = 1.0

        desired.data[21] = 0.5
        desired.data[22] = -0.5
        desired.data[23] = 1.0

        result = utils.compute_nodal_distance(mesh, mesh_position, nodes=contact_nodes)
        np.testing.assert_allclose(result.data, desired.data)


if __name__ == '__main__':
    rosunit.unitrun(PKG, 'test_mesh_shifter_utils', TestMeshShifterUtils)
