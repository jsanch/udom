#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
Integration test for the 'mesh_measurer' node.

"""

import rospy
import unittest
import rostest
import std_msgs.msg
import geometry_msgs.msg
import udom_modeling_msgs.msg

PKG = 'udom_geometric_transformation'


class TestMeshMeasurer(unittest.TestCase):
    def setUp(self):
        """
        Sets up the test fixture before exercising it.

        """
        # params
        self.result = None
        self.wait_for_result = None

        # publishers
        self.event_out = rospy.Publisher(
            '~event_out', std_msgs.msg.String, latch=True)
        self.mesh_reference = rospy.Publisher(
            '~mesh_reference', udom_modeling_msgs.msg.Mesh, queue_size=1)
        self.mesh_target = rospy.Publisher(
            '~mesh_target', udom_modeling_msgs.msg.Mesh, queue_size=1)

        # subscribers
        self.component_output = rospy.Subscriber(
            '~component_output', std_msgs.msg.Float32, self.result_callback)

    def tearDown(self):
        """
        Deconstructs the test fixture after testing it.

        """
        self.component_output.unregister()
        self.event_out.unregister()
        self.mesh_reference.unregister()
        self.mesh_target.unregister()

    def test_mesh_measurer_node(self):
        """
        Verifies the node's interface is correct.
        Note: this is not a functionality test.

        """
        mesh_reference = udom_modeling_msgs.msg.Mesh()
        mesh_target = udom_modeling_msgs.msg.Mesh()

        p_1 = geometry_msgs.msg.Point(0.0, 0.0, 0.0)
        p_2 = geometry_msgs.msg.Point(0.0, 0.0, 1.0)
        p_3 = geometry_msgs.msg.Point(0.0, 1.0, 0.0)
        p_4 = geometry_msgs.msg.Point(0.0, 1.0, 1.0)
        p_5 = geometry_msgs.msg.Point(1.0, 0.0, 0.0)
        p_6 = geometry_msgs.msg.Point(1.0, 0.0, 1.0)
        p_7 = geometry_msgs.msg.Point(1.0, 1.0, 0.0)
        p_8 = geometry_msgs.msg.Point(1.0, 1.0, 1.0)
        mesh_reference.vertices = [p_1, p_2, p_3, p_4, p_5, p_6, p_7, p_8]

        p_3_target = geometry_msgs.msg.Point(0.0, 1.5, -0.05)
        mesh_target.vertices = [p_1, p_2, p_3_target, p_4, p_5, p_6, p_7, p_8]

        t_1 = udom_modeling_msgs.msg.MeshTetrahedron([0, 2, 1, 4])
        t_2 = udom_modeling_msgs.msg.MeshTetrahedron([6, 5, 1, 3])
        t_3 = udom_modeling_msgs.msg.MeshTetrahedron([6, 7, 5, 3])
        t_4 = udom_modeling_msgs.msg.MeshTetrahedron([3, 5, 1, 2])
        mesh_reference.tetrahedra = [t_1, t_2, t_3, t_4]
        mesh_target.tetrahedra = [t_1, t_2, t_3, t_4]

        while not self.wait_for_result:
            self.mesh_reference.publish(mesh_reference)
            self.mesh_target.publish(mesh_target)
            self.event_out.publish('e_start')

        self.assertIsInstance(self.result, std_msgs.msg.Float32)
        self.assertGreater(self.result.data, 0.0)

    def result_callback(self, msg):
        self.result = msg
        self.wait_for_result = True


if __name__ == '__main__':
    rospy.init_node('mesh_measurer_test')
    rostest.rosrun(PKG, 'mesh_measurer_test', TestMeshMeasurer)
