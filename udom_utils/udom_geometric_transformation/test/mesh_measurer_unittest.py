#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
Unittest for the mesh_measurer_utils.py module.

"""

import numpy as np
import scipy.spatial as sp
import unittest
import rosunit
import geometry_msgs.msg
import udom_modeling_msgs.msg
import udom_geometric_transformation.mesh_measurer_utils as utils

PKG = 'udom_geometric_transformation'


class TestMeshMeasurerUtils(unittest.TestCase):
    """
    Tests the mesh_measurer_utils.py module.

    """
    def test_mesh_to_points(self):
        """
        Tests that the function correctly transforms a mesh object into a set of points.

        """
        mesh = udom_modeling_msgs.msg.Mesh()
        p_1 = geometry_msgs.msg.Point(0.5, 0.3, 0.9)
        p_2 = geometry_msgs.msg.Point(0.0, 0.0, 1.0)
        p_3 = geometry_msgs.msg.Point(0.0, 1.0, 0.0)
        p_4 = geometry_msgs.msg.Point(0.0, 1.0, 1.0)
        p_5 = geometry_msgs.msg.Point(1.0, 0.0, 0.0)
        mesh.vertices = [p_1, p_2, p_3, p_4, p_5]

        desired = np.array([
            [0.5, 0.3, 0.9],     # p_1 (x, y, z)
            [0.0, 0.0, 1.0],     # p_2....
            [0.0, 1.0, 0.0],
            [0.0, 1.0, 1.0],
            [1.0, 0.0, 0.0]])

        result = utils.mesh_to_points(mesh)
        np.testing.assert_allclose(result, desired)

    def test_hausdorff_distance(self):
        """
        Tests the Hausdorff distance between two set of points.

        """
        # TODO: test when problem with SciPy version is fixed
        return
        # The distance between two identical set of points should be zero.
        a = np.random.rand(5, 3)
        result = utils.hausdorff_distance(a, a)
        self.assertAlmostEqual(result, 0.0, places=6)

        # The distance between two set of points, each having one point, should
        # be the same as their Euclidean distance.
        a = np.random.rand(1, 3)
        b = np.random.rand(1, 3)
        result = utils.hausdorff_distance(a, b)
        euclidean = sp.distance.euclidean(a, b)
        self.assertAlmostEqual(result, euclidean, places=6)

        # The distance between two different set of points should be greater than zero.
        a = np.random.rand(5, 3)
        b = np.random.rand(5, 3) * 5
        result = utils.hausdorff_distance(a, b)
        self.assertGreater(result, 0.0)

    def test_procrustes_distance_same(self):
        """
        Tests the Procrustes distance between two set of points.

        """
        # Distance should be a float.
        a = np.random.rand(5, 3)
        result = utils.procrustes_distance(a, a)
        self.assertIsInstance(result, float)

        # The distance between two identical set of points should be zero.
        result = utils.procrustes_distance(a, a)
        self.assertAlmostEqual(result, 0.0, places=6)

        # The distance between two different set of points should be greater than zero.
        a = np.random.rand(5, 3)
        b = np.random.rand(5, 3) * 5
        result = utils.procrustes_distance(a, b)
        self.assertGreater(result, 0.0)


if __name__ == '__main__':
    rosunit.unitrun(PKG, 'test_mesh_measurer_utils', TestMeshMeasurerUtils)
