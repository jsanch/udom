#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
Integration test for the 'position_based_force_calculator' node.

"""

import numpy as np
import rospy
import unittest
import rostest
import std_msgs.msg
import geometry_msgs.msg
import udom_modeling_msgs.msg

PKG = 'udom_geometric_transformation'


class TestPositionBasedForceCalculator(unittest.TestCase):
    def setUp(self):
        """
        Sets up the test fixture before exercising it.

        """
        # params
        self.result = None
        self.wait_for_result = None

        # publishers
        self.event_out = rospy.Publisher('~event_out', std_msgs.msg.String, latch=True)
        self.mesh = rospy.Publisher('~mesh', udom_modeling_msgs.msg.Mesh, queue_size=1)
        self.mesh_position = rospy.Publisher(
            '~mesh_position', udom_modeling_msgs.msg.Mesh, queue_size=1)

        # subscribers
        self.component_output = rospy.Subscriber(
            '~component_output', std_msgs.msg.Float32MultiArray, self.result_callback)

    def tearDown(self):
        """
        Deconstructs the test fixture after testing it.

        """
        self.component_output.unregister()
        self.event_out.unregister()
        self.mesh_position.unregister()
        self.mesh.unregister()

    def test_position_based_forces_node(self):
        """
        Verifies the node's interface is correct.
        Note: this is not a functionality test.

        """
        mesh = udom_modeling_msgs.msg.Mesh()
        mesh_position = udom_modeling_msgs.msg.Mesh()

        p_1 = geometry_msgs.msg.Point(0.0, 0.0, 0.0)
        p_2 = geometry_msgs.msg.Point(0.0, 0.0, 1.0)
        p_3 = geometry_msgs.msg.Point(0.0, 1.0, 0.0)
        p_4 = geometry_msgs.msg.Point(0.0, 1.0, 1.0)
        p_5 = geometry_msgs.msg.Point(1.0, 0.0, 0.0)
        p_6 = geometry_msgs.msg.Point(1.0, 0.0, 1.0)
        p_7 = geometry_msgs.msg.Point(1.0, 1.0, 0.0)
        p_8 = geometry_msgs.msg.Point(1.0, 1.0, 1.0)
        mesh.vertices = [p_1, p_2, p_3, p_4, p_5, p_6, p_7, p_8]

        p_4_shifted = geometry_msgs.msg.Point(0.5, 1.0, 1.0)
        p_5_shifted = geometry_msgs.msg.Point(1.5, 0.0, 0.0)
        p_6_shifted = geometry_msgs.msg.Point(1.5, 0.0, 1.0)
        p_8_shifted = geometry_msgs.msg.Point(1.5, 1.0, 1.0)
        mesh_position.vertices = [p_4_shifted, p_5_shifted, p_6_shifted, p_8_shifted]

        t_1 = udom_modeling_msgs.msg.MeshTetrahedron([3, 2, 4, 0])
        t_2 = udom_modeling_msgs.msg.MeshTetrahedron([3, 1, 4, 0])
        t_3 = udom_modeling_msgs.msg.MeshTetrahedron([3, 6, 2, 4])
        t_4 = udom_modeling_msgs.msg.MeshTetrahedron([3, 6, 7, 4])
        t_5 = udom_modeling_msgs.msg.MeshTetrahedron([3, 5, 1, 4])
        t_6 = udom_modeling_msgs.msg.MeshTetrahedron([3, 5, 7, 4])
        mesh.tetrahedra = [t_1, t_2, t_3, t_4, t_5, t_6]

        desired = std_msgs.msg.Float32MultiArray()
        desired.data = np.zeros(len(mesh.vertices) * 3)
        desired.data[9] = 0.5
        desired.data[12] = 0.5
        desired.data[15] = 0.5
        desired.data[21] = 0.5

        while not self.wait_for_result:
            self.mesh_position.publish(mesh_position)
            self.mesh.publish(mesh)
            self.event_out.publish('e_start')

        self.assertIsInstance(self.result, std_msgs.msg.Float32MultiArray)

        # There should be a vector force for each node.
        self.assertEqual(len(self.result.data), len(mesh.vertices) * 3)
        np.testing.assert_allclose(self.result.data, desired.data)

    def result_callback(self, msg):
        self.result = msg
        self.wait_for_result = True


if __name__ == '__main__':
    rospy.init_node('position_based_force_calculator_test')
    rostest.rosrun(PKG, 'position_based_force_calculator_test', TestPositionBasedForceCalculator)
