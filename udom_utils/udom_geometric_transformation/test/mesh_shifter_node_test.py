#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
Integration test for the 'mesh_shifter' node.

"""

import rospy
import unittest
import rostest
import std_msgs.msg
import geometry_msgs.msg
import udom_common_msgs.msg
import udom_modeling_msgs.msg

PKG = 'udom_geometric_transformation'


class TestMeshShifter(unittest.TestCase):
    def setUp(self):
        """
        Sets up the test fixture before exercising it.

        """
        # params
        self.result = None
        self.wait_for_result = None

        # publishers
        self.event_out = rospy.Publisher(
            '~event_out', std_msgs.msg.String, latch=True)
        self.mesh_in = rospy.Publisher(
            '~mesh_in', udom_modeling_msgs.msg.Mesh, queue_size=1)
        self.offset = rospy.Publisher(
            '~offset', udom_common_msgs.msg.CartesianOffset, queue_size=1)

        # subscribers
        self.component_output = rospy.Subscriber(
            '~component_output', udom_modeling_msgs.msg.Mesh, self.result_callback)

    def tearDown(self):
        """
        Deconstructs the test fixture after testing it.

        """
        self.component_output.unregister()
        self.event_out.unregister()
        self.mesh_in.unregister()
        self.offset.unregister()

    def test_mesh_shifter_node(self):
        """
        Verifies the node's interface is correct.
        Note: this is not a functionality test.

        """
        offset = udom_common_msgs.msg.CartesianOffset()
        offset.linear.x = 1.0
        offset.linear.y = 0.0
        offset.linear.z = -1.0
        offset.angular.x = 0.0
        offset.angular.y = 0.0
        offset.angular.z = 0.0

        mesh_in = udom_modeling_msgs.msg.Mesh()

        p_1 = geometry_msgs.msg.Point(0.0, 0.0, 0.0)
        p_2 = geometry_msgs.msg.Point(0.0, 0.0, 1.0)
        p_3 = geometry_msgs.msg.Point(0.0, 1.0, 0.0)
        p_4 = geometry_msgs.msg.Point(0.0, 1.0, 1.0)
        p_5 = geometry_msgs.msg.Point(1.0, 0.0, 0.0)
        p_6 = geometry_msgs.msg.Point(1.0, 0.0, 1.0)
        p_7 = geometry_msgs.msg.Point(1.0, 1.0, 0.0)
        p_8 = geometry_msgs.msg.Point(1.0, 1.0, 1.0)
        mesh_in.vertices = [p_1, p_2, p_3, p_4, p_5, p_6, p_7, p_8]

        t_1 = udom_modeling_msgs.msg.MeshTetrahedron([0, 2, 1, 4])
        t_2 = udom_modeling_msgs.msg.MeshTetrahedron([6, 5, 1, 3])
        t_3 = udom_modeling_msgs.msg.MeshTetrahedron([6, 7, 5, 3])
        t_4 = udom_modeling_msgs.msg.MeshTetrahedron([3, 5, 1, 2])
        mesh_in.tetrahedra = [t_1, t_2, t_3, t_4]

        while not self.wait_for_result:
            self.mesh_in.publish(mesh_in)
            self.offset.publish(offset)
            self.event_out.publish('e_start')

        self.assertIsInstance(self.result, udom_modeling_msgs.msg.Mesh)

    def result_callback(self, msg):
        self.result = msg
        self.wait_for_result = True


if __name__ == '__main__':
    rospy.init_node('mesh_shifter_test')
    rostest.rosrun(PKG, 'mesh_shifter_test', TestMeshShifter)
