#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
Integration test for the 'interpolator' node.

"""

import numpy as np
import rospy
import unittest
import rostest
import std_msgs.msg
import sensor_msgs.msg

PKG = 'udom_geometric_transformation'


class TestInterpolator(unittest.TestCase):
    def setUp(self):
        """
        Sets up the test fixture before exercising it.

        """
        # params
        self.result = None
        self.wait_for_result = None

        # publishers
        self.target = rospy.Publisher('~target', std_msgs.msg.Float64, queue_size=1)
        self.joint_states = rospy.Publisher(
            '~joint_states', sensor_msgs.msg.JointState, queue_size=1)

        # subscribers
        self.component_output = rospy.Subscriber(
            '~component_output', std_msgs.msg.Float64, self.result_callback)

    def tearDown(self):
        """
        Deconstructs the test fixture after testing it.

        """
        self.component_output.unregister()
        self.target.unregister()
        self.joint_states.unregister()

    def test_interpolator_node(self):
        """
        Verifies the node's interface is correct.
        Note: this is not a functionality test.

        """
        target = std_msgs.msg.Float64()
        target.data = 10.0

        joint_states = sensor_msgs.msg.JointState()
        joint_states.name = [
            'rh_LFJ1', 'rh_LFJ2', 'rh_LFJ3', 'rh_LFJ4', 'rh_LFJ5', 'rh_FFJ4',
            'rh_FFJ3', 'rh_FFJ2', 'rh_MFJ1', 'rh_MFJ2', 'rh_MFJ3']
        joint_states.position = np.zeros(len(joint_states.name))
        # Testing for the first finger joint 2 (FFJ2)
        joint_states.position[7] = 0.05

        while not self.wait_for_result:
            self.target.publish(target)
            self.joint_states.publish(joint_states)

        self.assertIsInstance(self.result, std_msgs.msg.Float64)
        self.assertGreater(self.result.data, joint_states.position[7])
        self.assertLess(self.result.data, target.data)

    def result_callback(self, msg):
        self.result = msg
        self.wait_for_result = True


if __name__ == '__main__':
    rospy.init_node('interpolator_test')
    rostest.rosrun(PKG, 'interpolator_test', TestInterpolator)
