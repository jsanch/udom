#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
This node linearly interpolates between a target and current point based on the
step size. The current point is extracted from the joint_states topic.
It publishes each interpolated point until a new target is specified.
Once a new target is specified it interpolates from the current point to the
new target.

**Input(s):**

  * `target`: Target point.

    - *type:* `std_msgs/Float64`

  * `joint_states`: Joint states of the robot.

    - *type:* `sensor_msgs/JointState`

**Output(s):**

  * `result`: The interpolated point, one at a time, starting from the closest one
    from the current point.

    - *type:* `std_msgs/Float64`

**Parameter(s):**

  * `loop_rate`: Node cycle rate (in Hz).

  * `step`: Step size of the interpolation.

  * `joint_name`: Joint name to extract the current position.

"""

import numpy as np
import rospy
import std_msgs.msg
import sensor_msgs.msg
import udom_grasp_control.finger_ik_utils as utils


class InterpolatorNode(object):
    """
    Subscribes to two Float messages and publishes the interpolated poses
    between them one by one.

    """
    def __init__(self):
        """
        Instantiates an interpolator node.

        :return: Node to interpolate between two points.
        :rtype: InterpolatorNode

        """
        # Params
        self.current = None
        self.target = None

        # Node cycle rate (in Hz).
        self.loop_rate = rospy.Rate(rospy.get_param('~loop_rate', 10))

        # Step size of the interpolation.
        self.step = rospy.get_param('~step', 0.1)

        # Threshold error to stop publishing.
        self.threshold = rospy.get_param('~step', 0.018)

        # Joint name to extract the current position.
        self.joint_name = rospy.get_param('~joint_name', None)
        assert self.joint_name is not None, "'joint_name' must be specified."

        # Publishers
        self.result = rospy.Publisher(
            "~result", std_msgs.msg.Float64, queue_size=1, tcp_nodelay=True)
        self.current_pub = rospy.Publisher(
            "~current", std_msgs.msg.Float64, queue_size=1, tcp_nodelay=True)

        # Subscribers
        rospy.Subscriber('~target', std_msgs.msg.Float64, self.target_cb)
        rospy.Subscriber('~joint_states', sensor_msgs.msg.JointState, self.joint_states_cb)

    def target_cb(self, msg):
        """
        Obtains the target point.

        :param msg: Target point.
        :type msg: std_msgs.msg.Float64

        """
        self.target = msg

    def joint_states_cb(self, msg):
        """
        Obtains the current point based on the joint states of the robot.

        :param msg: Current point.
        :type msg: std_msgs.msg.Float64

        """
        self.current = std_msgs.msg.Float64(
            utils.extract_positions(msg, [self.joint_name])[0])

    def start(self):
        """
        Starts the node.

        """
        rospy.loginfo("Ready to start...")
        state = 'IDLE'

        while not rospy.is_shutdown():

            if state == 'IDLE':
                state = self.idle_state()
            elif state == 'RUNNING':
                state = self.running_state()

            rospy.logdebug("State: {0}".format(state))
            self.loop_rate.sleep()

    def idle_state(self):
        """
        Executes the IDLE state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        if (self.target is not None) and (self.current is not None):
            return 'RUNNING'
        else:
            return 'IDLE'

    def running_state(self):
        """
        Executes the RUNNING state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        points = self.interpolate(self.current, self.target)
        target = self.target.data
        self.target = None
        if points:
            for point in points:
                # Stop if the error is too small.
                if abs(point - target) <= self.threshold:
                    self.result.publish(point)
                    self.current_pub.publish(self.current)
                    self.current = None
                    return 'IDLE'
                self.result.publish(point)
                self.current_pub.publish(self.current)
                self.loop_rate.sleep()
        else:
            rospy.logwarn("Error while interpolating.")
        return 'IDLE'

    def interpolate(self, current, target, max_weight=3.0):
        """
        Interpolates between the current and target points based on the
        step size.

        :param current: Current point.
        :type current: std_msgs.msg.Float64

        :param target: Target point.
        :type target: std_msgs.msg.Float64

        :return: The interpolated points.
        :rtype: list

        """
        c = current.data
        t = target.data
        if t - c >= 0:
            points = list(np.arange(c, t, self.step))
        else:
            points = list(np.arange(c, t, -self.step))

        n = len(points)
        if n < 0:
            return [t]

        x = np.tanh(np.linspace(0, max_weight, n))

        min_ = min(points)
        max_ = max(points)
        scale = float(max_ - min_)

        if scale < 1e-6:
            scale = 1.0

        normalized_points = (points - min_) / scale
        points = list((normalized_points * x ) * scale + min_)
        points.append(t)
        return points

    def interpolate_linear(self, current, target):
        """
        Interpolates between the current and target points based on the
        step size.

        :param current: Current point.
        :type current: std_msgs.msg.Float64

        :param target: Target point.
        :type target: std_msgs.msg.Float64

        :return: The interpolated points.
        :rtype: list

        """
        c = current.data
        t = target.data
        if t - c >= 0:
            points = list(np.arange(current.data, target.data, self.step)[1:])
        else:
            points = list(np.arange(current.data, target.data, -self.step)[1:])

        points.append(target.data)

        return points


def main():
    rospy.init_node("interpolator_node", anonymous=True)
    interpolator_node = InterpolatorNode()
    interpolator_node.start()
