#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
This node computes forces based on the distance between two meshes. One mesh is the
current estimated mesh (mesh) and the second mesh is a mesh extracted based on position
information. If the second mesh (mesh_position) is smaller than the estimated mesh, then
the nodes of the mesh_position are used as the contact nodes (e.g. where the forces is
going to be applied).

**Input(s):**

  * `event_in`: The desired event for the node:

      `e_start`: starts the component.

      `e_stop`: stops the component.

    - *type:* `std_msgs/String`

  * `mesh`: Current estimated mesh.

    - *type:* `udom_modeling_msgs/Mesh`

  * `mesh_position`: Mesh obtained based on position information (e.g. forward kinematics
    of a robot grasping the object).

    - *type:* `udom_modeling_msgs/Mesh`

**Output(s):**

  * `forces`: Forces computed based on the distance between the meshes' nodes.

    - *type:* `std_msgs/Float32MultiArray`

  * `event_out`: The current event of the node.

      `e_running`: when the component is running.

      `e_stopped`: when the component is stopped.

    - *type:* `std_msgs/String`

**Parameter(s):**

  * `loop_rate`: Node cycle rate (in Hz).

  * `contact_nodes`: Nodes to where the forces are applied.

  * `p_gain`: Proportional gain to compute the forces based on the distance error.

  * `d_gain`: Derivative gain to compute the forces based on the distance error.

  * `i_gain`: Integral gain to compute the forces based on the distance error.

"""

import numpy as np
import rospy
from dynamic_reconfigure.server import Server
import std_msgs.msg
import udom_modeling_msgs.msg
import udom_geometric_transformation.mesh_shifter_utils as utils
from udom_geometric_transformation.cfg import ControllerGainsConfig as ControllerGains


class PositionBasedForceCalculatorNode(object):
    """
    Computes the external forces necessary to move the estimated mesh to match the
    mesh based on position information.

    """
    def __init__(self):
        """
        Instantiates a position based force calculator node.

        :return: Forces computed based on the two meshes.
        :rtype: PositionBasedForceCalculatorNode

        """
        # Params
        self.event = None
        self.mesh = None
        self.mesh_position = None

        # Nodes to where the forces are applied.
        self.contact_nodes = rospy.get_param('~contact_nodes', [])

        # Proportional, derivative and integral gain to compute the forces
        # based on the distance error.
        self.p_gain = rospy.get_param('~p_gain', 1.0)
        self.d_gain = rospy.get_param('~d_gain', 0.0)
        self.i_gain = rospy.get_param('~i_gain', 0.0)

        # Node cycle rate (in Hz).
        rate = rospy.get_param('~loop_rate', 100)
        self.loop_rate = rospy.Rate(rate)

        # Previous error used to compute the derivative error.
        self.previous_error = None
        # Cumulative error to compute the integral error.
        self.cumulative_error = None

        # Sample time used to compute the derivative error.
        self.dt = 1.0 / rate

        # Publishers
        self.event_out = rospy.Publisher("~event_out", std_msgs.msg.String, queue_size=10)
        self.forces = rospy.Publisher(
            "~forces", std_msgs.msg.Float32MultiArray, queue_size=1, tcp_nodelay=True)

        # Subscribers
        rospy.Subscriber("~event_in", std_msgs.msg.String, self.event_in_cb)
        rospy.Subscriber('~mesh', udom_modeling_msgs.msg.Mesh, self.mesh_cb)
        rospy.Subscriber(
            '~mesh_position', udom_modeling_msgs.msg.Mesh, self.mesh_position_cb)

        # Dynamic Server Reconfiguration
        dynamic_reconfig_srv = Server(ControllerGains, self.dynamic_reconfig_cb)

    def dynamic_reconfig_cb(self, config, level):
        """
        Reconfigures the controller gains.

        """
        self.p_gain = config.p_gain
        self.d_gain = config.d_gain
        self.i_gain = config.i_gain
        return config

    def event_in_cb(self, msg):
        """
        Obtains the event for the node (e.g. start, stop).

        :param msg: Event message for the node.
        :type msg: std_msgs.msg.String

        """
        self.event = msg.data

    def mesh_cb(self, msg):
        """
        Obtains the current estimated mesh.

        :param msg: Current estimated mesh.
        :type msg: udom_modeling_msgs.msg.Mesh

        """
        self.mesh = msg

    def mesh_position_cb(self, msg):
        """
        Obtains the mesh based on position information.

        :param msg: Mesh based on position information.
        :type msg: udom_modeling_msgs.msg.Mesh

        """
        self.mesh_position = msg

    def start(self):
        """
        Starts the node.

        """
        rospy.loginfo("Ready to start...")
        state = 'INIT'

        while not rospy.is_shutdown():
            init_time = rospy.Time.now()
            if state == 'INIT':
                state = self.init_state()
            elif state == 'IDLE':
                state = self.idle_state()
            elif state == 'RUNNING':
                state = self.running_state()

            rospy.logdebug("State: {0}".format(state))
            self.loop_rate.sleep()
            dt = rospy.Time.now() - init_time
            self.dt = dt.secs + dt.nsecs / 1e9

    def init_state(self):
        """
        Executes the INIT state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        if self.event == 'e_start':
            return 'IDLE'
        else:
            return 'INIT'

    def idle_state(self):
        """
        Executes the IDLE state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        if self.event == 'e_stop':
            self.event_out.publish('e_stopped')
            self.reset_component_data()
            return 'INIT'
        elif (self.mesh is not None) and (self.mesh_position is not None):
            return 'RUNNING'
        else:
            return 'IDLE'

    def running_state(self):
        """
        Executes the RUNNING state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        if self.event == 'e_stop':
            self.event_out.publish('e_stopped')
            self.reset_component_data()
            return 'INIT'
        else:
            forces = self.compute_forces()
            self.event_out.publish('e_running')
            self.forces.publish(forces)
            self.reset_component_data()
            return 'IDLE'

    def compute_forces(self):
        """
        Computes the forces based on the distance of the nodes between each mesh.

        :return: The forces based on the position information.
        :rtype: std_msgs/Float32MultiArray

        """
        error = utils.compute_nodal_distance(
            self.mesh, self.mesh_position, self.contact_nodes)
        p_error = error.data

        if self.previous_error is None:
            d_error = p_error
        else:
            d_error = (p_error - self.previous_error) / self.dt

        # Update previous error
        self.previous_error = p_error

        if self.cumulative_error is None:
            self.cumulative_error = np.zeros(len(p_error))
        else:
            self.cumulative_error += p_error * self.dt

        error.data = (self.p_gain * p_error) + (self.d_gain * d_error) \
            + (self.i_gain * self.cumulative_error)
        return error

    def reset_component_data(self):
        """
        Clears the data of the component.

        """
        self.mesh = None
        self.mesh_position = None
        self.event = None


def main():
    rospy.init_node("position_based_force_calculator_node", anonymous=True)
    position_based_force_calculator_node = PositionBasedForceCalculatorNode()
    position_based_force_calculator_node.start()
