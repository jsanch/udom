#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
This modules defines utility functions for the mesh measurer node.


"""

import numpy as np
import scipy.linalg as la


# I copied this code from a updated version of SciPy
def orthogonal_procrustes(A, B, check_finite=True):
    if check_finite:
        A = np.asarray_chkfinite(A)
        B = np.asarray_chkfinite(B)
    else:
        A = np.asanyarray(A)
        B = np.asanyarray(B)
    if A.ndim != 2:
        raise ValueError('expected ndim to be 2, but observed %s' % A.ndim)
    if A.shape != B.shape:
        raise ValueError('the shapes of A and B differ (%s vs %s)' % (
            A.shape, B.shape))
    # Be clever with transposes, with the intention to save memory.
    u, w, vt = la.svd(B.T.dot(A).T)
    R = u.dot(vt)
    scale = w.sum()
    return R, scale


# I copied this code from a updated version of SciPy
def procrustes(data1, data2):
    mtx1 = np.array(data1, dtype=np.double, copy=True)
    mtx2 = np.array(data2, dtype=np.double, copy=True)

    if mtx1.ndim != 2 or mtx2.ndim != 2:
        raise ValueError("Input matrices must be two-dimensional")
    if mtx1.shape != mtx2.shape:
        raise ValueError("Input matrices must be of same shape")
    if mtx1.size == 0:
        raise ValueError("Input matrices must be >0 rows and >0 cols")

    # translate all the data to the origin
    mtx1 -= np.mean(mtx1, 0)
    mtx2 -= np.mean(mtx2, 0)

    norm1 = np.linalg.norm(mtx1)
    norm2 = np.linalg.norm(mtx2)

    if norm1 == 0 or norm2 == 0:
        raise ValueError("Input matrices must contain >1 unique points")

    # change scaling of data (in rows) such that trace(mtx*mtx') = 1
    mtx1 /= norm1
    mtx2 /= norm2

    # transform mtx2 to minimize disparity
    R, s = orthogonal_procrustes(mtx1, mtx2)
    mtx2 = np.dot(mtx2, R.T) * s

    # measure the dissimilarity between the two datasets
    disparity = np.sum(np.square(mtx1 - mtx2))

    return mtx1, mtx2, disparity


def mesh_to_points(mesh):
    """
    Extracts the nodes' positions from the mesh and returns them as a numpy.ndarray
    with the shape (N, 3) where N is the number of vertices in the mesh.

    :param mesh: The mesh from which the set of points will be extracted.
    :type mesh: udom_modeling_msgs.msg.Mesh

    :return: The extracted nodes' positions of the mesh.
    :rtype: numpy.ndarray

    """
    points = [np.array([p.x, p.y, p.z]) for p in mesh.vertices]
    return np.array(points)


def hausdorff_distance(u, v):
    """
    Computes the Hausdorff distance using the EarlyBreak algorithm.

    :param u: Set of points.
    :type u: numpy.ndarray

    :param v: Set of points.
    :type v: numpy.ndarray

    :return: The Hausdorff distance between the two sets of points.
    :rtype: float

    """
    # This function is only available in an updated version of scipy(spatial.distance)
    raise NotImplementedError
    return max(sp_distance.directed_hausdorff(u, v)[0],
               sp_distance.directed_hausdorff(v, u)[0])


def procrustes_distance(u, v):
    """
    Computes the Procrustes distance between the two set of points.

    :param u: Set of points.
    :type u: numpy.ndarray

    :param v: Set of points.
    :type v: numpy.ndarray

    :return: The Procrustes distance between the two sets of points.
    :rtype: float

    """
    return procrustes(u, v)[2]


def compute_A_pq(p, q):
    """
    Computes the A_pq matrix as defined by Eq. (7) in [1].

    :param p: p matrix. It is assumed to have a (n, 3) shape, where 'n'
        is the number of nodes.
    :type p: np.ndarray

    :param q: q matrix. It is assumed to have a (n, 3) shape, where 'n'
        is the number of nodes.
    :type q: np.ndarray

    :return: A_pq matrix. Has a (3, 3) shape.
    :rtype: np.ndarray

    """
    return np.sum((
        np.dot(p_i[np.newaxis].T, q_i[np.newaxis]) for p_i, q_i in zip(p, q)), 0)


def compute_A_qq(q):
    """
    Computes the A_pq matrix as defined by Eq. (7) in [1].

    :param q: q matrix. It is assumed to have a (n, 3) shape, where 'n'
        is the number of nodes.
    :type q: np.ndarray

    :return: A_qq matrix. Has a (3, 3) shape.
    :rtype: np.ndarray

    """
    return np.sum((
        np.dot(q_i[np.newaxis].T, q_i[np.newaxis]) for q_i in q), 0)


def compute_q_tilde(q):
    """
    Computes the q_tilde array for each point in q as a matrix.

    :param q: q matrix. It is assumed to have a (n, 3) shape, where 'n'
        is the number of nodes.
    :type q: np.ndarray

    :return: q_tilde matrix. Has a (n, 9) shape.
    :rtype: np.ndarray

    """
    q_tilde = []
    for q_i in q:
        q_i_tilde = np.array([
            q_i[0], q_i[1], q_i[2], q_i[0]**2, q_i[1]**2, q_i[2]**2,
            q_i[0] * q_i[1], q_i[1] * q_i[2], q_i[2]* q_i[0]])
        q_tilde.append(q_i_tilde)

    return np.array(q_tilde)


def match_shapes(rest, target, b_param=0.5, quadratic=True):
    """
    Generate a shape between the initial shape (rest) and the target shape
    based on the algorithm proposed in [1].

    :param rest: Initial mesh. It is assumed to have a (n, 3) shape, where 'n'
        is the number of nodes.
    :type rest: np.ndarray

    :param target: Target mesh. It is assumed to have a (n, 3) shape, where 'n'
        is the number of nodes.
    :type target: np.ndarray

    :param b_param: Control parameter beta (0.0 - 1.0). A value of 0 gives the
        rigid transformation.
    :type b_param: float

    :param quadratic: If specified computes a quadratic deformation. It requires
        'b_param' to be specified.
    :type quadratic: bool

    :return: The goal shape between the initial and final meshes.
    :rtype: np.ndarray

    """
    if quadratic:
        assert b_param is not None, "'b_param' must be specified (0.0- 1.0)."

    centroid_rest = np.mean(rest, axis=0)
    centroid_target = np.mean(target, axis=0)

    q = rest - centroid_rest
    p = target - centroid_target
    A_pq = compute_A_pq(p, q)

    # Compute optimal rotation using the Kabsch algorithm.
    u, s, vt = la.svd(A_pq)
    d = la.det(vt.dot(u.T))

    d_matrix = np.identity(3)
    d_matrix[2, 2] = d

    R = np.dot(vt, np.dot(d_matrix, u.T))

    if b_param is None:
        return np.dot(R, (rest - centroid_rest).T).T + centroid_target
    elif not quadratic:
        A_qq = compute_A_qq(q)
        A = np.dot(A_pq, la.inv(A_qq))
        R_lin = b_param * A + (1.0 - b_param) * R
        return np.dot(R_lin, q.T).T + centroid_target
    else:
        q_tilde = compute_q_tilde(q)
        # TODO: See if we need new function or just new q_tilde
        A_pq_tilde = compute_A_pq(p, q_tilde)
        A_qq_tilde = compute_A_qq(q_tilde)
        A_tilde = np.dot(A_pq_tilde, la.inv(A_qq_tilde))
        R_tilde = np.concatenate((R, np.zeros((3, 3)), np.zeros((3, 3)))).T
        R_quad = b_param * A_tilde + (1.0 - b_param) * R_tilde

        return np.dot(R_quad, q_tilde.T).T + centroid_target
