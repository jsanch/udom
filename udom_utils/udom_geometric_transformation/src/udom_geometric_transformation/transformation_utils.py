#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
This modules defines a class and functions to perform transformations such as
wrench transforms.


"""

import numpy as np
import rospy
import tf.transformations
import tf2_ros
import geometry_msgs.msg
# Needed for tf2
from tf2_geometry_msgs import PointStamped


class GeometryTransformer:
    """
    Subclass of TransformListener that provides transformation methods.

    """
    def __init__(self):
        """
        Initializes a TransformListener object.

        :return: A transform listener object.
        :rtype: GeometryTransformer

        """
        self.buffer = tf2_ros.Buffer()
        self.listener = tf2_ros.TransformListener(self.buffer)

    def rotate_wrench(self, wrench, wrench_header, target_frame, duration=5.0):
        """
        Rotates the wrench from the frame specified in its header to the target_frame
        at the time specified by the header.
        Throws an exception if the rotation failed.

        :param wrench: The wrench to be rotated.
        :type wrench: geometry_msgs.msg.Wrench

        :param wrench_header: The header of the wrench to be rotated.
        :type wrench_header: std_msgs.msg.Header

        :param target_frame: The frame into which the wrench should be rotated.
        :type target_frame: str

        :param duration: Maximum duration to wait for the transform (in seconds).
        :type duration: float

        :return: The transformed wrench.
        :rtype: geometry_msgs.msg.Wrench

        """
        transform = self.buffer.lookup_transform(
            target_frame, wrench_header.frame_id, wrench_header.stamp,
            timeout=rospy.Duration(duration))

        # get matrix from quaternion (transformations.quaternion_matrix
        rotation_matrix = tf.transformations.quaternion_matrix([
            transform.transform.rotation.x, transform.transform.rotation.y,
            transform.transform.rotation.z, transform.transform.rotation.w])[0:3, 0:3]

        wrench_out = rotate_wrench(wrench, rotation_matrix)

        return wrench_out

    def rotate_twist(self, twist, twist_header, target_frame, duration=5.0):
        """
        Rotates the twist from the frame specified in its header to the target_frame
        at the time specified by the header.
        Throws an exception if the rotation failed.

        :param twist: The wrench to be rotated.
        :type twist: geometry_msgs.msg.Twist

        :param twist_header: The header of the twist to be rotated.
        :type twist_header: std_msgs.msg.Header

        :param target_frame: The frame into which the twist should be rotated.
        :type target_frame: str

        :param duration: Maximum duration to wait for the transform (in seconds).
        :type duration: float

        :return: The transformed twist.
        :rtype: geometry_msgs.msg.Twist

        """
        transform = self.buffer.lookup_transform(
            target_frame, twist_header.frame_id, twist_header.stamp,
            timeout=rospy.Duration(duration))

        # get matrix from quaternion (transformations.quaternion_matrix
        rotation_matrix = tf.transformations.quaternion_matrix([
            transform.transform.rotation.x, transform.transform.rotation.y,
            transform.transform.rotation.z, transform.transform.rotation.w])[0:3, 0:3]

        twist_out = rotate_twist(twist, rotation_matrix)

        return twist_out

    def transform_wrench(self, wrench, wrench_header, target_frame, point=None, duration=5.0):
        """
        Transforms the wrench from the frame specified in its header to the target_frame
        at the time specified by the header.
        Throws an exception if the transformation failed.

        :param wrench: The wrench to be transformed.
        :type wrench: geometry_msgs.msg.Wrench

        :param wrench_header: The header of the wrench to be transformed.
        :type wrench_header: std_msgs.msg.Header

        :param target_frame: The frame into which the wrench should be transformed.
        :type target_frame: str

        :param point: The position where the wrench is applied with respect to the wrench's
            reference frame.
        :type point: tuple

        :param duration: Maximum duration to wait for the transform (in seconds).
        :type duration: float

        :return: The transformed wrench.
        :rtype: geometry_msgs.msg.Wrench

        """
        transform = self.buffer.lookup_transform(
            target_frame, wrench_header.frame_id, wrench_header.stamp,
            timeout=rospy.Duration(duration))

        transform_matrix = np.zeros((4, 4), dtype=float)
        transform_matrix[3, 3] = 1.0
        transform_matrix[0:3, 3] = [
            transform.transform.translation.x, transform.transform.translation.y,
            transform.transform.translation.z]
        # get matrix from quaternion (transformations.quaternion_matrix
        transform_matrix[0:3, 0:3] = tf.transformations.quaternion_matrix([
            transform.transform.rotation.x, transform.transform.rotation.y,
            transform.transform.rotation.z, transform.transform.rotation.w])[0:3, 0:3]

        wrench_out = transform_wrench(wrench, transform_matrix, point)

        return wrench_out

    def transform_point(self, point, point_header, target_frame, duration=5.0):
        """
        Transform the point from the frame specified in its header to the target_frame
        at the time specified by the header.
        Throws an exception if the transformation failed.

        :param point: The point to be transformed.
        :type point: geometry_msgs.msg.Point

        :param point_header: The header of the point to be transformed.
        :type point_header: std_msgs.msg.Header

        :param target_frame: The frame into which the point should be transformed.
        :type target_frame: str

        :param duration: Maximum duration to wait for the transform (in seconds).
        :type duration: float

        :return: The transformed point.
        :rtype: geometry_msgs.msg.Point

        """
        source_point = geometry_msgs.msg.PointStamped()
        source_point.header = point_header
        source_point.point = point

        point_out = self.buffer.transform(
            source_point, target_frame, timeout=rospy.Duration(duration))

        return point_out.point

    def transform_pose(self, pose, target_frame, duration=5.0):
        """
        Transform the pose to the target_frame.
        Throws an exception if the transformation failed.

        :param pose: The pose to be transformed.
        :type pose: geometry_msgs.msg.PoseStamped

        :param target_frame: The frame into which the pose should be transformed.
        :type target_frame: str

        :param duration: Maximum duration to wait for the transform (in seconds).
        :type duration: float

        :return: The transformed Pose.
        :rtype: geometry_msgs.msg.PoseStamped

        """
        pose_out = self.buffer.transform(
            pose, target_frame, timeout=rospy.Duration(duration))

        return pose_out

    def transform_twist(self, twist_in, target_frame='base_link', wait_for_transform=0.1):
        """
        Transforms the provided twist to the target frame.
        Raises an exception if the transformation failed.

        :param twist_in: The twist which should be transformed.
        :type twist_in: geometry_msgs.msg.TwistStamped

        :param target_frame: The frame into which the twist should be transformed.
        :type target_frame: str

        :param wait_for_transform: Amount of seconds to wait for the transform.
        :type wait_for_transform: float

        :return: The transformed twist.
        :rtype: geometry_msgs.msg.TwistStamped

        """
        twist_out = geometry_msgs.msg.TwistStamped()
        transform = self.buffer.lookup_transform(
            target_frame, twist_in.header.frame_id, rospy.Time(),
            timeout=rospy.Duration(wait_for_transform))

        transform_matrix = np.zeros((4, 4), dtype=float)
        transform_matrix[3, 3] = 1.0
        transform_matrix[0:3, 3] = [
            transform.transform.translation.x, transform.transform.translation.y,
            transform.transform.translation.z]
        # get matrix from quaternion (transformations.quaternion_matrix
        transform_matrix[0:3, 0:3] = tf.transformations.quaternion_matrix([
            transform.transform.rotation.x, transform.transform.rotation.y,
            transform.transform.rotation.z, transform.transform.rotation.w])[0:3, 0:3]

        transformed_twist = transform_twist(twist_in.twist, transform_matrix)

        twist_out.twist = transformed_twist
        twist_out.header.frame_id = target_frame
        twist_out.header.stamp = twist_in.header.stamp

        return twist_out

    def get_transform_matrix(self, source_frame, target_frame, wait_for_transform=0.1):
        """
        Computes the transformation matrix from the source_frame to the target_frame.
        Note: This is the transform to get data from target_frame into the source_frame.

        :param source_frame: Source frame.
        :type target_frame: str

        :param target_frame: Target frame.
        :type target_frame: str

        :param wait_for_transform: Amount of seconds to wait for the transform.
        :type wait_for_transform: float

        :return: Transformation matrix.
        :rtype: np.array

        """
        transform = self.buffer.lookup_transform(
            source_frame, target_frame, rospy.Time.now(),
            timeout=rospy.Duration(wait_for_transform))

        transform_matrix = np.zeros((4, 4), dtype=float)
        transform_matrix[3, 3] = 1.0
        transform_matrix[0:3, 3] = [
            transform.transform.translation.x, transform.transform.translation.y,
            transform.transform.translation.z]
        transform_matrix[0:3, 0:3] = tf.transformations.quaternion_matrix([
            transform.transform.rotation.x, transform.transform.rotation.y,
            transform.transform.rotation.z, transform.transform.rotation.w])[0:3, 0:3]

        return transform_matrix


def rotate_wrench(wrench_in, rotation_matrix):
    """
    Applies a rotation to a wrench. It is assumed that the reference point and
    reference frame are collapsed into a single coordinate frame. (See also
    http://www.ros.org/wiki/tf/Reviews/2010-03-12_API_Review).

    :param wrench_in: The wrench to which the rotation should be applied.
    :type wrench_in: geometry_msgs.msg.Wrench

    :param rotation_matrix: A 3x3 rotation matrix that should be applied to the wrench.
    :type rotation_matrix: numpy.array

    :return: The transformed wrench.
    :rtype: geometry_msgs.msg.Wrench

    """
    wrench_out = geometry_msgs.msg.Wrench()

    force_in = np.array([wrench_in.force.x, wrench_in.force.y, wrench_in.force.z])
    torque_in = np.array([wrench_in.torque.x, wrench_in.torque.y, wrench_in.torque.z])

    force_out = np.dot(rotation_matrix, force_in)
    torque_out = np.dot(rotation_matrix, torque_in)

    wrench_out.force.x, wrench_out.force.y, wrench_out.force.z = force_out
    wrench_out.torque.x, wrench_out.torque.y, wrench_out.torque.z = torque_out

    return wrench_out


def rotate_twist(twist_in, rotation_matrix):
    """
    Applies a rotation to a twist. It is assumed that the reference point and
    reference frame are collapsed into a single coordinate frame. (See also
    http://www.ros.org/wiki/tf/Reviews/2010-03-12_API_Review).

    :param twist_in: The twist to which the rotation should be applied.
    :type twist_in: geometry_msgs.msg.Twist

    :param rotation_matrix: A 3x3 rotation matrix that should be applied to the twist.
    :type rotation_matrix: numpy.array

    :return: The transformed twist.
    :rtype: geometry_msgs.msg.Twist

    """
    twist_out = geometry_msgs.msg.Twist()

    linear_in = np.array([twist_in.linear.x, twist_in.linear.y, twist_in.linear.z])
    angular_in = np.array([twist_in.angular.x, twist_in.angular.y, twist_in.angular.z])

    linear_out = np.dot(rotation_matrix, linear_in)
    angular_out = np.dot(rotation_matrix, angular_in)

    twist_out.linear.x, twist_out.linear.y, twist_out.linear.z = linear_out
    twist_out.angular.x, twist_out.angular.y, twist_out.angular.z = angular_out

    return twist_out


def transform_wrench(wrench_in, transform, point=None):
    """
    Applies a transform to a wrench. It is assumed that the reference point and
    reference frame are collapsed into a single coordinate frame. (See also
    http://www.ros.org/wiki/tf/Reviews/2010-03-12_API_Review).

    :param wrench_in: The wrench to which the transform should be applied.
    :type wrench_in: geometry_msgs.msg.Wrench

    :param transform: A 4x4 transformation matrix that should be applied to the wrench.
    :type transform: numpy.array

    :param point: The position where the wrench is applied with respect to the wrench's
        reference frame.
    :type point: tuple

    :return: The transformed wrench.
    :rtype: geometry_msgs.msg.Wrench

    """
    wrench_out = geometry_msgs.msg.Wrench()

    rotation_matrix = transform[0:3, 0:3]
    translation = transform[0:3, 3]

    if point is not None:
        assert isinstance(point, tuple) and (len(point) == 3), \
            "'point' must be a three element tuple, not '{}'".format(point)
        translation += np.dot(rotation_matrix, np.array(point, dtype=np.float32))

    force_in = np.array([wrench_in.force.x, wrench_in.force.y, wrench_in.force.z])
    torque_in = np.array([wrench_in.torque.x, wrench_in.torque.y, wrench_in.torque.z])

    force_out = np.dot(rotation_matrix, force_in)
    torque_out = np.dot(rotation_matrix, torque_in) + np.cross(translation, force_out)

    wrench_out.force.x, wrench_out.force.y, wrench_out.force.z = force_out
    wrench_out.torque.x, wrench_out.torque.y, wrench_out.torque.z = torque_out

    return wrench_out


def quaternion_rotation(vector, quat):
    """
    Rotates a vector by a quaternion.
    Note: Quaternions w+ix+jy+kz are represented as [w, x, y, z].

    :param vector: Vector to rotate.
    :type vector: list

    :param quat: Quaternion.
    :type quat: geometry_msgs.msg.Quaternion

    :return: Rotated vector.
    :rtype: numpy.array

    """
    quaternion = np.array([
        [(1 - 2*quat.y**2 - 2*quat.z**2), 2*(quat.x*quat.y + quat.w*quat.z),
         2*(quat.x*quat.z - quat.w*quat.y)],
        [2*(quat.x*quat.y - quat.w*quat.z), (1 - 2*quat.x**2 - 2*quat.z**2),
         2*(quat.y*quat.z + quat.w*quat.x)],
        [2*(quat.x*quat.z + quat.w*quat.y), 2*(quat.y*quat.z - quat.w*quat.x),
         (1 - 2*quat.x**2 - 2*quat.y**2)]])

    return np.dot(quaternion, np.array(vector))


def transform_twist(twist, transform):
    """"
    Apply a transform to a twist. It is assumed that the reference point and
    reference frame are collapsed into a single coordinate frame. (See also
    http://www.ros.org/wiki/tf/Reviews/2010-03-12_API_Review)

    :param twist: The twist to which the transform should be applied.
    :type twist: geometry_msgs.msg.Twist

    :param transform: The desired transform that should be applied.
    :type transform: numpy.matrix[4][4]

    :return: The transformed twist.
    :rtype: geometry_msgs.msg.Twist

    """
    twist_out = geometry_msgs.msg.Twist()

    linear_velocity = np.array([
        twist.linear.x, twist.linear.y, twist.linear.z])
    angular_velocity = np.array([
        twist.angular.x, twist.angular.y, twist.angular.z])

    M = transform[0:3, 0:3]
    p = transform[0:3, 3]

    transformed_angular_velocity = np.dot(M, angular_velocity)
    transformed_linear_velocity = \
        np.dot(M, linear_velocity) + np.cross(p, transformed_angular_velocity)

    twist_out.linear.x = transformed_linear_velocity[0]
    twist_out.linear.y = transformed_linear_velocity[1]
    twist_out.linear.z = transformed_linear_velocity[2]
    twist_out.angular.x = transformed_angular_velocity[0]
    twist_out.angular.y = transformed_angular_velocity[1]
    twist_out.angular.z = transformed_angular_velocity[2]

    return twist_out


def triad_rotation(b, r):
    """
    Computes the rotation matrix between two frames (B and R) based
    on two vector observations on each frame.
    It uses the triad (TRIaxial Attitude Determination) method.

    :param b: Two vectors measured in the B frame.
    :type b: list(np.array)

    :param r: Two vectors measured in the R frame.
    :type r: list(np.array)

    :return: The rotation matrix
    :rtype: numpy.matrix[3][3]

    """
    r_1 = normalize(r[0])
    r_2 = normalize(r[1])

    b_1 = normalize(b[0])
    b_2 = normalize(b[1])

    v_1 = r_1[np.newaxis]
    v_2 = normalize(np.cross(v_1, r_2))
    v_3 = np.cross(v_1, v_2)

    w_1 = b_1[np.newaxis]
    w_2 = normalize(np.cross(w_1, b_2))
    w_3 = np.cross(w_1, w_2)

    rotation_matrix = np.dot(w_1.T, v_1) + np.dot(w_2.T, v_2) + np.dot(w_3.T, v_3)

    return rotation_matrix.reshape((3, 3))


def normalize(a, axis=-1, order=2):
    """
    Normalizes a vector. Taken from:
        https://stackoverflow.com/a/21032099/4788274

    """
    l2 = np.atleast_1d(np.linalg.norm(a, order, axis))
    l2[l2 == 0] = 1
    return a / np.expand_dims(l2, axis)
