#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
This node extracts the points (nodes) from a mesh given a set of the indices
of the desired nodes.

**Input(s):**

  * `mesh`: The mesh from which the pose will be extracted.
    - *type:* `udom_modeling_msgs/Mesh`

  * `event_in`: The desired event for the node:

      `e_start`: starts the component.

      `e_stop`: stops the component.

    - *type:* `std_msgs/String`

**Output(s):**

  * `points`: The extracted points as markers.
    - *type:* `visualization_msgs/Marker`

  * `event_out`: The current event of the node.

      `e_running`: when the component is running.

      `e_stopped`: when the component is stopped.

    - *type:* `std_msgs/String`

**Parameter(s):**

  * `loop_rate`: Node cycle rate (in Hz).

  * `nodes`: Indices for the nodes on the mesh to extract as positions.

  * `zero_based`: Whether the indices are specified on zero-based (True) or one-based (False).

"""

import rospy
import std_msgs.msg
import udom_modeling_msgs.msg
import visualization_msgs.msg
import udom_geometric_transformation.pose_extractor_utils as utils


class PointsExtractorNode(object):
    """
    Subscribes to a udom_modeling_msgs.msg.Mesh topic and extracts the positions of a
    given set of indices representing the nodes.
    It publishes a visualization_msgs/Marker message.

    """
    def __init__(self):
        """
        Instantiates a points extractor node.

        :return: Node to extract points from a mesh.
        :rtype: PointsExtractorNode

        """
        # Params
        self.event = None
        self.mesh = None
        self.points = visualization_msgs.msg.Marker()

        # Node cycle rate (in Hz).
        self.loop_rate = rospy.Rate(rospy.get_param('~loop_rate', 100))

        # Whether the indices are specified on zero-based or one-based.
        self.zero_based = rospy.get_param('~zero_based', True)

        self.nodes = rospy.get_param('~nodes', None)
        assert self.nodes is not None, "'nodes' must be specified."
        assert isinstance(self.nodes, list), "'nodes' must be a list, " \
                                             "not '{}'.".format(type(self.nodes))
        assert len(self.nodes) > 0, "'nodes' cannot be empty, ".format(len(self.nodes))

        # Publishers
        self.event_out = rospy.Publisher("~event_out", std_msgs.msg.String, queue_size=10)
        self.points_pub = rospy.Publisher(
            "~points", visualization_msgs.msg.Marker, queue_size=1, tcp_nodelay=True)

        # Subscribers
        rospy.Subscriber("~event_in", std_msgs.msg.String, self.event_in_cb)
        rospy.Subscriber('~mesh', udom_modeling_msgs.msg.Mesh, self.mesh_cb)

    def event_in_cb(self, msg):
        """
        Obtains the event for the node (e.g. start, stop).

        :param msg: Event message for the node.
        :type msg: std_msgs.msg.String

        """
        self.event = msg.data

    def mesh_cb(self, msg):
        """
        Obtains the mesh.

        :param msg: Mesh.
        :type msg: udom_modeling_msgs.msg.Mesh

        """
        self.mesh = msg

    def start(self):
        """
        Starts the node.

        """
        rospy.loginfo("Ready to start...")
        state = 'INIT'

        while not rospy.is_shutdown():

            if state == 'INIT':
                state = self.init_state()
            elif state == 'IDLE':
                state = self.idle_state()
            elif state == 'RUNNING':
                state = self.running_state()

            rospy.logdebug("State: {0}".format(state))
            self.loop_rate.sleep()

    def init_state(self):
        """
        Executes the INIT state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        if self.event == 'e_start':
            return 'IDLE'
        else:
            return 'INIT'

    def idle_state(self):
        """
        Executes the IDLE state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        if self.event == 'e_stop':
            self.event_out.publish('e_stopped')
            self.reset_component_data()
            return 'INIT'
        elif self.mesh is not None:
            return 'RUNNING'
        else:
            return 'IDLE'

    def running_state(self):
        """
        Executes the RUNNING state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        if self.event == 'e_stop':
            self.event_out.publish('e_stopped')
            self.reset_component_data()
            return 'INIT'
        else:
            self.points.points = self.extract_points(self.mesh)
            if self.points.points:
                self.event_out.publish('e_running')
                self.points_pub.publish(self.points)
            else:
                rospy.logwarn("Error while extracting the points.")
            self.reset_component_data()
            return 'IDLE'

    def extract_points(self, mesh):
        """
        Extracts the points from the mesh based on the specified nodes.

        :param mesh: The mesh from which the points will be extracted.
        :type mesh: udom_modeling_msgs.msg.Mesh

        :return: The extracted points, if there's an error it outputs None.
        :rtype: visualization_msgs.msg.Marker or None.

        """
        points = utils.extract_points(mesh, self.nodes, self.zero_based)
        if points is None:
            return None

        return points

    def reset_component_data(self):
        """
        Clears the data of the component.

        """
        self.mesh = None
        self.event = None


def main():
    rospy.init_node("points_extractor_node", anonymous=True)
    points_extractor_node = PointsExtractorNode()
    points_extractor_node.start()
