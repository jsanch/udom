#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
This node rotates every wrench in a udom_common_msgs.msg.ForceMultiArray into a single
message of type udom_common_msgs.msg.ForceArray with a single specified reference frame.

<sub>[**Note**: This node requires that a reference frame is specified and that it exists.]</sub>

**Input(s):**

  * `force_in`: The wrenches to be rotated.

    - *type:* `udom_common_msgs/ForceArray`

  * `event_in`: The desired event for the node:

      `e_start`: starts the component.

      `e_stop`: stops the component.

    - *type:* `std_msgs/String`

**Output(s):**

  * `force_out`: An array of wrench on which all elements are specified with respect to a
        single reference frame.

    - *type:* `udom_common_msgs/ForceArray`

  * `wrench_out`: The rotated wrench for visualization problems.

    - *type:* `geometry_msgs/WrenchStamped`

  * `event_out`: The current event of the node.

      `e_running`: when the component is running.

      `e_stopped`: when the component is stopped.

    - *type:* `std_msgs/String`

**Parameter(s):**

  * `loop_rate`: Node cycle rate (in Hz).

  * `wait_for_transform`: Maximum duration to wait for a transform (in seconds).

  * `reference_frame`: The wrenches will be described with respect to this frame.

"""

import rospy
import tf2_ros
import std_msgs.msg
import geometry_msgs.msg
import udom_common_msgs.msg
import udom_geometric_transformation.transformation_utils as transformation_utils


class WrenchRotatorNode(object):
    """
    Subscribes to a udom_common_msgs.msg.ForceArray topic and rotates each of the
    wrenches with respect to a single specified frame. It publishes a message of type
    udom_common_msgs.msg.ForceArray with all the wrenches.

    """
    def __init__(self):
        """
        Instantiates a wrench rotator node.

        :return: Node to transform forces into a single reference frame.
        :rtype: WrenchRotatorNode

        """
        # Params
        self.event = None
        self.force_info = None

        # Object to compute transformations.
        self.listener = transformation_utils.GeometryTransformer()

        # Node cycle rate (in Hz).
        self.loop_rate = rospy.Rate(rospy.get_param('~loop_rate', 10))

        # Maximum duration to wait for a transform (in seconds).
        self.wait_for_transform = rospy.get_param('~wait_for_transform', 0.1)

        # The forces will be described with respect to this frame.
        self.reference_frame = rospy.get_param("~reference_frame", None)
        assert self.reference_frame is not None, "A reference frame must be specified."

        # Publishers
        self.event_out = rospy.Publisher("~event_out", std_msgs.msg.String, queue_size=10)
        self.force_out = rospy.Publisher(
            "~force_out", udom_common_msgs.msg.ForceArray, queue_size=1, tcp_nodelay=True)
        self.wrench_out = rospy.Publisher(
            "~wrench_out", geometry_msgs.msg.WrenchStamped, queue_size=1, tcp_nodelay=True)

        # Subscribers
        rospy.Subscriber("~event_in", std_msgs.msg.String, self.event_in_cb)
        rospy.Subscriber('~force_in', udom_common_msgs.msg.ForceArray, self.force_in_cb)

    def event_in_cb(self, msg):
        """
        Obtains the event for the node (e.g. start, stop).

        :param msg: Event message for the node.
        :type msg: std_msgs.msg.String

        """
        self.event = msg.data

    def force_in_cb(self, msg):
        """
        Obtains the force information input.

        :param msg: Force information.
        :type msg: udom_common_msgs.msg.ForceMultiArray

        """
        self.force_info = msg

    def start(self):
        """
        Starts the node.

        """
        rospy.loginfo("Ready to start...")
        state = 'INIT'

        while not rospy.is_shutdown():

            if state == 'INIT':
                state = self.init_state()
            elif state == 'IDLE':
                state = self.idle_state()
            elif state == 'RUNNING':
                state = self.running_state()

            rospy.logdebug("State: {0}".format(state))
            self.loop_rate.sleep()

    def init_state(self):
        """
        Executes the INIT state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        if self.event == 'e_start':
            return 'IDLE'
        else:
            return 'INIT'

    def idle_state(self):
        """
        Executes the IDLE state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        if self.event == 'e_stop':
            self.event_out.publish('e_stopped')
            self.reset_component_data()
            return 'INIT'
        elif self.force_info is not None:
            return 'RUNNING'
        else:
            return 'IDLE'

    def running_state(self):
        """
        Executes the RUNNING state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        if self.event == 'e_stop':
            self.event_out.publish('e_stopped')
            self.reset_component_data()
            return 'INIT'
        else:
            try:
                force_out, wrench_out = self.rotate_wrench(self.force_info)
                self.event_out.publish('e_running')
                self.force_out.publish(force_out)
                self.wrench_out.publish(wrench_out)
            except (tf2_ros.LookupException, tf2_ros.ConnectivityException,
                    tf2_ros.ExtrapolationException) as e:
                rospy.logwarn("Error while transforming the message.\n{}".format(e))
            self.reset_component_data()
            return 'IDLE'

    def rotate_wrench(self, force_in):
        """
        Rotate the wrenches in the 'force_in' message to the specified reference frame.

        :param force_in: The force information to rotate.
        :type force_in: udom_common_msgs.msg.ForceArray

        :return: The rotated wrench as a force array and a wrench stamped message.
        :rtype: udom_common_msgs.msg.ForceArray and geometry_msgs.msg.WrenchStamped

        """
        force_out = udom_common_msgs.msg.ForceArray()
        wrench_out = geometry_msgs.msg.WrenchStamped()

        force_out.positions = [
            self.listener.transform_point(
                point, force_in.header, self.reference_frame, self.wait_for_transform)
            for point in force_in.positions]
        force_out.wrenches = [
            self.listener.rotate_wrench(
                wrench, force_in.header, self.reference_frame, self.wait_for_transform)
            for wrench in force_in.wrenches]

        # TODO: remove after debugging
        wrench_out.wrench = force_out.wrenches[0]
        wrench_out.wrench.force.x *= -1
        wrench_out.wrench.force.y *= -1
        wrench_out.wrench.force.z *= -1
        wrench_out.wrench.torque.x *= -1
        wrench_out.wrench.torque.y *= 1
        wrench_out.wrench.torque.z *= 1

        force_out.header.frame_id = self.reference_frame
        wrench_out.header.frame_id = self.reference_frame
        force_out.header.stamp = rospy.Time.now()
        wrench_out.header.stamp = rospy.Time.now()
        return force_out, wrench_out

    def reset_component_data(self):
        """
        Clears the data of the component.

        """
        self.force_info = None
        self.event = None


def main():
    rospy.init_node("force_transformer_node", anonymous=True)
    force_transformer_node = WrenchRotatorNode()
    force_transformer_node.start()
