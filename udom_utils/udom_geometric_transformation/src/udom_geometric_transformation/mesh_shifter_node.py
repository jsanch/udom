#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
This node shifts the contact nodes of a mesh by a Cartesian offset. If no
contact nodes are specified, the whole mesh is shifted.

**Input(s):**

  * `event_in`: The desired event for the node:

      `e_start`: starts the component.

      `e_stop`: stops the component.

    - *type:* `std_msgs/String`

  * `mesh_in`: Mesh to shift by an offset.

    - *type:* `udom_modeling_msgs/Mesh`

  * `offset`: Cartesian offset.

    - *type:* `udom_common_msgs/CartesianOffset`

**Output(s):**

  * `mesh_out`: Shifted mesh.

    - *type:* `udom_modeling_msgs/Mesh`

  * `event_out`: The current event of the node.

      `e_running`: when the component is running.

      `e_stopped`: when the component is stopped.

    - *type:* `std_msgs/String`

**Parameter(s):**

  * `loop_rate`: Node cycle rate (in Hz).

  * `contact_nodes`: Nodes to extract from the mesh to shift.

"""

import rospy
import std_msgs.msg
import udom_common_msgs.msg
import udom_modeling_msgs.msg
import udom_geometric_transformation.pose_extractor_utils as pe_utils
import udom_geometric_transformation.mesh_shifter_utils as ms_utils


class MeshShifterNode(object):
    """
    Shifts a mesh by a Cartesian offset.

    """
    def __init__(self):
        """
        Instantiates a mesh shifter node.

        :return: Shifted mesh.
        :rtype: MeshShifterNode

        """
        # Params
        self.event = None
        self.mesh_in = None
        self.offset = None

        # Shifted mesh.
        self.shifted_mesh = udom_modeling_msgs.msg.Mesh()

        # Nodes to extract from the mesh to shift.
        self.contact_nodes = rospy.get_param('~contact_nodes', [])

        # Node cycle rate (in Hz).
        self.loop_rate = rospy.Rate(rospy.get_param('~loop_rate', 100))

        # Publishers
        self.event_out = rospy.Publisher("~event_out", std_msgs.msg.String, queue_size=10)
        self.mesh_out = rospy.Publisher(
            "~mesh_out", udom_modeling_msgs.msg.Mesh, queue_size=1, tcp_nodelay=True)

        # Subscribers
        rospy.Subscriber("~event_in", std_msgs.msg.String, self.event_in_cb)
        rospy.Subscriber('~mesh_in', udom_modeling_msgs.msg.Mesh, self.mesh_in_cb)
        rospy.Subscriber('~offset', udom_common_msgs.msg.CartesianOffset, self.offset_cb)

    def event_in_cb(self, msg):
        """
        Obtains the event for the node (e.g. start, stop).

        :param msg: Event message for the node.
        :type msg: std_msgs.msg.String

        """
        self.event = msg.data

    def mesh_in_cb(self, msg):
        """
        Obtains the input mesh.

        :param msg: Input mesh.
        :type msg: udom_modeling_msgs.msg.Mesh

        """
        self.mesh_in = msg

    def offset_cb(self, msg):
        """
        Obtains the Cartesian offset.

        :param msg: Cartesian offset.
        :type msg: udom_common_msgs.msg.CartesianOffset

        """
        self.offset = msg

    def start(self):
        """
        Starts the node.

        """
        rospy.loginfo("Ready to start...")
        state = 'INIT'

        while not rospy.is_shutdown():

            if state == 'INIT':
                state = self.init_state()
            elif state == 'IDLE':
                state = self.idle_state()
            elif state == 'RUNNING':
                state = self.running_state()

            rospy.logdebug("State: {0}".format(state))
            self.loop_rate.sleep()

    def init_state(self):
        """
        Executes the INIT state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        if self.event == 'e_start':
            return 'IDLE'
        else:
            return 'INIT'

    def idle_state(self):
        """
        Executes the IDLE state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        if self.event == 'e_stop':
            self.event_out.publish('e_stopped')
            self.reset_component_data()
            return 'INIT'
        elif (self.mesh_in is not None) and (self.offset is not None):
            return 'RUNNING'
        else:
            return 'IDLE'

    def running_state(self):
        """
        Executes the RUNNING state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        if self.event == 'e_stop':
            self.event_out.publish('e_stopped')
            self.reset_component_data()
            return 'INIT'
        else:
            mesh_out = self.shift_mesh()
            self.event_out.publish('e_running')
            self.mesh_out.publish(mesh_out)
            self.reset_component_data()
            return 'IDLE'

    def shift_mesh(self):
        """
        Shifts the input mesh by the Cartesian offset.

        :return: The shifted mesh.
        :rtype: udom_modeling_msgs/Mesh

        """
        # 1. Extract nodes
        points = pe_utils.mesh_to_list(self.mesh_in, self.contact_nodes)

        # 2. Apply offset
        shifted_points = ms_utils.shift_points(points, self.offset)

        # 3. Update/create mesh
        shifted_points = shifted_points.flatten().tolist()

        return pe_utils.list_to_mesh(self.mesh_in, shifted_points, self.contact_nodes)

    def reset_component_data(self):
        """
        Clears the data of the component.

        """
        self.mesh_in = None
        self.offset = None
        self.event = None


def main():
    rospy.init_node("mesh_shifter_node", anonymous=True)
    mesh_shifter_node = MeshShifterNode()
    mesh_shifter_node.start()
