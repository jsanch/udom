#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
This modules defines utility functions for the mesh shifter node.


"""

import numpy as np
import std_msgs.msg


def shift_points(points, offset):
    """
    Shifts a set of three-dimensional points by a Cartesian offset.

    :param points: Set of three-dimensional points arranged in the following order:
        [p1_x, p1_y, p1_z, p2_x, p2_y, p2_z...]
        is the number of nodes.
    :type points: list

    :param offset: Cartesian offset.
    :type offset: udom_common_msgs.msg.CartesianOffset

    :return: The shifted set of points.
    :rtype: np.ndarray

    """
    number_of_nodes = len(points) / 3

    shifted_points = np.array(points).reshape((number_of_nodes, 3))
    shifted_points[:, 0] += offset.linear.x
    shifted_points[:, 1] += offset.linear.y
    shifted_points[:, 2] += offset.linear.z

    return shifted_points


def compute_nodal_distance(mesh_1, mesh_2, nodes=None):
    """
    Computes distances between the nodes of the two meshes. It assumes mesh_1 is
    complete, but mesh_2 might be an offset of mesh_1. The nodes parameter indicates
    which nodes of mesh_1 are the nodes of mesh_2.

    :param mesh_1: Mesh.
    :type mesh_1: udom_modeling_msgs.msg.Mesh

    :param mesh_2: Mesh.
    :type mesh_2: udom_modeling_msgs.msg.Mesh

    :param nodes: Nodes relating mesh_2 to mesh_1
    :type nodes: list

    :return: The distances between each node of the two meshes.
    :rtype: std_msgs.msg.Float32MultiArray

    """
    distances = std_msgs.msg.Float32MultiArray()
    distances.data = np.zeros(len(mesh_1.vertices) * 3)

    if nodes:
        for ii, node in enumerate(nodes):
            distances.data[node * 3] = mesh_2.vertices[ii].x - mesh_1.vertices[node].x
            distances.data[node * 3 + 1] = mesh_2.vertices[ii].y - mesh_1.vertices[node].y
            distances.data[node * 3 + 2] = mesh_2.vertices[ii].z - mesh_1.vertices[node].z
    else:
        for ii, (node_1, node_2) in enumerate(zip(mesh_1.vertices, mesh_2.vertices)):
            distances.data[ii * 3] = node_2.x - node_1.x
            distances.data[ii * 3 + 1] = node_2.y - node_1.y
            distances.data[ii * 3 + 2] = node_2.z - node_1.z

    return distances
