#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
This node rotates a twist to a specified reference frame.

<sub>[**Note**: This node requires that a reference frame is specified and that it exists.]</sub>

**Input(s):**

  * `twist_in`: The twist to be rotated.

    - *type:* `geometry_msgs/TwistStamped`

  * `event_in`: The desired event for the node:

      `e_start`: starts the component.

      `e_stop`: stops the component.

    - *type:* `std_msgs/String`

**Output(s):**

  * `twist_out`: The rotated twist in the reference frame.

    - *type:* `geometry_msgs/TwistStamped`

  * `event_out`: The current event of the node.

      `e_running`: when the component is running.

      `e_stopped`: when the component is stopped.

    - *type:* `std_msgs/String`

**Parameter(s):**

  * `loop_rate`: Node cycle rate (in Hz).

  * `wait_for_transform`: Maximum duration to wait for a transform (in seconds).

  * `reference_frame`: The twist will be described with respect to this frame.

"""

import rospy
import tf2_ros
import std_msgs.msg
import geometry_msgs.msg
import udom_geometric_transformation.transformation_utils as transformation_utils


class TwistRotatorNode(object):
    """
    Subscribes to a geometry_msgs.msg.TwistStamped topic and rotates it with respect
    to the specified frame.

    """
    def __init__(self):
        """
        Instantiates a twist rotator node.

        :return: Node to rotate a twist into areference frame.
        :rtype: TwistRotatorNode

        """
        # Params
        self.event = None
        self.twist_info = None

        # Object to compute transformations.
        self.listener = transformation_utils.GeometryTransformer()

        # Node cycle rate (in Hz).
        self.loop_rate = rospy.Rate(rospy.get_param('~loop_rate', 10))

        # Maximum duration to wait for a transform (in seconds).
        self.wait_for_transform = rospy.get_param('~wait_for_transform', 0.1)

        # The forces will be described with respect to this frame.
        self.reference_frame = rospy.get_param("~reference_frame", None)
        assert self.reference_frame is not None, "A reference frame must be specified."

        # Publishers
        self.event_out = rospy.Publisher("~event_out", std_msgs.msg.String, queue_size=10)
        self.twist_out = rospy.Publisher(
            "~twist_out", geometry_msgs.msg.TwistStamped, queue_size=1, tcp_nodelay=True)

        # Subscribers
        rospy.Subscriber("~event_in", std_msgs.msg.String, self.event_in_cb)
        rospy.Subscriber('~twist_in', geometry_msgs.msg.TwistStamped, self.twist_in_cb)

    def event_in_cb(self, msg):
        """
        Obtains the event for the node (e.g. start, stop).

        :param msg: Event message for the node.
        :type msg: std_msgs.msg.String

        """
        self.event = msg.data

    def twist_in_cb(self, msg):
        """
        Obtains the twist information input.

        :param msg: Twist information.
        :type msg: geometry_msgs.msg.TwistStamped

        """
        self.twist_info = msg

    def start(self):
        """
        Starts the node.

        """
        rospy.loginfo("Ready to start...")
        state = 'INIT'

        while not rospy.is_shutdown():

            if state == 'INIT':
                state = self.init_state()
            elif state == 'IDLE':
                state = self.idle_state()
            elif state == 'RUNNING':
                state = self.running_state()

            rospy.logdebug("State: {0}".format(state))
            self.loop_rate.sleep()

    def init_state(self):
        """
        Executes the INIT state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        if self.event == 'e_start':
            return 'IDLE'
        else:
            return 'INIT'

    def idle_state(self):
        """
        Executes the IDLE state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        if self.event == 'e_stop':
            self.event_out.publish('e_stopped')
            self.reset_component_data()
            return 'INIT'
        elif self.twist_info is not None:
            return 'RUNNING'
        else:
            return 'IDLE'

    def running_state(self):
        """
        Executes the RUNNING state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        if self.event == 'e_stop':
            self.event_out.publish('e_stopped')
            self.reset_component_data()
            return 'INIT'
        else:
            try:
                twist_out = self.rotate_twist(self.twist_info)
                self.event_out.publish('e_running')
                self.twist_out.publish(twist_out)
            except (tf2_ros.LookupException, tf2_ros.ConnectivityException,
                    tf2_ros.ExtrapolationException) as e:
                rospy.logwarn("Error while transforming the message.\n{}".format(e))
            self.reset_component_data()
            return 'IDLE'

    def rotate_twist(self, twist_in):
        """
        Rotates the twist to the specified reference frame.

        :param twist_in: The force information to rotate.
        :type twist_in: geometry_msgs.msg.TwistStamped

        :return: The rotated twist.
        :rtype: geometry_msgs.msg.TwistStamped

        """
        twist_out = geometry_msgs.msg.TwistStamped()

        twist_out.twist = self.listener.rotate_twist(
                twist_in.twist, twist_in.header, self.reference_frame, self.wait_for_transform)
        twist_out.header.frame_id = self.reference_frame
        twist_out.header.stamp = rospy.Time.now()
        return twist_out

    def reset_component_data(self):
        """
        Clears the data of the component.

        """
        self.twist_info = None
        self.event = None


def main():
    rospy.init_node("twist_rotator_node", anonymous=True)
    twist_rotator_node = TwistRotatorNode()
    twist_rotator_node.start()
