#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
This node merges multiple PoseStamped messages into a single pose from a PoseArray.
It is assumed that all poses have the same reference frame.

**Input(s):**

  * `poses`: PoseArray.

    - *type:* `geometry_msgs/PoseArray`

**Output(s):**

  * `pose_out`: The selected pose.

    - *type:* `geometry_msgs/PoseStamped`

**Parameter(s):**

  * `loop_rate`: Node cycle rate (in Hz).

  * `config_file`: Specifies which topics the node should subscribe to.

"""

import copy
import yaml
import numpy as np
import rospy
import geometry_msgs.msg
import udom_topic_tools.force_merger_node as utils


class PoseArrayMux(object):
    """
    Merges multiple PoseStamped messages into a PoseArray message.

    """
    def __init__(self):
        """
        Instantiates a pose array mux node.

        :return: Node to merge several PoseStamped messages.
        :rtype: PoseArrayMux

        """
        # Params
        self.config_file = rospy.get_param("~config_file", None)
        assert self.config_file is not None, "A configuration file must be specified."
        self.nodes = yaml.load(open(self.config_file, "r"))

        # Node cycle rate (in Hz).
        self.loop_rate = rospy.Rate(rospy.get_param('~loop_rate', 100))

        # Publishers
        self.pose_array = rospy.Publisher(
            "~pose_array", geometry_msgs.msg.PoseArray, queue_size=1, tcp_nodelay=True)

        # Subscribers
        self.poses_subscribers = [
            utils.NodeSubscription(topic, geometry_msgs.msg.PoseStamped)
            for _, topic in self.nodes.items()]

    def start(self):
        """
        Starts the node.

        """
        rospy.loginfo("Ready to start...")
        state = 'IDLE'

        while not rospy.is_shutdown():

            if state == 'IDLE':
                state = self.idle_state()
            elif state == 'RUNNING':
                state = self.running_state()

            rospy.logdebug("State: {0}".format(state))
            self.loop_rate.sleep()

    def idle_state(self):
        """
        Executes the IDLE state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        if self.has_new_data():
            return 'RUNNING'
        else:
            return 'IDLE'

    def running_state(self):
        """
        Executes the RUNNING state of the state machine.

        :return: The updated state.
        :rtype: str

        """

        pose_array = self.merge_poses()
        if pose_array:
            self.pose_array.publish(pose_array)
        else:
            rospy.logwarn("Error while muxing the poses.")
        return 'IDLE'

    def merge_poses(self):
        """
        Merge the poses into a single pose array message.

        :return: Merged poses.
        :rtype: geometry_msgs.msg.PoseArray()

        """
        pose_array = geometry_msgs.msg.PoseArray()
        pose_array.header.frame_id = self.poses_subscribers[0].msg.header.frame_id

        pose_array.poses = []
        for sub in self.poses_subscribers:
            pose_array.poses.append(copy.deepcopy(sub.msg.pose))

        pose_array.header.stamp = rospy.Time.now()
        return pose_array

    def has_new_data(self):
        """
        Checks if all subscribed topics have new data.

        :return: True if any topics have new data, false otherwise.
        :rtype: bool

        """
        if not np.any([sub.subscribed for sub in self.poses_subscribers]):
            return False
        updated = [
            sub.previous_sequence != sub.current_sequence for sub in self.poses_subscribers]
        if np.any(updated):
            for sub in self.poses_subscribers:
                sub.previous_sequence = sub.current_sequence
            return True
        else:
            return False


def main():
    rospy.init_node("pose_array_mux", anonymous=True)
    pose_array_mux = PoseArrayMux()
    pose_array_mux.start()
