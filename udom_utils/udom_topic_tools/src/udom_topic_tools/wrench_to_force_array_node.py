#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
This node converts a WrenchStamped message into a udom_common_msgs/ForceArray message.

**Input(s):**

  * `wrench`: The wrench to be converted.

    - *type:* `geometry_msgs/WrenchStamped`

**Output(s):**

  * `force_array`: The wrench converted into a force array.

    - *type:* `udom_common_msgs/ForceArray`

**Parameter(s):**

  * `loop_rate`: Node cycle rate (in Hz).

"""

import rospy
import std_msgs.msg
import geometry_msgs.msg
import udom_common_msgs.msg


class WrenchToForceArray(object):
    """
    Subscribes to wrench topic and converts it into a force array message.

    """
    def __init__(self):
        """
        Instantiates a wrench to force array converter node.

        :return: Node to convert a wrench into a force array message.
        :rtype: WrenchToForceArray

        """
        # Params
        self.event = None
        self.wrench = None

        # Node cycle rate (in Hz).
        self.loop_rate = rospy.Rate(rospy.get_param('~loop_rate', 10))

        # Publishers
        self.event_out = rospy.Publisher("~event_out", std_msgs.msg.String, queue_size=10)
        self.force_array = rospy.Publisher(
            "~force_array", udom_common_msgs.msg.ForceArray, queue_size=1)

        # Subscribers
        rospy.Subscriber("~wrench", geometry_msgs.msg.WrenchStamped, self.wrench_cb)

    def wrench_cb(self, msg):
        """
        Obtains the wrench message.

        :param msg: Wrench.
        :type msg: geometry_msgs.msg.WrenchStamped

        """
        self.wrench = msg

    def start(self):
        """
        Starts the node.

        """
        rospy.loginfo("Ready to start...")
        state = 'IDLE'

        while not rospy.is_shutdown():

            if state == 'IDLE':
                state = self.idle_state()
            elif state == 'RUNNING':
                state = self.running_state()

            rospy.logdebug("State: {0}".format(state))
            self.loop_rate.sleep()

    def idle_state(self):
        """
        Executes the IDLE state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        if self.wrench:
            return 'RUNNING'
        else:
            return 'IDLE'

    def running_state(self):
        """
        Executes the RUNNING state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        force_array = convert_wrench_to_force_array(self.wrench)
        self.force_array.publish(force_array)
        self.reset_component_data()
        return 'IDLE'

    def reset_component_data(self):
        """
        Clears the data of the component.

        """
        self.wrench = None


def convert_wrench_to_force_array(wrench):
    """
    Converts a wrench into a force array message.

    :param wrench: The wrench to convert.
    :type wrench: geometry_msgs.msg.WrenchStamped

    :return: The converted wrench as a force array.
    :rtype: udom_common_msgs.msg.ForceArray

    """
    force_array = udom_common_msgs.msg.ForceArray()
    force_array.header.frame_id = wrench.header.frame_id
    force_array.wrenches = [wrench.wrench]

    force_array.header.stamp = rospy.Time.now()
    return force_array


def main():
    rospy.init_node("wrench_to_force_array", anonymous=True)
    wrench_to_force_array = WrenchToForceArray()
    wrench_to_force_array.start()
