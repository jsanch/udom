#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
This node sums the messages from two topics of force array.

**Input(s):**
  * `forces_1`: The first force array

    - *type:* `std_msgs/Float32MultiArray`

  * `forces_2`: The second force array

    - *type:* `std_msgs/Float32MultiArray`

  * `event_in`: The desired event for the node:

      `e_start`: starts the component.

      `e_stop`: stops the component.

    - *type:* `std_msgs/String`

**Output(s):**
  * `force_out`: The summed force arrays.

    - *type:* `std_msgs/Float32MultiArray`

  * `event_out`: The current event of the node.

      `e_running`: when the component is running.

      `e_stopped`: when the component is stopped.

    - *type:* `std_msgs/String`

**Parameter(s):**

  * `loop_rate`: Node cycle rate (in Hz).

  * `number_of_nodes`: Number of nodes that correspond to the force array.

"""

import numpy as np
import rospy
import std_msgs.msg


class ForceAdderNode(object):
    """
    Subscribes to two topics of force array and adds them to publish the result.

    """
    def __init__(self):
        """
        Instantiates a force adder node.

        :return: Node to add force messages.
        :rtype: ForceAdderNode

        """
        # Params
        self.event = None
        self.force_1 = std_msgs.msg.Float32MultiArray()
        self.force_2 = std_msgs.msg.Float32MultiArray()

        # Node cycle rate (in Hz).
        self.loop_rate = rospy.Rate(rospy.get_param('~loop_rate', 10))

        # Number of nodes that correspond to the force array.
        self.number_of_nodes = rospy.get_param('~number_of_nodes', 152)

        # Set initial zero values for the force arrays.
        self.force_1.data = np.zeros(self.number_of_nodes * 3).tolist()
        self.force_2.data = np.zeros(self.number_of_nodes * 3).tolist()

        # Publishers
        self.event_out = rospy.Publisher("~event_out", std_msgs.msg.String, queue_size=10)
        self.force_out = rospy.Publisher(
            "~force_out", std_msgs.msg.Float32MultiArray, queue_size=1)

        # Subscribers
        rospy.Subscriber("~event_in", std_msgs.msg.String, self.event_in_cb)
        rospy.Subscriber("~forces_1", std_msgs.msg.Float32MultiArray, self.forces_1_cb)
        rospy.Subscriber("~forces_2", std_msgs.msg.Float32MultiArray, self.forces_2_cb)

    def event_in_cb(self, msg):
        """
        Obtains the event for the node (e.g. start, stop).

        :param msg: Event message for the node.
        :type msg: std_msgs.msg.String

        """
        self.event = msg.data

    def forces_1_cb(self, msg):
        """
        Obtains the force array message.

        :param msg: Force array.
        :type msg: std_msgs.msg.ForceMultiArray

        """
        self.force_1.data = msg.data

    def forces_2_cb(self, msg):
        """
        Obtains the force array message.

        :param msg: Force array.
        :type msg: std_msgs.msg.ForceMultiArray

        """
        self.force_2.data = msg.data

    def start(self):
        """
        Starts the node.

        """
        rospy.loginfo("Ready to start...")
        state = 'INIT'

        while not rospy.is_shutdown():

            if state == 'INIT':
                state = self.init_state()
            elif state == 'IDLE':
                state = self.idle_state()
            elif state == 'RUNNING':
                state = self.running_state()

            rospy.logdebug("State: {0}".format(state))
            self.loop_rate.sleep()

    def init_state(self):
        """
        Executes the INIT state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        if self.event == 'e_start':
            return 'IDLE'
        else:
            return 'INIT'

    def idle_state(self):
        """
        Executes the IDLE state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        if self.event == 'e_stop':
            self.event_out.publish('e_stopped')
            self.reset_component_data()
            return 'INIT'
        elif self.force_1.data and self.force_2.data:
            return 'RUNNING'
        else:
            return 'IDLE'

    def running_state(self):
        """
        Executes the RUNNING state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        if self.event == 'e_stop':
            self.event_out.publish('e_stopped')
            self.reset_component_data()
            return 'INIT'
        else:
            force_out = self.add_forces()
            self.event_out.publish('e_running')
            self.force_out.publish(force_out)
        self.reset_component_data()
        return 'IDLE'

    def add_forces(self):
        """
        Adds the two forces and returns the result as a force array.

        :return: The sum of the two force arrays.
        :rtype: std_msgs.msg.Float32MultiArray

        """
        forces = std_msgs.msg.Float32MultiArray()
        total = np.array(self.force_1.data) + np.array(self.force_2.data)
        forces.data = total.tolist()

        return forces

    def reset_component_data(self):
        """
        Clears the data of the component.

        """
        self.event = None
        self.force_1.data = np.zeros(self.number_of_nodes * 3).tolist()
        self.force_2.data = np.zeros(self.number_of_nodes * 3).tolist()


def main():
    rospy.init_node("force_adder_node", anonymous=True)
    force_adder_node = ForceAdderNode()
    force_adder_node.start()
