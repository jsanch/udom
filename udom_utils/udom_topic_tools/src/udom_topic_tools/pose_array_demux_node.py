#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
This node selects a single pose from a PoseArray to publish as a PoseStamped message.

**Input(s):**

  * `poses`: PoseArray.

    - *type:* `geometry_msgs/PoseArray`

**Output(s):**

  * `pose_out`: The selected pose.

    - *type:* `geometry_msgs/PoseStamped`

**Parameter(s):**

  * `loop_rate`: Node cycle rate (in Hz).

  * `index`: Index of the pose to be extracted from the PoseArray message.

"""

import copy
import rospy
import geometry_msgs.msg


class PoseArrayDemux(object):
    """
    Selects, from a PoseArray message, a single pose based on the index.

    """
    def __init__(self):
        """
        Instantiates a pose array demux node.

        :return: Node to select a PoseStamped message.
        :rtype: PoseArrayDemux

        """
        # Params
        self.pose_array = None

        # Index of the pose to be selected.
        self.index = rospy.get_param('~index', None)
        assert self.index is not None, "index must be specified."

        # Node cycle rate (in Hz).
        self.loop_rate = rospy.Rate(rospy.get_param('~loop_rate', 100))

        # Publishers
        self.pose_out = rospy.Publisher(
            "~pose_out", geometry_msgs.msg.PoseStamped, queue_size=1, tcp_nodelay=True)

        # Subscribers
        rospy.Subscriber("~poses", geometry_msgs.msg.PoseArray, self.pose_array_cb)

    def pose_array_cb(self, msg):
        """
        Obtains the pose array.

        :param msg: Pose array.
        :type msg: geometry_msgs/PoseArray

        """
        self.pose_array = msg

    def start(self):
        """
        Starts the node.

        """
        rospy.loginfo("Ready to start...")
        state = 'IDLE'

        while not rospy.is_shutdown():

            if state == 'IDLE':
                state = self.idle_state()
            elif state == 'RUNNING':
                state = self.running_state()

            rospy.logdebug("State: {0}".format(state))
            self.loop_rate.sleep()

    def idle_state(self):
        """
        Executes the IDLE state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        if self.pose_array is not None:
            return 'RUNNING'
        else:
            return 'IDLE'

    def running_state(self):
        """
        Executes the RUNNING state of the state machine.

        :return: The updated state.
        :rtype: str

        """

        pose_out = self.extract_pose()
        if pose_out:
            self.pose_out.publish(pose_out)
        else:
            rospy.logwarn("Error while demuxing the pose array.")
        self.reset_component_data()
        return 'IDLE'

    def extract_pose(self):
        """
        Extracts the pose specified by index from the pose array message.

        :return: Extracted pose.
        :rtype: geometry_msgs.msg.PoseStamped()

        """
        pose_out = geometry_msgs.msg.PoseStamped()
        pose_out.header.frame_id = self.pose_array.header.frame_id

        pose_out.pose = copy.deepcopy(self.pose_array.poses[self.index])
        pose_out.header.stamp = rospy.Time.now()
        return pose_out

    def reset_component_data(self):
        """
        Clears the data of the component.

        """
        self.pose_array = None


def main():
    rospy.init_node("pose_array_demux", anonymous=True)
    pose_array_demux = PoseArrayDemux()
    pose_array_demux.start()
