#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
This node combines the messages from a forces (std_msgs/Float32MultiArray) and a
mesh (udom_modeling_msgs/Mesh) topic into a single udom_common_msgs/ForceMesh message
which is then published.

**Input(s):**
  * `mesh`: The mesh.
    - *type:* `udom_modeling_msgs/Mesh`

  * `forces`: The forces applied to the mesh's nodes.
    - *type:* `std_msgs/Float32MultiArray`

**Output(s):**
  * `force_mesh`: A single message containing both the mesh and the forces applied to it.
    - *type:* `udom_common_msgs/ForceMesh`

**Parameter(s):**
  * `loop_rate`: Node cycle rate (in Hz).

"""

import rospy
import std_msgs.msg
import udom_modeling_msgs.msg
import udom_common_msgs.msg


class ForceMeshMergerNode(object):
    """
    Subscribes to messages from a forces (std_msgs/Float32MultiArray) and a
    mesh (udom_modeling_msgs/Mesh) topic into a single udom_common_msgs/ForceMesh message
    which is then published.

    """
    def __init__(self):
        """
        Instantiates a force mesh merger node.

        :return: Node to merger a forces and a mesh messages.
        :rtype: ForceMeshMergerNode

        """
        # Params
        self.event = None
        self.mesh = None
        self.forces = None
        self.forces_mesh = udom_common_msgs.msg.ForceMesh()

        # Node cycle rate (in Hz).
        self.loop_rate = rospy.Rate(rospy.get_param('~loop_rate', 100))

        # Publishers
        self.force_mesh_pub = rospy.Publisher(
            "~force_mesh", udom_common_msgs.msg.ForceMesh, queue_size=1,
            tcp_nodelay=True)

        # Subscribers
        rospy.Subscriber("~mesh", udom_modeling_msgs.msg.Mesh, self.mesh_cb)
        rospy.Subscriber("~forces", std_msgs.msg.Float32MultiArray, self.forces_cb)

    def event_in_cb(self, msg):
        """
        Obtains the event for the node (e.g. start, stop).

        :param msg: Event message for the node.
        :type msg: std_msgs.msg.String

        """
        self.event = msg.data

    def mesh_cb(self, msg):
        """
        Obtains the mesh message.

        :param msg: Message for the mesh.
        :type msg: udom_modeling_msgs.msg.Mesh

        """
        self.mesh = msg

    def forces_cb(self, msg):
        """
        Obtains the forces message.

        :param msg: Message for the forces.
        :type msg: std_msgs.msg.Float32MultiArray

        """
        self.forces = msg

    def start(self):
        """
        Starts the node.

        """
        rospy.loginfo("Ready to start...")
        state = 'IDLE'

        while not rospy.is_shutdown():

            if state == 'IDLE':
                state = self.idle_state()
            elif state == 'RUNNING':
                state = self.running_state()

            rospy.logdebug("State: {0}".format(state))
            self.loop_rate.sleep()

    def idle_state(self):
        """
        Executes the IDLE state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        if (self.mesh is not None) and (self.forces is not None):
            return 'RUNNING'
        else:
            return 'IDLE'

    def running_state(self):
        """
        Executes the RUNNING state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        self.fill_message()
        self.force_mesh_pub.publish(self.forces_mesh)
        return 'IDLE'

    def fill_message(self):
        """
        Fills the ForceMesh message with the forces, mesh and the current time stamp.

        """
        self.forces_mesh.header.stamp = rospy.Time.now()
        self.forces_mesh.forces = self.forces
        self.forces_mesh.mesh = self.mesh


def main():
    rospy.init_node("force_mesh_merger_node", anonymous=True)
    force_mesh_merger_node = ForceMeshMergerNode()
    force_mesh_merger_node.start()
