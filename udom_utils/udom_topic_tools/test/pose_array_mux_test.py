#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
Integration test for the 'pose_array_mux' node.

"""

import rospy
import unittest
import rostest
import geometry_msgs.msg

PKG = 'udom_topic_tools'


class TestPoseArrayMux(unittest.TestCase):
    def setUp(self):
        """
        Sets up the test fixture before exercising it.

        """
        # params
        self.result = None
        self.wait_for_result = None

        # publishers
        self.pose_1 = rospy.Publisher('~pose_1', geometry_msgs.msg.PoseStamped, queue_size=10)
        self.pose_2 = rospy.Publisher('~pose_2', geometry_msgs.msg.PoseStamped, queue_size=10)
        self.pose_3 = rospy.Publisher('~pose_3', geometry_msgs.msg.PoseStamped, queue_size=10)

        # subscribers
        self.component_output = rospy.Subscriber(
            '~component_output', geometry_msgs.msg.PoseArray, self.result_callback)

    def tearDown(self):
        """
        Deconstructs the test fixture after testing it.

        """
        self.pose_1.unregister()
        self.pose_2.unregister()
        self.pose_3.unregister()
        self.component_output.unregister()

    def test_pose_array_mux(self):
        """
        Verifies that the node correctly outputs a PoseArray message based on
        multiple PoseStamped messages.

        """
        reference_frame = 'map'

        desired = geometry_msgs.msg.PoseArray()
        desired.header.frame_id = reference_frame
        desired.poses = [
            geometry_msgs.msg.Pose(position=geometry_msgs.msg.Point(1.0, 0.0, 0.0)),
            geometry_msgs.msg.Pose(position=geometry_msgs.msg.Point(0.0, 1.0, 0.0)),
            geometry_msgs.msg.Pose(position=geometry_msgs.msg.Point(0.0, 0.0, 1.0))]

        pose_1 = geometry_msgs.msg.PoseStamped()
        pose_1.header.frame_id = reference_frame
        pose_1.pose.position = geometry_msgs.msg.Point(1.0, 0.0, 0.0)

        pose_2 = geometry_msgs.msg.PoseStamped()
        pose_2.header.frame_id = reference_frame
        pose_2.pose.position = geometry_msgs.msg.Point(0.0, 1.0, 0.0)

        pose_3 = geometry_msgs.msg.PoseStamped()
        pose_3.header.frame_id = reference_frame
        pose_3.pose.position = geometry_msgs.msg.Point(0.0, 0.0, 1.0)

        while not self.wait_for_result:
            pose_1.header.stamp = rospy.Time.now()
            pose_2.header.stamp = rospy.Time.now()
            pose_3.header.stamp = rospy.Time.now()
            self.pose_1.publish(pose_1)
            self.pose_2.publish(pose_2)
            self.pose_3.publish(pose_3)

        self.assertIsInstance(self.result, geometry_msgs.msg.PoseArray)
        self.assertEqual(self.result.header.frame_id, desired.header.frame_id)
        self.assertEqual(self.result.poses[0].position, desired.poses[0].position)
        self.assertEqual(self.result.poses[1].position, desired.poses[1].position)
        self.assertEqual(self.result.poses[2].position, desired.poses[2].position)

    def result_callback(self, msg):
        print("in callback!!")
        self.result = msg
        self.wait_for_result = True


if __name__ == '__main__':
    rospy.init_node('pose_array_mux_test')
    rostest.rosrun(PKG, 'pose_array_mux_test', TestPoseArrayMux)
