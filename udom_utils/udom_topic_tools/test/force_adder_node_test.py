#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
Integration test for the 'force_adder' node.

"""

import numpy as np
import rospy
import unittest
import rostest
import std_msgs.msg

PKG = 'udom_topic_tools'


class TestForceAdder(unittest.TestCase):
    def setUp(self):
        """
        Sets up the test fixture before exercising it.

        """
        # params
        self.result = None
        self.wait_for_result = None

        # publishers
        self.event_out = rospy.Publisher('~event_out', std_msgs.msg.String, latch=True)
        self.forces_1 = rospy.Publisher(
            '~forces_1', std_msgs.msg.Float32MultiArray, queue_size=1)
        self.forces_2 = rospy.Publisher(
            '~forces_2', std_msgs.msg.Float32MultiArray, queue_size=1)

        # subscribers
        self.component_output = rospy.Subscriber(
            '~component_output', std_msgs.msg.Float32MultiArray, self.result_callback)

    def tearDown(self):
        """
        Deconstructs the test fixture after testing it.

        """
        self.component_output.unregister()
        self.event_out.unregister()
        self.forces_1.unregister()
        self.forces_2.unregister()

    def test_force_adder_node(self):
        """
        Verifies the node's interface is correct.

        """
        desired = std_msgs.msg.Float32MultiArray()

        forces_1 = std_msgs.msg.Float32MultiArray()
        forces_2 = std_msgs.msg.Float32MultiArray()

        forces_1.data = [1, 1, 1]
        forces_2.data = [1, -2, 3]
        desired.data = [2, -1, 4]

        while not self.wait_for_result:
            self.forces_1.publish(forces_1)
            self.forces_2.publish(forces_2)
            self.event_out.publish('e_start')

        self.assertIsInstance(self.result, std_msgs.msg.Float32MultiArray)
        np.testing.assert_allclose(self.result.data, desired.data)

    def result_callback(self, msg):
        self.result = msg
        self.wait_for_result = True


if __name__ == '__main__':
    rospy.init_node('force_adder_test')
    rostest.rosrun(PKG, 'force_adder_test', TestForceAdder)
