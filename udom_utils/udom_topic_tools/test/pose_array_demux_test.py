#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
Integration test for the 'pose_array_demux' node.

"""

import rospy
import unittest
import rostest
import geometry_msgs.msg

PKG = 'udom_topic_tools'


class TestPoseArrayDemux(unittest.TestCase):
    def setUp(self):
        """
        Sets up the test fixture before exercising it.

        """
        # params
        self.result = None
        self.wait_for_result = None

        # publishers
        self.poses = rospy.Publisher('~poses', geometry_msgs.msg.PoseArray, queue_size=10)

        # subscribers
        self.component_output = rospy.Subscriber(
            '~component_output', geometry_msgs.msg.PoseStamped, self.result_callback)

    def tearDown(self):
        """
        Deconstructs the test fixture after testing it.

        """
        self.poses.unregister()
        self.component_output.unregister()

    def test_pose_array_demux(self):
        """
        Verifies that the node correctly outputs a PoseStamped message based on
        a PoseArray message.

        """
        # This must match the index in the .test file.
        index = 0
        poses = geometry_msgs.msg.PoseArray()
        poses.header.frame_id = 'map'
        poses.poses = [
            geometry_msgs.msg.Pose(position=geometry_msgs.msg.Point(1.0, 0.0, 0.0)),
            geometry_msgs.msg.Pose(position=geometry_msgs.msg.Point(1.0, 1.0, 1.0)),
            geometry_msgs.msg.Pose(position=geometry_msgs.msg.Point(1.0, 1.0, 1.0)),
            geometry_msgs.msg.Pose(position=geometry_msgs.msg.Point(1.0, 1.0, 1.0)),
            geometry_msgs.msg.Pose(position=geometry_msgs.msg.Point(0.0, 0.0, 1.0))]

        while not self.wait_for_result:
            poses.header.stamp = rospy.Time.now()
            self.poses.publish(poses)

        self.assertIsInstance(self.result, geometry_msgs.msg.PoseStamped)
        self.assertEqual(self.result.header.frame_id, poses.header.frame_id)
        self.assertEqual(self.result.pose.position, poses.poses[index].position)

    def result_callback(self, msg):
        self.result = msg
        self.wait_for_result = True


if __name__ == '__main__':
    rospy.init_node('pose_array_demux_test')
    rostest.rosrun(PKG, 'pose_array_demux_test', TestPoseArrayDemux)
