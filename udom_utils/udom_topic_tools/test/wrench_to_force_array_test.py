#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
Integration test for the 'wrench_to_force_array' node.

"""

import rospy
import unittest
import rostest
import std_msgs.msg
import geometry_msgs.msg
import udom_common_msgs.msg

PKG = 'udom_topic_tools'


class TestWrenchToForceArrayConverter(unittest.TestCase):
    def setUp(self):
        """
        Sets up the test fixture before exercising it.

        """
        # params
        self.result = None
        self.wait_for_result = None

        # publishers
        self.wrench = rospy.Publisher(
            '~wrench', geometry_msgs.msg.WrenchStamped, queue_size=1)

        # subscribers
        self.component_output = rospy.Subscriber(
            '~component_output', udom_common_msgs.msg.ForceArray, self.result_callback)

    def tearDown(self):
        """
        Deconstructs the test fixture after testing it.

        """
        self.component_output.unregister()
        self.wrench.unregister()

    def test_wrench_to_force_array_converter(self):
        """
        Verifies the node's interface is correct.

        """
        desired = udom_common_msgs.msg.ForceArray()
        desired.header.frame_id = "frame_1"
        desired.wrenches = [geometry_msgs.msg.Wrench(
            force=geometry_msgs.msg.Vector3(x=1.0, y=2.0, z=3.0),
            torque=geometry_msgs.msg.Vector3(x=4.0, y=5.0, z=6.0))]

        wrench = geometry_msgs.msg.WrenchStamped()
        wrench.header.frame_id = "frame_1"
        wrench.wrench.force = geometry_msgs.msg.Vector3(x=1.0, y=2.0, z=3.0)
        wrench.wrench.torque = geometry_msgs.msg.Vector3(x=4.0, y=5.0, z=6.0)

        while not self.wait_for_result:
            wrench.header.stamp = rospy.Time.now()
            self.wrench.publish(wrench)

        self.assertIsInstance(self.result, udom_common_msgs.msg.ForceArray)
        self.assertEqual(self.result.wrenches[0].force.x, desired.wrenches[0].force.x)
        self.assertEqual(self.result.wrenches[0].force.y, desired.wrenches[0].force.y)
        self.assertEqual(self.result.wrenches[0].force.z, desired.wrenches[0].force.z)
        self.assertEqual(self.result.wrenches[0].torque.x, desired.wrenches[0].torque.x)
        self.assertEqual(self.result.wrenches[0].torque.y, desired.wrenches[0].torque.y)
        self.assertEqual(self.result.wrenches[0].torque.z, desired.wrenches[0].torque.z)

    def result_callback(self, msg):
        self.result = msg
        self.wait_for_result = True


if __name__ == '__main__':
    rospy.init_node('wrench_to_force_array_test')
    rostest.rosrun(PKG, 'wrench_to_force_array_test', TestWrenchToForceArrayConverter)
