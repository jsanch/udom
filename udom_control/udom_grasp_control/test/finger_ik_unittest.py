#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
Unittest for the finger_ik_utils.py module.

"""

import numpy as np
import numpy.testing as testing
import unittest
import rosunit
import sensor_msgs.msg
import udom_grasp_control.finger_ik_utils as utils

PKG = 'udom_grasp_control'


class TestFingerIK(unittest.TestCase):
    """
    Tests the finger_ik_utils.py module.

    """
    def test_compute_t_matrix(self):
        """
        Tests that the function returns the correct transformation matrix.

        """
        a_0 = 0
        alpha_0 = 0
        d_1 = 0
        theta_1 = np.radians(90.0)

        a_1 = 0
        alpha_1 = np.radians(90.0)
        d_2 = 0.07
        theta_2 = 0.0

        a_2 = 0
        alpha_2 = 0
        d_3 = 0.2
        theta_3 = np.radians(45.0)

        desired_t_0_1 = np.array([
            [np.cos(theta_1), -np.sin(theta_1), 0.0, 0.0],
            [np.sin(theta_1), np.cos(theta_1), 0.0, 0.0],
            [0.0, 0.0, 1.0, 0.0],
            [0.0, 0.0, 0.0, 1.0]])
        desired_t_1_2 = np.array([
            [1.0, 0.0, 0.0, 0.0],
            [0.0, 0.0, -1.0, -d_2],
            [0.0, 1.0, 0.0, 0.0],
            [0.0, 0.0, 0.0, 1.0]])
        desired_t_2_3 = np.array([
            [np.cos(theta_3), -np.sin(theta_3), 0.0, 0.0],
            [np.sin(theta_3), np.cos(theta_3), 0.0, 0.0],
            [0.0, 0.0, 1.0, d_3],
            [0.0, 0.0, 0.0, 1.0]])

        result_t_0_1 = utils.compute_t_matrix(a_0, d_1, alpha_0, theta_1)
        result_t_1_2 = utils.compute_t_matrix(a_1, d_2, alpha_1, theta_2)
        result_t_2_3 = utils.compute_t_matrix(a_2, d_3, alpha_2, theta_3)

        testing.assert_array_almost_equal(result_t_0_1, desired_t_0_1, decimal=4)
        testing.assert_array_almost_equal(desired_t_1_2, result_t_1_2, decimal=4)
        testing.assert_array_almost_equal(desired_t_2_3, result_t_2_3, decimal=4)

    def test_compute_fk_rest(self):
        """
        Tests that the function returns joint values that approximate the desired
        end-effector position for the rest joint configuration.

        """
        finger = utils.Finger()
        joints = np.radians([0, 0, 0])
        desired = [0.0, 0.0, 0.096]

        result = finger.compute_fk(joints)
        testing.assert_array_almost_equal(result, desired, decimal=3)

    def test_compute_fk_j_0(self):
        """
        Tests that the function returns joint values that approximate the desired
        end-effector position for the joint 0.

        """
        finger = utils.Finger()
        joints = np.radians([0, 0, 60])
        desired = [0.0, -0.0442, 0.0705]

        result = finger.compute_fk(joints)
        testing.assert_array_almost_equal(result, desired, decimal=3)

    def test_compute_fk_j_3(self):
        """
        Tests that the function returns joint values that approximate the desired
        end-effector position for the joint 3.

        """
        finger = utils.Finger()
        joints = np.radians([0, 30, 0])
        desired = [0.0, -0.048, 0.0831]

        result = finger.compute_fk(joints)
        testing.assert_array_almost_equal(result, desired, decimal=3)

    def test_compute_fk_j_4(self):
        """
        Tests that the function returns joint values that approximate the desired
        end-effector position for the joint 4.

        """
        finger = utils.Finger()
        joints = np.radians([15, 0, 0])
        desired = [-0.0248, 0.0, 0.0927]

        result = finger.compute_fk(joints)
        testing.assert_array_almost_equal(result, desired, decimal=3)

    def test_compute_fk(self):
        """
        Tests that the function returns joint values that approximate the desired
        end-effector position for all the joints.

        """
        finger = utils.Finger()

        joints = np.radians([-10, 60, 35])
        desired = [0.0031, -0.0898, 0.0178]

        result = finger.compute_fk(joints)
        testing.assert_array_almost_equal(result, desired, decimal=3)

    def test_compute_ik(self):
        """
        Tests that the function returns joint values that approximate the desired
        end-effector position.

        """
        finger = utils.Finger()
        ee_position = [0.01, -0.073, 0.038]
        desired = np.radians([-15, 30, 60]).tolist()

        result = finger.compute_ik(ee_position)
        testing.assert_array_almost_equal(result, desired, decimal=2)

    def test_extract_positions(self):
        """
        Tests that the function returns the correct joint positions.

        """
        desired = np.radians([30, 5, -5]).tolist()

        joint_states = sensor_msgs.msg.JointState()
        joint_states.name = [
            'rh_LFJ1', 'rh_LFJ2', 'rh_LFJ3', 'rh_LFJ4', 'rh_LFJ5', 'rh_FFJ0',
            'rh_FFJ3', 'rh_FFJ4',  'rh_MFJ1', 'rh_MFJ2', 'rh_MFJ3']
        joint_states.position = np.zeros(len(joint_states.name))
        # Testing for the first finger joints (FFJ)
        joint_states.position[5] = np.radians(30)
        joint_states.position[6] = np.radians(5)
        joint_states.position[7] = np.radians(-5)

        joint_names = ['rh_FFJ0', 'rh_FFJ3', 'rh_FFJ4']

        result = utils.extract_positions(joint_states, joint_names)

        testing.assert_array_equal(result, desired)

    def test_thumb_compute_fk_rest(self):
        """
        Tests that the function returns joint values that approximate the desired
        end-effector position for the rest joint configuration of the thumb.

        """
        thumb = utils.Thumb()
        joints = np.radians([0, 0, 0, 0])
        desired = [0.0, 0.0, 0.099]

        result = thumb.compute_fk(joints)
        testing.assert_array_almost_equal(result, desired, decimal=3)

    def test_thumb_compute_fk_j_2(self):
        """
        Tests that the function returns joint values that approximate the desired
        end-effector position for the joint 2 of the thumb.

        """
        thumb = utils.Thumb()
        joints = np.radians([0, 0, 0, 30])
        desired = [-0.0255, 0.0, 0.0922]

        result = thumb.compute_fk(joints)
        testing.assert_array_almost_equal(result, desired, decimal=3)

    def test_thumb_compute_fk_j_3(self):
        """
        Tests that the function returns joint values that approximate the desired
        end-effector position for the joint 3 of the thumb.

        """
        thumb = utils.Thumb()
        joints = np.radians([0, 0, 6, 0])
        desired = [0.000, -0.006, 0.0987]

        result = thumb.compute_fk(joints)
        testing.assert_array_almost_equal(result, desired, decimal=3)

    def test_thumb_compute_fk_j_4(self):
        """
        Tests that the function returns joint values that approximate the desired
        end-effector position for the joint 4 of the thumb.

        """
        thumb = utils.Thumb()
        joints = np.radians([0, 45, 0, 0])
        desired = [0.0, -0.07, 0.07]

        result = thumb.compute_fk(joints)
        testing.assert_array_almost_equal(result, desired, decimal=3)

    def test_thumb_compute_fk_j_5(self):
        """
        Tests that the function returns joint values that approximate the desired
        end-effector position for the joint 5 of the thumb.

        """
        thumb = utils.Thumb()
        joints = np.radians([-30, 0, 0, 0])
        desired = [0.0, 0.0, 0.099]

        result = thumb.compute_fk(joints)
        testing.assert_array_almost_equal(result, desired, decimal=3)

    def test_thumb_compute_fk(self):
        """
        Tests that the function returns joint values that approximate the desired
        end-effector position for all the joints of the thumb.

        """
        thumb = utils.Thumb()
        joints = np.radians([-30, 60, -6, 15])
        desired = [0.029, -0.077, 0.054]

        result = thumb.compute_fk(joints)
        testing.assert_array_almost_equal(result, desired, decimal=3)

    def test_thumb_compute_ik(self):
        """
        Tests that the function returns joint values that approximate the desired
        end-effector position of the thumb.

        """
        thumb = utils.Thumb()
        ee_position = [0.029, -0.077, 0.054]

        result = thumb.compute_ik(ee_position)
        testing.assert_array_almost_equal(thumb.compute_fk(result), ee_position, decimal=2)


if __name__ == '__main__':
    rosunit.unitrun(PKG, 'test_finger_ik', TestFingerIK)
