#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
Integration test for the 'shadow_cartesian_controller' node.

"""

import numpy as np
import rospy
import unittest
import rostest
import std_msgs.msg
import geometry_msgs.msg
import sensor_msgs.msg
import tf2_ros

PKG = 'udom_grasp_control'


class TestCartesianController(unittest.TestCase):
    def setUp(self):
        """
        Sets up the test fixture before exercising it.

        """
        # params
        self.result_j0 = None
        self.result_j3 = None
        self.result_j4 = None
        self.wait_for_result_j0 = None
        self.wait_for_result_j3 = None
        self.wait_for_result_j4 = None

        # publishers
        self.event_out = rospy.Publisher(
            '~event_out', std_msgs.msg.String, latch=True)
        self.ee_position = rospy.Publisher(
            '~ee_position', geometry_msgs.msg.PoseStamped, queue_size=1)
        self.joint_states = rospy.Publisher(
            '~joint_states', sensor_msgs.msg.JointState, queue_size=1)

        # subscribers
        self.j0_position_command = rospy.Subscriber(
            '~j0_position_command', std_msgs.msg.Float64, self.j0_position_command_cb)
        self.j3_position_command = rospy.Subscriber(
            '~j3_position_command', std_msgs.msg.Float64, self.j3_position_command_cb)
        self.j4_position_command = rospy.Subscriber(
            '~j4_position_command', std_msgs.msg.Float64, self.j4_position_command_cb)

    def tearDown(self):
        """
        Deconstructs the test fixture after testing it.

        """
        self.j0_position_command.unregister()
        self.j3_position_command.unregister()
        self.j4_position_command.unregister()
        self.event_out.unregister()
        self.ee_position.unregister()

    def test_cartesian_controller_node(self):
        """
        Verifies the node's interface is correct.
        Note: this is not a functionality test.

        """
        # These parameters must be the match the ones specified in the .test file.
        reference_frame = 'rh_palm'
        finger_frame = 'rh_ffknuckle'

        joint_states = sensor_msgs.msg.JointState()
        joint_states.name = [
            'rh_LFJ1', 'rh_LFJ2', 'rh_LFJ3', 'rh_LFJ4', 'rh_LFJ5', 'rh_FFJ4',
            'rh_FFJ3', 'rh_FFJ0', 'rh_MFJ1', 'rh_MFJ2', 'rh_MFJ3']
        joint_states.position = np.zeros(len(joint_states.name))
        # Testing for the first finger joints (FFJ)
        joint_states.position[5] = np.radians(0)
        joint_states.position[6] = np.radians(10)
        joint_states.position[7] = np.radians(30)

        ee_position = geometry_msgs.msg.PoseStamped()
        ee_position.pose.position.x = 0.042
        ee_position.pose.position.y = -0.071
        ee_position.pose.position.z = 0.144
        ee_position.pose.orientation.w = 1.0
        ee_position.header.frame_id = reference_frame
        ee_position.header.stamp = rospy.Time.now()

        # Set transforms.
        transform = geometry_msgs.msg.TransformStamped()
        transform.header.frame_id = reference_frame
        transform.child_frame_id = finger_frame
        transform.transform.translation.x = 0.033
        transform.transform.translation.y = 0.0
        transform.transform.translation.z = 0.095
        transform.transform.rotation.x = 0.0
        transform.transform.rotation.y = 0.0
        transform.transform.rotation.z = 0.0
        transform.transform.rotation.w = 1.0

        broadcaster = tf2_ros.TransformBroadcaster()

        # Desired values.
        desired_j4, desired_j3, desired_j0 = [-0.18, 0.487, 0.884]

        while not (self.wait_for_result_j0 and self.wait_for_result_j3
                   and self.wait_for_result_j4):
            ee_position.header.stamp = rospy.Time.now()
            transform.header.stamp = rospy.Time.now()
            broadcaster.sendTransform(transform)

            self.ee_position.publish(ee_position)
            self.joint_states.publish(joint_states)
            self.event_out.publish('e_start')

        self.assertAlmostEqual(self.result_j0.data, desired_j0, places=1)
        self.assertAlmostEqual(self.result_j3.data, desired_j3, places=1)
        self.assertAlmostEqual(self.result_j4.data, desired_j4, places=1)

    def j0_position_command_cb(self, msg):
        self.result_j0 = msg
        self.wait_for_result_j0 = True

    def j3_position_command_cb(self, msg):
        self.result_j3 = msg
        self.wait_for_result_j3 = True

    def j4_position_command_cb(self, msg):
        self.result_j4 = msg
        self.wait_for_result_j4 = True


if __name__ == '__main__':
    rospy.init_node('cartesian_controller_test')
    rostest.rosrun(PKG, 'cartesian_controller_test', TestCartesianController)
