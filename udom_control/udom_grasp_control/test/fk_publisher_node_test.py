#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
Integration test for the 'fk_publisher' node.

"""

import numpy as np
import rospy
import unittest
import rostest
import std_msgs.msg
import geometry_msgs.msg
import sensor_msgs.msg
import tf2_ros

PKG = 'udom_grasp_control'


class TestFKPublisher(unittest.TestCase):
    def setUp(self):
        """
        Sets up the test fixture before exercising it.

        """
        # params
        self.result_j0 = None
        self.result_j3 = None
        self.result_j4 = None
        self.result_ee = None
        self.wait_for_result_j0 = None
        self.wait_for_result_j3 = None
        self.wait_for_result_j4 = None
        self.wait_for_result_ee = None

        # publishers
        self.event_out = rospy.Publisher(
            '~event_out', std_msgs.msg.String, latch=True)
        self.joint_states = rospy.Publisher(
            '~joint_states', sensor_msgs.msg.JointState, queue_size=1)

        # subscribers
        self.ee_computed = rospy.Subscriber(
            '~ee_computed', geometry_msgs.msg.PoseStamped, self.ee_computed_cb)

    def tearDown(self):
        """
        Deconstructs the test fixture after testing it.

        """
        self.ee_computed.unregister()
        self.event_out.unregister()

    def test_fk_publisher_node(self):
        """
        Verifies the node's interface is correct.
        Note: this is not a functionality test.

        """
        # These parameters must be the match the ones specified in the .test file.
        reference_frame = 'rh_palm'
        finger_frame = 'rh_ffknuckle'

        joint_states = sensor_msgs.msg.JointState()
        joint_states.name = [
            'rh_LFJ1', 'rh_LFJ2', 'rh_LFJ3', 'rh_LFJ4', 'rh_LFJ5', 'rh_FFJ4',
            'rh_FFJ3', 'rh_FFJ0', 'rh_MFJ1', 'rh_MFJ2', 'rh_MFJ3']
        joint_states.position = np.zeros(len(joint_states.name))
        # Testing for the first finger joints (FFJ)
        joint_states.position[5] = np.radians(0)
        joint_states.position[6] = np.radians(10)
        joint_states.position[7] = np.radians(30)

        # Set transforms.
        transform = geometry_msgs.msg.TransformStamped()
        transform.header.frame_id = reference_frame
        transform.child_frame_id = finger_frame
        transform.transform.translation.x = 0.033
        transform.transform.translation.y = 0.0
        transform.transform.translation.z = 0.095
        transform.transform.rotation.x = 0.0
        transform.transform.rotation.y = 0.0
        transform.transform.rotation.z = 0.0
        transform.transform.rotation.w = 1.0

        broadcaster = tf2_ros.TransformBroadcaster()

        desired = geometry_msgs.msg.PoseStamped()
        desired.pose.position.x = 0.033
        desired.pose.position.y = -0.04
        desired.pose.position.z = 0.178
        desired.pose.orientation.w = 1.0
        desired.header.frame_id = reference_frame

        while not self.wait_for_result_ee:
            transform.header.stamp = rospy.Time.now()
            broadcaster.sendTransform(transform)

            self.joint_states.publish(joint_states)
            self.event_out.publish('e_start')

        self.assertAlmostEqual(
            self.result_ee.pose.position.x, desired.pose.position.x, places=2)
        self.assertAlmostEqual(
            self.result_ee.pose.position.y, desired.pose.position.y, places=2)
        self.assertAlmostEqual(
            self.result_ee.pose.position.z, desired.pose.position.z, places=2)

    def ee_computed_cb(self, msg):
        self.result_ee = msg
        self.wait_for_result_ee = True


if __name__ == '__main__':
    rospy.init_node('fk_publisher_test')
    rostest.rosrun(PKG, 'fk_publisher_test', TestFKPublisher)
