#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
This node controls the fingers of the Shadow hand to move their fingertips to
the desired Cartesian positions. It switches the `trajectory_controller` to
allow for joint position control based on topics. If `e_reset` is send to the
coordinator, the fingers are moved to their rest configuration.

**Input(s):**

  * `event_in`: The desired event for the node:

      `e_start`: starts the component.

      `e_stop`: stops the component.

      `e_reset`: resets the hand to its rest configuration.

    - *type:* `std_msgs/String`

**Output(s):**

  * `event_out`: The current event of the node.

      `e_running`: when the component is running.

      `e_stopped`: when the component is stopped.

    - *type:* `std_msgs/String`

**Parameter(s):**

  * `loop_rate`: Node cycle rate (in Hz).

  * `use_sim`: Set to true if using a simulation.

"""

import rospy
import std_msgs.msg
import controller_manager_msgs.srv


class CartesianArandaCoordinator(object):
    """
    Controls the fingers of the Shadow hand to move their fingertips to
    the desired Cartesian positions. It switches the `trajectory_controller` to
    allow for joint position control based on topics. If `e_reset` is send to the
    coordinator, the fingers are moved to their rest configuration.

    """
    def __init__(self):
        """
        Returns a Aranda controller coordinator.

        :return: Aranda controller coordinator
        :rtype: CartesianArandaCoordinator

        """
        # Params
        self.started_components = False
        self.event = None
        self.ff_controller_status = None
        self.mf_controller_status = None
        self.rf_controller_status = None
        self.lf_controller_status = None
        self.th_controller_status = None
        self.ff_monitor_status = None
        self.mf_monitor_status = None
        self.rf_monitor_status = None
        self.lf_monitor_status = None
        self.th_monitor_status = None

        # If simulation is used, then the controller does not need to be switched.
        self.use_sim = rospy.get_param('~use_sim', False)

        # Node cycle rate (in Hz).
        self.loop_rate = rospy.Rate(rospy.get_param('~loop_rate', 100))

        # Publishers
        self.event_out = rospy.Publisher("~event_out", std_msgs.msg.String, queue_size=10)
        self.start_ff_controller = rospy.Publisher(
            "~start_ff_controller", std_msgs.msg.String, queue_size=10, latch=True)
        self.start_mf_controller = rospy.Publisher(
            "~start_mf_controller", std_msgs.msg.String, queue_size=10, latch=True)
        self.start_rf_controller = rospy.Publisher(
            "~start_rf_controller", std_msgs.msg.String, queue_size=10, latch=True)
        self.start_lf_controller = rospy.Publisher(
            "~start_lf_controller", std_msgs.msg.String, queue_size=10, latch=True)
        self.start_th_controller = rospy.Publisher(
            "~start_th_controller", std_msgs.msg.String, queue_size=10, latch=True)

        self.start_ff_fk_publisher = rospy.Publisher(
            "~start_ff_fk_publisher", std_msgs.msg.String, queue_size=10, latch=True)
        self.start_mf_fk_publisher = rospy.Publisher(
            "~start_mf_fk_publisher", std_msgs.msg.String, queue_size=10, latch=True)
        self.start_rf_fk_publisher = rospy.Publisher(
            "~start_rf_fk_publisher", std_msgs.msg.String, queue_size=10, latch=True)
        self.start_lf_fk_publisher = rospy.Publisher(
            "~start_lf_fk_publisher", std_msgs.msg.String, queue_size=10, latch=True)
        self.start_th_fk_publisher = rospy.Publisher(
            "~start_th_fk_publisher", std_msgs.msg.String, queue_size=10, latch=True)

        # Subscribers
        rospy.Subscriber("~event_in", std_msgs.msg.String, self.event_in_cb)
        rospy.Subscriber(
            "~ff_fk_publisher_status", std_msgs.msg.String, self.ff_controller_status_cb)
        rospy.Subscriber(
            "~mf_fk_publisher_status", std_msgs.msg.String, self.mf_controller_status_cb)
        rospy.Subscriber(
            "~rf_fk_publisher_status", std_msgs.msg.String, self.rf_controller_status_cb)
        rospy.Subscriber(
            "~lf_fk_publisher_status", std_msgs.msg.String, self.lf_controller_status_cb)
        rospy.Subscriber(
            "~th_controller_status", std_msgs.msg.String, self.th_controller_status_cb)

        rospy.Subscriber(
            "~ff_fk_publisher_status", std_msgs.msg.String, self.ff_fk_publisher_status_cb)
        rospy.Subscriber(
            "~mf_fk_publisher_status", std_msgs.msg.String, self.mf_fk_publisher_status_cb)
        rospy.Subscriber(
            "~rf_fk_publisher_status", std_msgs.msg.String, self.rf_fk_publisher_status_cb)
        rospy.Subscriber(
            "~lf_fk_publisher_status", std_msgs.msg.String, self.lf_fk_publisher_status_cb)
        rospy.Subscriber(
            "~th_fk_publisher_status", std_msgs.msg.String, self.th_fk_publisher_status_cb)

        # Services
        controller_to_switch = rospy.get_param(
            '~switch_controller', '/controller_manager/switch_controller')
        rospy.loginfo("Waiting for service '{}'...".format(controller_to_switch))
        rospy.wait_for_service(controller_to_switch)
        self.switch_controller = rospy.ServiceProxy(
            controller_to_switch, controller_manager_msgs.srv.SwitchController)
        rospy.loginfo("Found service '{}'.".format(controller_to_switch))

        # Trajectory controller.
        self.trajectory_controller = rospy.get_param(
            '~trajectory_controller', 'rh_trajectory_controller')

    def event_in_cb(self, msg):
        """
        Obtains the event for the node (e.g. start, stop).

        :param msg: Event message for the node.
        :type msg: std_msgs.msg.String

        """
        self.event = msg.data

    def ff_controller_status_cb(self, msg):
        """
        Obtains the status of the finger controller (as an event).

        :param msg: Event message for the node.
        :type msg: std_msgs.msg.String

        """
        self.ff_controller_status = msg.data

    def mf_controller_status_cb(self, msg):
        """
        Obtains the status of the finger controller (as an event).

        :param msg: Event message for the node.
        :type msg: std_msgs.msg.String

        """
        self.mf_controller_status = msg.data

    def rf_controller_status_cb(self, msg):
        """
        Obtains the status of the finger controller (as an event).

        :param msg: Event message for the node.
        :type msg: std_msgs.msg.String

        """
        self.rf_controller_status = msg.data

    def lf_controller_status_cb(self, msg):
        """
        Obtains the status of the finger controller (as an event).

        :param msg: Event message for the node.
        :type msg: std_msgs.msg.String

        """
        self.lf_controller_status = msg.data

    def th_controller_status_cb(self, msg):
        """
        Obtains the status of the finger controller (as an event).

        :param msg: Event message for the node.
        :type msg: std_msgs.msg.String

        """
        self.th_controller_status = msg.data

    def ff_fk_publisher_status_cb(self, msg):
        """
        Obtains the status of the finger monitor (as an event).

        :param msg: Event message for the node.
        :type msg: std_msgs.msg.String

        """
        self.ff_monitor_status = msg.data

    def mf_fk_publisher_status_cb(self, msg):
        """
        Obtains the status of the finger monitor (as an event).

        :param msg: Event message for the node.
        :type msg: std_msgs.msg.String

        """
        self.mf_monitor_status = msg.data

    def rf_fk_publisher_status_cb(self, msg):
        """
        Obtains the status of the finger monitor (as an event).

        :param msg: Event message for the node.
        :type msg: std_msgs.msg.String

        """
        self.rf_monitor_status = msg.data

    def lf_fk_publisher_status_cb(self, msg):
        """
        Obtains the status of the finger monitor (as an event).

        :param msg: Event message for the node.
        :type msg: std_msgs.msg.String

        """
        self.lf_monitor_status = msg.data

    def th_fk_publisher_status_cb(self, msg):
        """
        Obtains the status of the finger monitor (as an event).

        :param msg: Event message for the node.
        :type msg: std_msgs.msg.String

        """
        self.th_monitor_status = msg.data

    def tear_down(self):
        """
        Switches back to the trajectory controller.

        """
        if self.use_sim:
            return
        rospy.loginfo("Shutting down the hand Cartesian controller node...")
        self.toggle_trajectory_controller('start')
        rospy.loginfo("The Cartesian controller node has been successfully terminated.")

    def start(self):
        """
        Starts the node.

        """
        rospy.loginfo("Ready to start...")
        state = 'INIT'

        while not rospy.is_shutdown():

            if state == 'INIT':
                state = self.init_state()
            elif state == 'HALT':
                state = self.halt_state()
            elif state == 'RUNNING':
                state = self.running_state()

            rospy.logdebug("State: {0}".format(state))
            rospy.logdebug("event: {0}".format(self.event))
            self.loop_rate.sleep()

    def init_state(self):
        """
        Executes the INIT state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        if self.event in ['e_start', 'e_reset']:
            if self.use_sim:
                return 'RUNNING'
            if self.toggle_trajectory_controller('stop'):
                return 'RUNNING'
            else:
                rospy.logerr("Unable to stop trajectory controller.")
                return 'INIT'
        else:
            return 'INIT'

    def halt_state(self):
        """
        Executes the HALT state of the state machine.
        It waits until the component is either reset or started again.

        :return: The updated state.
        :rtype: str

        """
        if self.event == 'e_start':
            status = 'e_started'
            self.event_out.publish(status)
            self.reset_component_data(status)
            if self.use_sim:
                return 'RUNNING'
            if not self.toggle_trajectory_controller('stop'):
                rospy.logerr("Unable to stop trajectory controller.")
            return 'RUNNING'
        elif self.event == 'e_reset':
            status = 'e_restarted'
            self.event_out.publish(status)
            self.reset_component_data(status)
            return 'INIT'
        else:
            return 'HALT'

    def running_state(self):
        """
        Executes the RUNNING state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        self.toggle_components(self.event)

        if self.event == 'e_stop':
            status = 'e_stopped'
            self.event_out.publish(status)
            self.reset_component_data(status)
            if self.use_sim:
                return 'HALT'
            if not self.toggle_trajectory_controller('start'):
                rospy.logerr("Unable to start trajectory controller.")
            return 'HALT'
        elif self.event == 'e_reset':
            status = 'e_restarted'
            self.event_out.publish(status)
            self.reset_component_data(status)
            return 'INIT'
        else:
            return 'RUNNING'

    def toggle_components(self, event):
        """
        Starts or stops the necessary components based on the event.

        :param event: The event that determines either to start or stop the components.
        :type event: str

        """
        if event == 'e_stop':
            self.start_ff_controller.publish('e_stop')
            self.start_mf_controller.publish('e_stop')
            self.start_rf_controller.publish('e_stop')
            self.start_lf_controller.publish('e_stop')
            self.start_th_controller.publish('e_stop')

            self.start_ff_fk_publisher.publish('e_stop')
            self.start_mf_fk_publisher.publish('e_stop')
            self.start_rf_fk_publisher.publish('e_stop')
            self.start_lf_fk_publisher.publish('e_stop')
            self.start_th_fk_publisher.publish('e_stop')
            self.started_components = False

        if event == 'e_reset':
            self.start_ff_controller.publish('e_reset')
            self.start_mf_controller.publish('e_reset')
            self.start_rf_controller.publish('e_reset')
            self.start_lf_controller.publish('e_reset')
            self.start_th_controller.publish('e_reset')
            self.started_components = False

        if event == 'e_start' and not self.started_components:
            self.start_ff_controller.publish('e_start')
            self.start_mf_controller.publish('e_start')
            self.start_rf_controller.publish('e_start')
            self.start_lf_controller.publish('e_start')
            self.start_th_controller.publish('e_start')

            self.start_ff_fk_publisher.publish('e_start')
            self.start_mf_fk_publisher.publish('e_start')
            self.start_rf_fk_publisher.publish('e_start')
            self.start_lf_fk_publisher.publish('e_start')
            self.start_th_fk_publisher.publish('e_start')
            self.started_components = True

    def reset_component_data(self, result):
        """
        Clears the data of the component.

        :param result: The result of the component, e.g. stopped, failure, success.
        :type result: str

        """
        self.toggle_components(result)
        self.ff_controller_status = None
        self.mf_controller_status = None
        self.rf_controller_status = None
        self.lf_controller_status = None
        self.th_controller_status = None
        self.ff_monitor_status = None
        self.mf_monitor_status = None
        self.rf_monitor_status = None
        self.lf_monitor_status = None
        self.th_monitor_status = None

    def toggle_trajectory_controller(self, action):
        """
        Starts or stops the trajectory controller depending on the desired action.

        :param action: To 'start' or 'stop' the controller.
        :type action: str

        :return: True if it was able to toggle the controller.
        :rtype: bool

        """
        if action == 'stop':
            try:
                req = controller_manager_msgs.srv.SwitchControllerRequest()
                req.strictness = req.STRICT
                req.stop_controllers = [self.trajectory_controller]
                res = self.switch_controller(req)
                if res.ok:
                    rospy.loginfo("Successfully stopped trajectory controller.")
                    return True
                else:
                    rospy.logerr("Couldn't stop trajectory controller.")
                    return False
            except rospy.ServiceException, e:
                rospy.logwarn("Service call failed: {}".format(e))
        elif action == 'start':
            try:
                req = controller_manager_msgs.srv.SwitchControllerRequest()
                req.strictness = req.STRICT
                req.start_controllers = [self.trajectory_controller]
                res = self.switch_controller(req)
                if res.ok:
                    rospy.loginfo("Successfully started trajectory controller.")
                    return True
                else:
                    rospy.logerr("Couldn't start trajectory controller.")
                    return False
            except rospy.ServiceException, e:
                rospy.logwarn("Service call failed: {}".format(e))
        else:
            return False


def main():
    rospy.init_node("cartesian_controller", anonymous=True)
    cartesian_controller = CartesianArandaCoordinator()
    rospy.on_shutdown(cartesian_controller.tear_down)
    cartesian_controller.start()
