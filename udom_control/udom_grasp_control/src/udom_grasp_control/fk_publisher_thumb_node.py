#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
This node publishes the end-effector (fingertip) pose of a finger in the given
reference frame.

**Input(s):**

  * `joint_states`: Joint states of the robot.

    - *type:* `sensor_msgs/JointState`

  * `event_in`: The desired event for the node:

      `e_start`: starts the component.

      `e_stop`: stops the component.

    - *type:* `std_msgs/String`

**Output(s):**

  * `ee_computed`: End-effector position computed by the forward kinematics.

    - *type:* `geometry_msgs/PoseStamped`

  * `event_out`: The current event of the node.

      `e_running`: when the component is running.

      `e_stopped`: when the component is stopped.

    - *type:* `std_msgs/String`

**Parameter(s):**

  * `loop_rate`: Node cycle rate (in Hz).

  * `joint_names`: The name of the joints to control (order matters).

  * `finger_frame`: Name of the finger's base frame.

  * `reference_frame`: Reference frame where to publish pose.

"""

import rospy
import tf2_ros
import std_msgs.msg
import geometry_msgs.msg
import sensor_msgs.msg
import udom_grasp_control.finger_ik_utils as utils
import udom_geometric_transformation.transformation_utils as tf_utils


class FKPublisherThumb(object):
    """
    Publishes the end-effector (fingertip) pose of a thumb in the given reference frame.

    """
    def __init__(self):
        """
        Returns a Forward Kinematics publisher for a thumb of the Shadow hand.

        :return: FK publisher.
        :rtype: FKPublisherThumb

        """
        # Params
        self.event = None
        self.joint_states = None

        # Create a thumb object.
        self.thumb = utils.Thumb()

        # Node cycle rate (in Hz).
        self.loop_rate = rospy.Rate(rospy.get_param('~loop_rate', 100))

        # Name of the finger's base frame.
        self.finger_frame = rospy.get_param('~finger_frame', None)
        assert self.finger_frame is not None, "'finger_frame' must be specified."

        # Reference frame, this should match the frame of the target pose.
        self.reference_frame = rospy.get_param('~reference_frame', None)
        assert self.reference_frame is not None, "'reference_frame' must be specified."

        # The name of the joints to control (order matters).
        self.joint_names = rospy.get_param('~joint_names', [])
        assert len(self.joint_names) == 4, \
            "'joint_names' must be specified and it should be of length 4, not {}.".format(
                len(self.joint_names))

        # Object to compute transformations.
        self.listener = tf_utils.GeometryTransformer()

        # Maximum duration to wait for a transform (in seconds).
        self.wait_for_transform = rospy.get_param('~wait_for_transform', 0.1)

        # Publishers
        self.event_out = rospy.Publisher("~event_out", std_msgs.msg.String, queue_size=10)
        self.ee_computed = rospy.Publisher(
            "~ee_computed", geometry_msgs.msg.PoseStamped, queue_size=1)

        # Subscribers
        rospy.Subscriber("~event_in", std_msgs.msg.String, self.event_in_cb)
        rospy.Subscriber(
            "~joint_states", sensor_msgs.msg.JointState, self.joint_states_cb)

    def event_in_cb(self, msg):
        """
        Obtains the event for the node (e.g. start, stop).

        :param msg: Event message for the node.
        :type msg: std_msgs.msg.String

        """
        self.event = msg.data

    def joint_states_cb(self, msg):
        """
        Obtains the joint state of the robot.

        :param msg: Current joint state of the robot.
        :type msg: sensor_msgs.msg.JointState

        """
        self.joint_states = msg

    def start(self):
        """
        Starts the node.

        """
        rospy.loginfo("Ready to start...")
        state = 'INIT'

        while not rospy.is_shutdown():

            if state == 'INIT':
                state = self.init_state()
            elif state == 'IDLE':
                state = self.idle_state()
            elif state == 'RUNNING':
                state = self.running_state()

            rospy.logdebug("State: {0}".format(state))
            rospy.logdebug("event: {0}".format(self.event))
            self.loop_rate.sleep()

    def init_state(self):
        """
        Executes the INIT state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        if self.event == 'e_start':
            return 'IDLE'
        else:
            return 'INIT'

    def idle_state(self):
        """
        Executes the IDLE state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        if self.event == 'e_stop':
            self.event_out.publish('e_stopped')
            self.reset_component_data()
            return 'INIT'
        elif self.joint_states is not None:
            return 'RUNNING'
        else:
            return 'IDLE'

    def running_state(self):
        """
        Executes the RUNNING state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        if self.event == 'e_stop':
            self.event_out.publish('e_stopped')
            self.reset_component_data()
            return 'INIT'
        else:
            joints = utils.extract_positions(self.joint_states, self.joint_names)
            ee_pose = self.compute_ee_pose(joints)
            if ee_pose is not None:
                self.ee_computed.publish(ee_pose)
            self.reset_component_data()
            return 'IDLE'

    def compute_ee_pose(self, joints):
        """
        Computes the end-effector pose based on the joints in the reference frame.

        :param joints: Joint values of the thumb.
        :type joints: list

        :return: The pose of the end-effector.
        :rtype: list or None.

        """
        try:
            ee_computed = geometry_msgs.msg.PoseStamped()
            ee_computed.header.frame_id = self.finger_frame

            # Hack since the base frame has the same rotation as the joint_5.
            joints[0] = 0
            x, y, z = self.thumb.compute_fk(joints)
            ee_computed.pose.position.x = x
            ee_computed.pose.position.y = y
            ee_computed.pose.position.z = z

            ee_computed.header.stamp = rospy.Time.now()
            return self.listener.transform_pose(ee_computed, self.reference_frame)

        except (tf2_ros.LookupException, tf2_ros.ConnectivityException,
                tf2_ros.ExtrapolationException) as e:
            rospy.logwarn("Error while transforming the computed pose.\n{}".format(e))
            return None

    def reset_component_data(self):
        """
        Clears the data of the component.

        """
        self.event = None


def main():
    rospy.init_node("fk_publisher", anonymous=True)
    fk_publisher = FKPublisherThumb()
    fk_publisher.start()
