#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
This modules defines utility functions for the Shadow finger Cartesian controller.


"""

import collections
import numpy as np
import scipy.optimize as so


def extract_positions(joint_state, joint_names):
    """
    Extracts the joint position values of the given joints in the
    specified order.

    :param joint_state: Joint state of the robot.
    :type joint_names: sensor_msgs.msg.JointState

    :returns: The joint positions of the given joint_names.
    :rtype: list

    """
    position = []

    for name in joint_names:
        for ii, joint in enumerate(joint_state.name):
            if joint == name:
                position.append(joint_state.position[ii])

    return position


def compute_t_matrix(a, d, alpha, theta):
    r"""Computes the homogeneous transform matrix based on the
    DH-parameters.

    Parameters
    ----------
    :param a: Distance from :math:`Z_{i-1}` to :math:`Z_{i}` measured along
        :math:`X_{i-1}` (in meters).
    :type a: float

    :param d: Distance from :math:`X_{i-1}` to :math:`X_{i}` measured along
        :math:`Z_{i}` (in meters).
    :type d: float

    :param alpha: Angle from :math:`Z_{i-1}` to :math:`Z_{i}` measured about
        :math:`X_{i-1}` (in radians).
    :type alpha: float

    :param theta: Angle from :math:`X_{i-1}` to :math:`X_{i}` measured about
        :math:`Z_{i}` (in radians).
    :type theta: float

    Returns
    ----------
    :returns: The homogeneous transform matrix.
    :rtype: np.array of size (4, 4).

    """
    c_alpha = np.cos(alpha)
    s_alpha = np.sin(alpha)
    c_theta = np.cos(theta)
    s_theta = np.sin(theta)

    return np.array([
        [c_theta, - s_theta, 0.0, a],
        [s_theta * c_alpha, c_theta * c_alpha, - s_alpha, - s_alpha * d],
        [s_theta * s_alpha, c_theta * s_alpha, c_alpha, c_alpha * d],
        [0.0, 0.0, 0.0, 1.0]])


class Finger(object):
    """Finger class for the Shadow robot hand."""

    def __init__(self, finger_frame="rh_ffknuckle"):
        """
        Sets the Denavit–Hartenberg parameters for the finger and its
        minimum and maximum joint angles.

        :param finger_frame: Finger frame used to decide which DH parameters to load.
        :type finger_frame: str

        """
        # Rest configuration
        self.q_rest = np.radians([0.0, 20.0, 20.0]).tolist()

        # Current configuration
        self.q = [0.0, 0.0, 0.0]

        if 'ff' in finger_frame or 'mf' in finger_frame:
            self.dh_params = collections.OrderedDict()
            # We assume the base frame is the 'knuckle' frame (Z points to the next joint).
            self.dh_params['j4'] = collections.OrderedDict(
                {'a': 0.0, 'd': 0.0, 'alpha': np.pi / 2, 'theta': np.pi / 2})
            self.dh_params['j3'] = collections.OrderedDict(
                {'a': 0.0, 'd': 0.0, 'alpha': np.pi / 2, 'theta': 0.0})
            self.dh_params['j0'] = collections.OrderedDict(
                {'a': 45 / 1000., 'd': 0.0, 'alpha': 0.0, 'theta': 0.0})
            self.dh_params['ee'] = collections.OrderedDict(
                {'a': 51 / 1000., 'd': 0.0, 'alpha': np.radians(6.75), 'theta': 0.0})
        elif 'lf' in finger_frame or 'rf' in finger_frame:
            self.dh_params = collections.OrderedDict()
            # We assume the base frame is the 'knuckle' frame (Z points to the next joint).
            self.dh_params['j4'] = collections.OrderedDict(
                {'a': 0.0, 'd': 0.0, 'alpha': -np.pi / 2, 'theta': -np.pi / 2})
            self.dh_params['j3'] = collections.OrderedDict(
                {'a': 0.0, 'd': 0.0, 'alpha': -np.pi / 2, 'theta': 0.0})
            self.dh_params['j0'] = collections.OrderedDict(
                {'a': 45 / 1000., 'd': 0.0, 'alpha': 0.0, 'theta': 0.0})
            self.dh_params['ee'] = collections.OrderedDict(
                {'a': 51 / 1000., 'd': 0.0, 'alpha': -np.radians(6.75), 'theta': 0.0})
        else:
            raise Exception("finger_frame: '{}' is not recognized.".format(finger_frame))

        self.max_joints = [np.pi / 9, np.pi / 2, 1.90]
        self.min_joints = [-np.pi / 9, 0, np.pi / 9]

    def compute_fk(self, joints):
        """
        Computes the forward kinematics of the finger based on the joint values.
        The order of the joints is assumed to be the same order in the DH params.

        :param joints: Joint configuration (in radians).
        :type joints: list

        :return: Fingertip position (in meters).
        :rtype: np.array

        """
        assert len(joints) == len(self.dh_params) - 1, \
            "'joints 'q' must match the size of DH params ({}, is: {}).".format(
                len(self.dh_params) - 1, len(joints))
        joints = list(joints)
        joints.append(0.0)

        t_final = np.identity(4)
        for ii, dh in enumerate(self.dh_params.itervalues()):
            a, alpha, d, theta = dh.values()
            theta += joints[ii]
            t_i = compute_t_matrix(a, d, alpha, theta)
            t_final = np.dot(t_final, t_i)

        return np.dot(t_final, np.array([0, 0, 0, 1], dtype=float))[:3]

    def compute_ik(self, ee_position, tolerance=1e-6):
        """
        Computes the inverse kinematics of the finger based on the end effector
        position (3D position in Cartesian space).

        Code based from:
            https://github.com/studywolf/blog/blob/master/InvKin/Arm.py

        :param ee_position: Desired position for the fingertip (in meters).
        :type ee_position: list

        :param tolerance: Error tolerance to find a solution.
        :type tolerance: float

        :return: The joint configuration that approximates the target position (in radians).
        :rtype: list

        """

        def distance_to_default(q, *args):
            """
            Calculates the Euclidean distance through joint space to the rest configuration.
            The weight list allows the penalty of each joint being away from the rest
            position to be scaled differently. Higher weighs will keep the joint closer to
            the rest configuration.

            :param q: Joint configuration (in radians).
            :type q: list

            :return: Euclidean distance to the rest configuration (in radians).
            :rtype: float

            """
            # TODO: check if this is passed as argument
            weight = np.ones(len(q)).tolist()
            return np.sqrt(np.sum(
                [(qi - q0i) ** 2 * wi for qi, q0i, wi in zip(q, self.q, weight)]))

        def xyz_constraint(q, xyz):
            """
            Computes the Frobenius norm between the position given by the joint
            configuration 'q' and the target position 'xyz'.

            :param q: Joint configuration (in radians).
            :type q: list

            :param xyz: Target position in the 3D Cartesian space (in meters).
            :type xyz: list

            :return: The distance between the computed and desired point (in meters).
            :rtype: float

            """
            res = np.array(self.compute_fk(q))
            return np.linalg.norm(res - xyz)

        def joint_limits_upper(q, xyz):
            """
            Computes the distance between the current joint configuration and their
            maximum joint values. If the constraint is met, then all values are
            positive.

            :param q: Joint configuration (in radians).
            :type q: list

            :param xyz: Target position in the 3D Cartesian space (not used).
            :type xyz: list

            :return: Distance between the current joint configuration and their
                maximum joint values.
            :rtype: np.array

            """
            return self.max_joints - np.array(q)

        def joint_limits_lower(q, xyz):
            """
            Computes the distance between the current joint configuration and their
            minimum joint values. If the constraint is met, then all values are
            positive.

            :param q: Joint configuration (in radians).
            :type q: list

            :param xyz: Target position in the 3D Cartesian space (not used).
            :type xyz: list

            :return: Distance between the current joint configuration and their
                minimum joint values (in radians).
            :rtype: np.array

            """
            return np.array(q) - self.min_joints

        return so.fmin_slsqp(
            func=distance_to_default, x0=self.q, eqcons=[xyz_constraint], iprint=0,
            ieqcons=[joint_limits_upper, joint_limits_lower], args=(ee_position,),
            acc=tolerance)


class Thumb(Finger):
    """Thumb class for the Shadow robot hand. It inherits the methods of the finger class.
    """

    def __init__(self):
        """
        Sets the Denavit–Hartenberg parameters for the thumb and its
        minimum and maximum joint angles.

        """
        super(Thumb, self).__init__()
        # Rest configuration
        self.q_rest = np.radians([20.0, 60.0, 0.0, 0.0]).tolist()

        # Current configuration
        self.q = [0.0, 0.0, 0.0, 0.0]

        self.dh_params = collections.OrderedDict()
        # We assume the base frame is the 'base' frame (Z points to the next joint).
        self.dh_params['j5'] = collections.OrderedDict(
            {'a': 0.0, 'd': 0.0, 'alpha': np.pi, 'theta': -np.pi / 2})
        self.dh_params['j4'] = collections.OrderedDict(
            {'a': 0.0, 'd': 0.0, 'alpha': -np.pi / 2, 'theta': np.pi / 2})
        self.dh_params['j3'] = collections.OrderedDict(
            {'a': 38 / 1000., 'd': 0.0, 'alpha': 0.0, 'theta': 0.0})
        self.dh_params['j2'] = collections.OrderedDict(
            {'a': 10 / 1000., 'd': 0.0, 'alpha': -np.pi / 2, 'theta': 0.0})
        self.dh_params['ee'] = collections.OrderedDict(
            {'a': 51 / 1000., 'd': 0.0, 'alpha': np.radians(7.9), 'theta': 0.0})

        self.max_joints = np.radians([60, 70, 12, 40]).tolist()
        self.min_joints = np.radians([-60, 0, -12, 0]).tolist()
