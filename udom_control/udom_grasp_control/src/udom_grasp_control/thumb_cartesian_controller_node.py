#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
This node controls the thumb of the Shadow hand by publishing joint values to
reach a desired Cartesian position for the fingertip. An `e_reset` event moves
the fingers to their rest configuration.

**Input(s):**

  * `ee_position`: End-effector position (i.e. fingertip) to move the thumb.

    - *type:* `geometry_msgs/PoseStamped`

  * `joint_states`: Joint states of the robot.

    - *type:* `sensor_msgs/JointState`

  * `event_in`: The desired event for the node:

      `e_start`: starts the component.

      `e_reset`: resets the component.

      `e_stop`: stops the component.

    - *type:* `std_msgs/String`

**Output(s):**

  * `j2_position_command`: Joint position value for the joint 2 to reach the desired
    end-effector position.

    - *type:* `std_msgs/Float64`

  * `j3_position_command`: Joint position value for the joint 3 to reach the desired
    end-effector position.

    - *type:* `std_msgs/Float64`

  * `j4_position_command`: Joint position value for the joint 4 to reach the desired
    end-effector position.

    - *type:* `std_msgs/Float64`

  * `j5_position_command`: Joint position value for the joint 5 to reach the desired
    end-effector position.

    - *type:* `std_msgs/Float64`

  * `event_out`: The current event of the node.

      `e_running`: when the component is running.

      `e_stopped`: when the component is stopped.

    - *type:* `std_msgs/String`

**Parameter(s):**

  * `loop_rate`: Node cycle rate (in Hz).

  * `angle_step`: Maximum joint angle increment allowed for the controller (in radians).

  * `joint_names`: The name of the joints to control (order matters).

  * `finger_frame`: Name of the thumb's base frame.

  * `reference_frame`: Reference frame, this should match the frame of the target pose.

  * `tolerance`: Error tolerance to not call the IK solver.

"""

import numpy as np
import rospy
import tf2_ros
import std_msgs.msg
import geometry_msgs.msg
import sensor_msgs.msg
import udom_grasp_control.finger_ik_utils as utils
import udom_geometric_transformation.transformation_utils as tf_utils


class ThumbControllerNode(object):
    """
    Controls the thumb of the Shadow hand by publishing joint values to
    reach a desired Cartesian position for the fingertip.

    """
    def __init__(self):
        """
        Returns a Cartesian controller for a thumb of the Shadow hand.

        :return: Thumb controller.
        :rtype: ThumbControllerNode

        """
        # Params
        self.event = None
        self.ee_position = None
        self.joint_states = None

        # Past target position.
        self.ee_position_past = geometry_msgs.msg.PoseStamped()

        # Create a thumb object.
        self.thumb = utils.Thumb()

        # Node cycle rate (in Hz).
        self.loop_rate = rospy.Rate(rospy.get_param('~loop_rate', 100))

        # Error tolerance to not call the IK solver.
        self.tolerance = rospy.get_param('~tolerance', 0.01)

        # Maximum joint angle increment allowed for the controller (in radians).
        self.angle_step = rospy.get_param('~angle_step', np.radians(5))

        # Name of the thumb's base frame.
        self.finger_frame = rospy.get_param('~finger_frame', None)
        assert self.finger_frame is not None, "'finger_frame' must be specified."

        # Reference frame, this should match the frame of the target pose.
        self.reference_frame = rospy.get_param('~reference_frame', None)
        assert self.reference_frame is not None, "'reference_frame' must be specified."

        # The name of the joints to control (order matters).
        self.joint_names = rospy.get_param('~joint_names', [])
        assert len(self.joint_names) == 4, \
            "'joint_names' must be specified and it should be of length 4, not {}.".format(
                len(self.joint_names))

        # Object to compute transformations.
        self.listener = tf_utils.GeometryTransformer()

        # Maximum duration to wait for a transform (in seconds).
        self.wait_for_transform = rospy.get_param('~wait_for_transform', 0.1)

        # Publishers
        self.event_out = rospy.Publisher("~event_out", std_msgs.msg.String, queue_size=10)
        self.pub_joint_2 = rospy.Publisher(
            "~j2_position_command", std_msgs.msg.Float64, queue_size=10)
        self.pub_joint_3 = rospy.Publisher(
            "~j3_position_command", std_msgs.msg.Float64, queue_size=10)
        self.pub_joint_4 = rospy.Publisher(
            "~j4_position_command", std_msgs.msg.Float64, queue_size=10)
        self.pub_joint_5 = rospy.Publisher(
            "~j5_position_command", std_msgs.msg.Float64, queue_size=10)

        # Subscribers
        rospy.Subscriber("~event_in", std_msgs.msg.String, self.event_in_cb)
        rospy.Subscriber(
            "~ee_position", geometry_msgs.msg.PoseStamped, self.ee_position_cb)
        rospy.Subscriber(
            "~joint_states", sensor_msgs.msg.JointState, self.joint_states_cb)

    def event_in_cb(self, msg):
        """
        Obtains the event for the node (e.g. start, stop).

        :param msg: Event message for the node.
        :type msg: std_msgs.msg.String

        """
        self.event = msg.data

    def ee_position_cb(self, msg):
        """
        Obtains the target position for the finger tip.

        :param msg: Desired end-effector position.
        :type msg: geometry_msgs.msg.PointStamped

        """
        self.ee_position = msg

    def joint_states_cb(self, msg):
        """
        Obtains the joint state of the robot.

        :param msg: Current joint state of the robot.
        :type msg: sensor_msgs.msg.JointState

        """
        self.joint_states = msg

    def start(self):
        """
        Starts the node.

        """
        rospy.loginfo("Ready to start...")
        state = 'INIT'

        while not rospy.is_shutdown():

            if state == 'INIT':
                state = self.init_state()
            elif state == 'IDLE':
                state = self.idle_state()
            elif state == 'RUNNING':
                state = self.running_state()

            rospy.logdebug("State: {0}".format(state))
            rospy.logdebug("event: {0}".format(self.event))
            self.loop_rate.sleep()

    def init_state(self):
        """
        Executes the INIT state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        if self.event == 'e_start':
            return 'IDLE'
        else:
            return 'INIT'

    def idle_state(self):
        """
        Executes the IDLE state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        if self.event == 'e_stop':
            self.pub_joint_5.publish(self.thumb.q[0])
            self.pub_joint_4.publish(self.thumb.q[1])
            self.pub_joint_3.publish(self.thumb.q[2])
            self.pub_joint_2.publish(self.thumb.q[3])
            self.event_out.publish('e_stopped')
            self.reset_component_data()
            return 'INIT'
        elif self.ee_position is not None and self.joint_states is not None:
            return 'RUNNING'
        else:
            return 'IDLE'

    def running_state(self):
        """
        Executes the RUNNING state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        if self.event == 'e_stop':
            self.pub_joint_5.publish(self.thumb.q[0])
            self.pub_joint_4.publish(self.thumb.q[1])
            self.pub_joint_3.publish(self.thumb.q[2])
            self.pub_joint_2.publish(self.thumb.q[3])
            self.event_out.publish('e_stopped')
            self.reset_component_data()
            return 'INIT'
        elif self.event == 'e_reset':
            self.pub_joint_5.publish(self.thumb.q_rest[0])
            self.pub_joint_4.publish(self.thumb.q_rest[1])
            self.pub_joint_3.publish(self.thumb.q_rest[2])
            self.pub_joint_2.publish(self.thumb.q_rest[3])
            self.event_out.publish('e_reset')
            self.reset_component_data()
            return 'INIT'
        else:
            joints = self.compute_joints()
            self.ee_position_past.pose.position = self.ee_position.pose.position
            if joints is not None:
                self.pub_joint_5.publish(joints[0])
                self.pub_joint_4.publish(joints[1])
                self.pub_joint_3.publish(joints[2])
                self.pub_joint_2.publish(joints[3])
            else:
                rospy.logwarn("Error while computing the inverse kinematics.")
            self.reset_component_data()
            return 'IDLE'

    def compute_joints(self):
        """
        Computes the joint values that reach the desired end effector position.
        If there is an error it returns None.

        :return: The position values for each joint.
        :rtype: list or None.

        """
        try:
            # Update finger positions to current ones.
            self.thumb.q = utils.extract_positions(self.joint_states, self.joint_names)

            current = self.thumb.compute_fk(self.thumb.q)

            xyz = np.array([
                self.ee_position.pose.position.x, self.ee_position.pose.position.y,
                self.ee_position.pose.position.z])

            transform_matrix = self.listener.get_transform_matrix(
                self.finger_frame, self.ee_position.header.frame_id)

            target = np.dot(transform_matrix, [xyz[0], xyz[1], xyz[2], 1.0])[:3]

            # Check if it is necessary to call the IK solver
            error = np.linalg.norm(target - current)
            if error <= self.tolerance:
                return self.thumb.q

            # TODO: Handle what happens when results has NaN
            q = self.thumb.compute_ik(target)

            # Clip the joint angles such that they can only move a +/- angle step from their
            # current configuration.
            min_angles = np.array(self.thumb.q) - [
                self.angle_step / 6.0, self.angle_step, self.angle_step, np.pi]
            max_angles = np.array(self.thumb.q) + [
                self.angle_step / 6.0, self.angle_step, self.angle_step, np.pi]
            q = np.clip(q, min_angles, max_angles)
            return np.round(q, 3)
        except (tf2_ros.LookupException, tf2_ros.ConnectivityException,
                tf2_ros.ExtrapolationException) as e:
            rospy.logwarn("Error while transforming the target pose.\n{}".format(e))
            return None

    def reset_component_data(self):
        """
        Clears the data of the component.

        """
        self.ee_position = None
        self.joint_states = None
        self.event = None


def main():
    rospy.init_node("thumb_controller", anonymous=True)
    thumb_controller = ThumbControllerNode()
    thumb_controller.start()
