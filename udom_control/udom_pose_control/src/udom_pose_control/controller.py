#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
This modules defines diverse controllers (e.g. PID).


"""


class PID(object):
    """
    PID controller.

    """
    def __init__(self, dt, p=0.2, i=0.0, d=0.0):
        """
        Instantiates a PID controller.

        :param dt: Sample time.
        :type dt: float

        :param p: Proportional gain.
        :type p: float

        :param i: Integral gain.
        :type i: float

        :param d: Derivative gain.
        :type d: float

        :return: Controller output.
        :rtype: float

        """
        self.dt = dt
        self.k_p = p
        self.k_i = i
        self.k_d = d

        self.pre_error = 0
        self.cum_error = 0

    def compute(self, set_point, pv=0.0):
        """
        Computes the output of the PID controller.

        :param set_point: Set point.
        :type set_point: float

        :param pv: Process value.
        :type pv: float

        :return: Controller's output.
        :rtype: float

        """
        error = set_point - pv
        self.cum_error += error * self.dt
        derivative = (error - self.pre_error) / float(self.dt)

        self.pre_error = error

        return (self.k_p * error) + (self.k_i * self.cum_error) + (self.k_d * derivative)
