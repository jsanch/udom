#!/usr/bin/env python

from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup

d = generate_distutils_setup(
    packages=['udom_shape_control'],
    package_dir={
        'udom_shape_control': 'src/udom_shape_control',
    }
)

setup(**d)
