#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import rospy
import std_msgs.msg


class Coordinator(object):
    def __init__(self):
        # Params
        self.started_components = False
        self.event = None
        self.point_extractor_status = None
        self.deformation_model_status = None
        self.shape_predictor_status = None
        self.shape_controller_status = None
        self.mesh_measurer_status = None

        # Node cycle rate (in Hz).
        self.loop_rate = rospy.Rate(rospy.get_param('~loop_rate', 100))

        # Publishers
        self.event_out = rospy.Publisher("~event_out", std_msgs.msg.String, queue_size=10)
        self.start_point_extractor = rospy.Publisher(
            "~start_points_extractor", std_msgs.msg.String, queue_size=10, latch=True)
        self.start_deformation_model = rospy.Publisher(
            "~start_deformation_model", std_msgs.msg.String, queue_size=10, latch=True)
        self.start_shape_predictor = rospy.Publisher(
            "~start_shape_predictor", std_msgs.msg.String, queue_size=10, latch=True)
        self.start_shape_controller = rospy.Publisher(
            "~start_shape_controller", std_msgs.msg.String, queue_size=10, latch=True)
        self.start_mesh_measurer = rospy.Publisher(
            "~start_mesh_measurer", std_msgs.msg.String, queue_size=10, latch=True)

        # Subscribers
        rospy.Subscriber("~event_in", std_msgs.msg.String, self.event_in_cb)
        rospy.Subscriber(
            "~point_extractor_status", std_msgs.msg.String, self.point_extractor_status_cb)
        rospy.Subscriber(
            "~deformation_model_status", std_msgs.msg.String,
            self.deformation_model_status_cb)
        rospy.Subscriber(
            "~shape_predictor_status", std_msgs.msg.String, self.shape_predictor_status_cb)
        rospy.Subscriber(
            "~shape_controller_status", std_msgs.msg.String, self.shape_controller_status_cb)
        rospy.Subscriber(
            "~mesh_measurer_status", std_msgs.msg.String, self.mesh_measurer_status_cb)

    def event_in_cb(self, msg):
        """
        Obtains an event for the component.

        :param msg: Event message for the node.
        :type msg: std_msgs.msg.String

        """
        self.event = msg.data

    def point_extractor_status_cb(self, msg):
        """
        Obtains the status of the points extractor (as an event).

        :param msg: Event message for the node.
        :type msg: std_msgs.msg.String

        """
        self.point_extractor_status = msg.data

    def deformation_model_status_cb(self, msg):
        """
        Obtains the status of the deformation model (as an event).

        :param msg: Event message for the node.
        :type msg: std_msgs.msg.String

        """
        self.deformation_model_status = msg.data

    def shape_predictor_status_cb(self, msg):
        """
        Obtains the status of the shape predictor (as an event).

        :param msg: Event message for the node.
        :type msg: std_msgs.msg.String

        """
        self.shape_predictor_status = msg.data

    def shape_controller_status_cb(self, msg):
        """
        Obtains the status of the shape controller (as an event).

        :param msg: Event message for the node.
        :type msg: std_msgs.msg.String

        """
        self.shape_controller_status = msg.data

    def mesh_measurer_status_cb(self, msg):
        """
        Obtains the status of the mesh measurer (as an event).

        :param msg: Event message for the node.
        :type msg: std_msgs.msg.String

        """
        self.mesh_measurer_status = msg.data

    def start(self):
        """
        Starts the component.

        """
        rospy.loginfo("Ready to start...")
        state = 'INIT'

        while not rospy.is_shutdown():

            if state == 'INIT':
                state = self.init_state()
            elif state == 'RUNNING':
                state = self.running_state()

            rospy.logdebug("State: {0}".format(state))
            self.loop_rate.sleep()

    def init_state(self):
        """
        Executes the INIT state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        if self.event in ['e_start', 'e_reset']:
            return 'RUNNING'
        else:
            return 'INIT'

    def running_state(self):
        """
        Executes the RUNNING state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        self.toggle_components(self.event)

        if self.event == 'e_stop':
            status = 'e_stopped'
            self.event_out.publish(status)
            self.reset_component_data(status)
            return 'INIT'
        else:
            return 'RUNNING'

    def toggle_components(self, event):
        """
        Starts or stops the necessary components based on the event.

        :param event: The event that determines either to start or stop the components.
        :type event: str

        """
        if event == 'e_stop':
            self.start_point_extractor.publish('e_stop')
            self.start_deformation_model.publish('e_stop')
            self.start_shape_predictor.publish('e_stop')
            self.start_shape_controller.publish('e_stop')
            self.start_mesh_measurer.publish('e_stop')
            self.started_components = False

        if event == 'e_reset':
            self.start_deformation_model.publish('e_reset')
            self.start_shape_controller.publish('e_reset')
            self.start_point_extractor.publish('e_stop')
            self.start_shape_predictor.publish('e_stop')
            self.start_mesh_measurer.publish('e_stop')
            self.event = 'e_start'
            self.started_components = False

        if event == 'e_start' and not self.started_components:
            self.start_point_extractor.publish('e_start')
            self.start_deformation_model.publish('e_start')
            self.start_shape_predictor.publish('e_start')
            self.start_shape_controller.publish('e_start')
            self.start_mesh_measurer.publish('e_start')
            self.started_components = True

    def reset_component_data(self, result):
        """
        Clears the data of the component.

        :param result: The result of the component, e.g. stopped, failure, success.
        :type result: str

        """
        self.toggle_components(result)
        self.point_extractor_status = None
        self.deformation_model_status = None
        self.shape_predictor_status = None
        self.shape_controller_status = None
        self.mesh_measurer_status = None


def main():
    rospy.init_node("coordinator", anonymous=True)
    coordinator = Coordinator()
    coordinator.start()
