#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
This modules defines utility functions for the Aranda's controller node.


"""

import numpy as np
import scipy.linalg as la
import geometry_msgs.msg


def pose_array_to_matrix(array):
    """
    Converts a pose array into a matrix where the rows represent the number of poses.
    The number of columns is assumed to be 3 (as the poses are assumed to be
    three-dimensional points).

    :param array: Array to convert.
    :type array: geometry_msgs.msg.PoseArray

    :return: Converted array as matrix.
    :rtype: np.ndarray

    """
    cols = []

    for pose in array.poses:
        cols.append([pose.position.x, pose.position.y, pose.position.z])
    return np.array(cols).reshape((len(array.poses), 3))


def matrix_to_pose_array(matrix):
    """
    Converts a matrix into a pose array where the rows represent the number of poses.
    The number of columns is assumed to be 3 (as the poses are assumed to be
    three-dimensional points).

    :param matrix: Matrix to convert.
    :type matrix: np.ndarray

    :return: Converted matrix as array.
    :rtype: geometry_msgs.msg.PoseArray

    """
    pose_array = geometry_msgs.msg.PoseArray()
    pose_array.poses = []

    for row in matrix:
        pose_array.poses.append(
            geometry_msgs.msg.Pose(position=geometry_msgs.msg.Point(row[0], row[1], row[2])))

    return pose_array


class ArandasController(object):
    """
    Computes a command to move a set of points from a current configuration to
    a target configuration.

    """
    def __init__(
            self, n, gain=1e-2, weights=(0.1, 0.1, 0.1, 0.1, 0.1), scale=1.0,
            saturation=0.03):
        """
        Instantiates an Aranda's controller.

        :param n: Number of points to control.
        :type n: int

        :param gain: Controller gain.
        :type gain: double

        :param weights: Weights for each term in the controller.
        :type weights: list

        :param scale: Desired scale.
        :type scale: float

        :param saturation: Saturation threshold.
        :type saturation: float

        :return: Aranda's controller
        :rtype: ArandasController

        """
        assert len(weights) == 5, "weights must be of dimension 5."
        self.n = n
        self.gain = gain
        self.weights = weights
        self.scale = scale
        self.saturation = saturation
        self.K_b = np.identity(self.n) - (1.0/self.n * np.ones((self.n, self.n)))

    def compute_optimal_rotation_matrix(self, current, target):
        """
        Computes the optimal rotation matrix between two data sets of points using the
        Kabsch algorithm ([1]_). Each input matrix is a set of points (the rows of
        the matrix). The dimension of the space is the number of columns of
        matrix, e.g. (N, 3) represents N three-dimensional points.


        [1] https://en.wikipedia.org/wiki/Kabsch_algorithm

        :param current: Current matrix.
        :type current: np.ndarray

        :param target: Target matrix.
        :type target: np.ndarray

        :return: Optimal rotation matrix between the two matrices.
        :rtype: np.ndarray

        References
        ----------
        .. [1] https://en.wikipedia.org/wiki/Kabsch_algorithm

        """
        C_b = np.dot(target.T, self.K_b).T
        Q_b = np.dot(current.T, self.K_b).T

        M = np.dot(C_b.T, Q_b)

        u, s, vt = la.svd(M)

        d = la.det(vt.dot(u.T))

        d_matrix = np.identity(3)
        d_matrix[2, 2] = d
        return np.dot(vt.T, np.dot(d_matrix, u.T))

    def compute_similarity(self, current, target):
        """
        Computes a similarity measure between two data sets of points. Each input matrix
        is a set of points (the rows of the matrix). The dimension of the space is the
        number of columns of matrix, e.g. (N, 3) represents N three-dimensional points.

        :param current: Current matrix.
        :type current: np.ndarray

        :param target: Target matrix.
        :type target: np.ndarray

        :return: Similarity measure.
        :rtype: np.ndarray

        """
        R = self.compute_optimal_rotation_matrix(current, target)

        C_b = np.dot(target.T, self.K_b).T
        Q_b = np.dot(current.T, self.K_b).T

        c_s = np.trace(np.dot(C_b.T, C_b))

        # Optimal scale
        s = np.trace(np.dot(Q_b, np.dot(R, C_b.T))) / c_s

        return s * R

    def compute_command(self, current, target, desired_centroid=None):
        """
        Computes the control law to move the points based on the weights of the different
        terms: similarity, affine.

        :param current: Current matrix.
        :type current: np.ndarray

        :param target: Target matrix.
        :type target: np.ndarray

        :return: Similarity measure.
        :rtype: np.ndarray

        """
        C_b = np.dot(target.T, self.K_b).T
        Q_b = np.dot(current.T, self.K_b).T

        H = self.compute_similarity(current, target)
        similarity_term = np.dot(H, C_b.T).T - Q_b

        C_inv = np.linalg.pinv(C_b)
        G = np.dot(Q_b.T, C_inv.T)
        affine_term = np.dot(G, C_b.T).T - Q_b

        current_centroid = (np.dot(current.T, np.ones((self.n, self.n))) * 1.0/self.n).T
        if desired_centroid is not None:
            centroid_term = desired_centroid - current_centroid
        else:
            target_centroid = (np.dot(target.T, np.ones((self.n, self.n))) * 1.0/self.n).T
            centroid_term = target_centroid - current_centroid

        c_s = np.trace(np.dot(C_b.T, C_b))
        R = self.compute_optimal_rotation_matrix(current, target)
        scale_error = self.scale - np.trace(np.dot(Q_b, np.dot(R, C_b.T))) / c_s
        scale_term = Q_b * scale_error

        orientation_size_term = (C_b * self.scale) - Q_b

        command = self.weights[0] * similarity_term + self.weights[1] * affine_term \
            + self.weights[2] * centroid_term + self.weights[3] * scale_term \
            + self.weights[4] * orientation_size_term

        U = self.gain * command

        # Saturate command.
        for ii, point in enumerate(U):
            norm_u = np.linalg.norm(point)
            if norm_u > self.saturation:
                U[ii, :] = self.saturation * U[ii, :] / norm_u

        return current + U

    def compute_destination(self, current, target):
        C_b = np.dot(target.T, self.K_b).T
        current_centroid = (np.dot(current.T, np.ones((self.n, self.n))) * 1.0/self.n).T
        H = self.compute_similarity(current, target)

        return np.dot(H, C_b.T).T + current_centroid

    def compute_destination_g(self, current, target):
        C_b = np.dot(target.T, self.K_b).T
        Q_b = np.dot(current.T, self.K_b).T

        current_centroid = (np.dot(current.T, np.ones((self.n, self.n))) * 1.0/self.n).T
        C_inv = np.linalg.pinv(C_b)
        G = np.dot(Q_b.T, C_inv.T)

        return np.dot(G, C_b.T).T + current_centroid
