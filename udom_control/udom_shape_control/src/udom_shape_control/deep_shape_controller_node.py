#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
This node computes a set of nodal forces to be applied on a mesh in order
to reach a target mesh configuration. It selects the forces that
maximize the similarity between a predicted mesh (using RNNs) and the
target mesh.

**Input(s):**

  * `event_in`: The desired event for the node:

      `e_start`: starts the component.

      `e_stop`: stops the component.

    - *type:* `std_msgs/String`

  * `target_mesh`: Target configuration of the mesh.

    - *type:* `udom_modeling_msgs/Mesh`

  * `current_mesh`: Current configuration of the mesh.

    - *type:* `udom_modeling_msgs/Mesh`

**Output(s):**

  * `force`: The force array to be applied to the mesh's nodes.

    - *type:* `std_msgs/Float32MultiArray`

  * `event_out`: The current event of the node.

      `e_running`: when the component is running.

      `e_stopped`: when the component is stopped.

    - *type:* `std_msgs/String`

**Parameter(s):**

  * `loop_rate`: Node cycle rate (in Hz).

  * `timesteps`: Time steps used for the network model (RNN).

  * `max_force`: The maximum force applied per axis (in N).

  * `scale`: The amount of force to apply as increments/decrements (in N).

  * `p_gain`: Proportional gain for the controller.

  * `d_gain`: Derivative gain for the controller.

  * `beta`: Parameter to control the rigidity/deformation of the intermediate
        shape between the current predicted and target shapes.

  * `model_name`: Name of the model for the trained network.

  * `number_of_nodes`: Number of nodes of the mesh.

  * `target_nodes`: Target nodes used as the input to the network model.

  * `contact_nodes`: Contact nodes (e.g. where the force is applied).

"""

import os
import copy
import yaml
import collections

import numpy as np
import tflearn

import rospy
from dynamic_reconfigure.server import Server
import rospkg
import std_msgs.msg
import udom_modeling_msgs.msg
import udom_geometric_transformation.pose_extractor_utils as utils
import udom_geometric_transformation.mesh_measurer_utils as mesh_utils
from udom_shape_control.cfg import ShapeControllerConfig as ShapeController


class ShapeControllerNode(object):
    """
    Subscribes to two udom_modeling_msgs/Mesh topics, and computes the
    force array to apply to the mesh in order to move the mesh closer
    to the target mesh. It publishes this force array as a
    std_msgs/Float32MultiArray message.

    """
    def __init__(self):
        """
        Instantiates a shape controller node.

        :return: A node that controls the configuration of a mesh.
        :rtype: ShapeControllerNode

        """
        # Params
        self.event = None
        self.target_mesh = None
        self.goal_mesh = None
        self.current_mesh = None

        # Number of nodes of the mesh.
        self.number_of_nodes = rospy.get_param('~number_of_nodes', 152)
        # Target nodes used as the input to the network model.
        self.target_nodes = rospy.get_param(
            '~target_nodes', [57, 61, 123, 107, 111, 106, 4, 5, 121, 6, 62, 120])
        # Contact nodes (e.g. where the force is applied).
        self.contact_nodes = rospy.get_param('~contact_nodes', [6, 62])

        # The maximum force applied per axis (in N).
        self.max_force = rospy.get_param('~max_force', 1)

        # The amount of force to apply as increments/decrements (in N).
        self.scale = rospy.get_param('~scale', 0.001)

        # Proportional gain for the controller.
        self.p_gain = rospy.get_param('~p_gain', 0.5)

        # Derivative gain for the controller.
        self.d_gain = rospy.get_param('~d_gain', 0.1)

        # Tolerance for the controller.
        self.tolerance = rospy.get_param('~tolerance', 0.1)

        # Error history for computing the derivative of the error.
        self.errors = collections.deque([], maxlen=2)

        # Parameter to control the rigidity/deformation of the intermediate
        # shape between the current predicted and target shapes.
        self.beta = rospy.get_param('~beta', 0.5)

        # Number of timesteps used by the network.
        self.timesteps = rospy.get_param('~timesteps', 20)

        # Name of the model for the trained network.
        self.model_name = rospy.get_param('~model_name', 'time_offset_1_rnn')

        # Name of the configuration file with the action space.
        self.actions_config_file = rospy.get_param(
            '~actions_config_file', 'deep_shape_controller_actions.yaml')

        # Input features for the RNN model.
        self.input_features = len(self.target_nodes) * 3 + len(self.contact_nodes) * 3

        # Output features for the RNN model.
        self.output_features = self.number_of_nodes * 3

        # Initialize the force array that will be sent as the controller's output.
        self.force_array = std_msgs.msg.Float32MultiArray()
        self.force_array.data = (np.zeros(self.number_of_nodes * 3)).tolist()

        # Array to collect the inputs for the network model for the specified time steps.
        self.sequence = collections.deque([], self.timesteps)

        # Load the action space.
        ros_pack = rospkg.RosPack()
        package_path = ros_pack.get_path('udom_shape_control')
        config_path = package_path + '/config/'
        with open(config_path + self.actions_config_file, 'r') as stream:
            self.actions = yaml.load(stream)

        # Relative path (ending with a slash '/') where the network model has been saved.
        self.model_path = rospy.get_param(
            '~model_path', 'config/')

        # Build and load the neural network.
        self.model = self.build_network()
        self.model.load(
            os.path.join(os.path.dirname(__file__), self.model_path + self.model_name + '/' +
                         self.model_name + ".tfl"))

        # Node cycle rate (in Hz).
        self.loop_rate = rospy.Rate(rospy.get_param('~loop_rate', 100))

        # Publishers
        self.event_out = rospy.Publisher("~event_out", std_msgs.msg.String, queue_size=10)
        self.force_pub = rospy.Publisher(
            "~force", std_msgs.msg.Float32MultiArray, queue_size=1, tcp_nodelay=True)
        self.disparity = rospy.Publisher(
            "~disparity", std_msgs.msg.Float32, queue_size=10)
        self.mesh_out = rospy.Publisher(
            "~mesh_out", udom_modeling_msgs.msg.Mesh, queue_size=1, tcp_nodelay=True)
        self.predicted_mesh = rospy.Publisher(
            "~predicted_mesh", udom_modeling_msgs.msg.Mesh, queue_size=1, tcp_nodelay=True)

        # Subscribers
        rospy.Subscriber("~event_in", std_msgs.msg.String, self.event_in_cb)
        rospy.Subscriber('~target_mesh', udom_modeling_msgs.msg.Mesh, self.target_mesh_cb)
        rospy.Subscriber('~current_mesh', udom_modeling_msgs.msg.Mesh, self.current_mesh_cb)

        # Dynamic Server Reconfiguration
        dynamic_reconfig_srv = Server(ShapeController, self.dynamic_reconfig_cb)

    def dynamic_reconfig_cb(self, config, level):
        """
        Reconfigures the weights for the disparity measure and the controller gains.

        """
        self.p_gain = config.p_gain
        self.d_gain = config.d_gain
        # self.scale = config.scale
        self.beta = config.beta

        return config

    def build_network(self):
        """
        Builds a neural network model.

        :return: Neural network model.
        :rtype: tflearn.DNN

        """
        input_layer = tflearn.input_data(
            shape=[None, self.timesteps, self.input_features], name='input_layer')
        layer_1 = tflearn.lstm(input_layer, 30, return_seq=True, name='layer1')
        layer_2 = tflearn.lstm(layer_1, 30, name='layer2')
        output_layer = tflearn.fully_connected(
            layer_2, self.output_features, name='output_layer')

        regression_layer = tflearn.regression(
            output_layer, optimizer='sgd', loss='mean_square', metric='R2',
            learning_rate=0.01)

        return tflearn.DNN(
            regression_layer, tensorboard_verbose=0,
            checkpoint_path='{}/{}.tfl.ckpt'.format(os.path.join(
                os.path.dirname(__file__), self.model_path + self.model_name + '/'),
                self.model_name))

    def event_in_cb(self, msg):
        """
        Obtains the event for the node (e.g. start, stop).

        :param msg: Event message for the node.
        :type msg: std_msgs.msg.String

        """
        self.event = msg.data

    def target_mesh_cb(self, msg):
        """
        Obtains the target mesh configuration.

        :param msg: Target mesh.
        :type msg: udom_modeling_msgs.msg.Mesh

        """
        self.target_mesh = msg

    def current_mesh_cb(self, msg):
        """
        Obtains the current mesh configuration.

        :param msg: Current mesh.
        :type msg: udom_modeling_msgs.msg.Mesh

        """
        self.current_mesh = msg

    def start(self):
        """
        Starts the node.

        """
        rospy.loginfo("Ready to start...")
        state = 'INIT'

        while not rospy.is_shutdown():

            if state == 'INIT':
                state = self.init_state()
            elif state == 'IDLE':
                state = self.idle_state()
            elif state == 'RUNNING':
                state = self.running_state()

            rospy.logdebug("State: {0}".format(state))
            self.loop_rate.sleep()

    def init_state(self):
        """
        Executes the INIT state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        if self.event == 'e_start':
            return 'IDLE'
        else:
            return 'INIT'

    def idle_state(self):
        """
        Executes the IDLE state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        if self.event == 'e_stop':
            self.event_out.publish('e_stopped')
            self.reset_component_data()
            return 'INIT'
        elif self.event == 'e_reset':
            self.reset_forces()
            return 'INIT'
        elif (self.current_mesh is not None) and (self.target_mesh is not None):
            self.sequence.append(self.shape_data())
            if len(self.sequence) < self.timesteps:
                self.sequence.append(self.shape_data())
                return 'IDLE'
            return 'RUNNING'
        else:
            return 'IDLE'

    def running_state(self):
        """
        Executes the RUNNING state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        if self.event == 'e_stop':
            self.event_out.publish('e_stopped')
            self.reset_component_data()
            return 'INIT'
        elif self.event == 'e_reset':
            self.reset_forces()
            return 'INIT'
        else:
            disparity = std_msgs.msg.Float32()
            disparity.data = self.compute_optimal_action()

            self.event_out.publish('e_running')
            self.force_pub.publish(self.force_array)
            self.disparity.publish(disparity)
            self.reset_component_data()
            return 'IDLE'

    def shape_data(self):
        """
        Combines the target nodes of the mesh with the force applied to the
        contact nodes into a single array.

        :return: The array combining the input data for the network model.
        :rtype: numpy.ndarray

        """
        mesh = utils.mesh_to_list(self.current_mesh, self.target_nodes)
        forces = utils.extract_forces(self.force_array, self.contact_nodes)
        return np.concatenate((mesh, forces))

    def compute_optimal_action(self):
        """
        Searches the action space to find the action that maximizes the similarity
        to the target shape. It updates teh forces to be applied based on the optimal
        action.

        """
        self.goal_mesh = self.compute_goal_shape()
        self.mesh_out.publish(self.goal_mesh)

        disparity = 1e9
        direction = 'stay'
        # First we find the 'rough' direction that maximizes the similarity.
        for action in self.actions['general_actions']:
            for dd, aa in action.iteritems():
                current_state = copy.deepcopy(self.sequence)

                # Only modify the last step of the sequence.
                current_state[-1][-len(self.contact_nodes) * 3:] += \
                    np.array(aa) * self.scale

                candidate_score = self.compute_disparity(current_state)

                if candidate_score < disparity:
                    disparity = candidate_score
                    direction = dd

        if (direction == 'stay') or (disparity <= self.tolerance):
            self.force_array.data = utils.fill_forces(
                self.sequence[-1][-len(self.contact_nodes) * 3:],
                self.contact_nodes, self.number_of_nodes)
            return disparity

        # Now we only search actions that are in the correct direction.
        disparity = 1e9
        index = 0
        for ii, action in enumerate(self.actions[direction + '_actions']):
                current_state = copy.deepcopy(self.sequence)

                current_state[-1][-len(self.contact_nodes) * 3:] += \
                    np.array(action, dtype=np.float64) * self.scale

                candidate_score = self.compute_disparity(current_state)

                if candidate_score < disparity:
                    disparity = candidate_score
                    index = ii

        # Search for forces in the X direction.
        disparity = 1e9
        x_index = 0
        action = np.array(self.actions[direction + '_actions'][index], dtype=np.float64)

        for ii, x_action in enumerate(self.actions['fx_actions']):
                current_state = copy.deepcopy(self.sequence)

                new_action = action + np.array(x_action, dtype=np.float64)
                current_state[-1][-len(self.contact_nodes) * 3:] += \
                    np.array(new_action, dtype=np.float64) * self.scale

                candidate_score = self.compute_disparity(current_state)

                if candidate_score < disparity:
                    disparity = candidate_score
                    x_index = ii

        x_action = np.array(
            self.actions['fx_actions'][x_index], dtype=np.float64)
        action = np.array(
            self.actions[direction + '_actions'][index], dtype=np.float64)

        self.errors.append(disparity)
        error = self.errors[-1]
        try:
            d_error = np.diff(self.errors[-1])[0]
        except IndexError:
            d_error = self.errors[-1]

        gain = (self.p_gain * error + self.d_gain * d_error)
        self.scale = np.clip(gain, 0.0001, 0.05)
        action = ((action + x_action) * self.scale)

        current_state = self.sequence[-1][-len(self.contact_nodes) * 3:]
        action += current_state
        action = np.clip(action, -self.max_force, self.max_force)

        self.force_array.data = utils.fill_forces(
            action.tolist(), self.contact_nodes, self.number_of_nodes)
        return disparity

    def compute_goal_shape(self):
        """
        Computes an intermediate mesh between the current predicted mesh and the
        target mesh.

        :return: The goal mesh.
        :rtype: udom_modeling_msgs/Mesh

        """
        # Predict mesh points
        x_data = np.array(self.sequence).reshape((
            self.timesteps, self.input_features))
        mesh_points = self.model.predict([x_data])[0]

        # Convert mesh points to a mesh
        predicted_mesh = utils.list_to_mesh(self.current_mesh, mesh_points)

        self.predicted_mesh.publish(predicted_mesh)

        x = mesh_utils.mesh_to_points(predicted_mesh)
        target_mesh = mesh_utils.mesh_to_points(self.target_mesh)

        y = mesh_utils.match_shapes(target_mesh, x, self.beta, quadratic=True)

        # Remove the offset between the shapes.
        goal = y - y.mean(axis=0) + target_mesh.mean(axis=0)

        goal_mesh = utils.list_to_mesh(self.current_mesh, goal.flatten())
        return goal_mesh

    def compute_disparity(self, sequence):
        """
        Computes the Frobenius norm as a measure of disparity between the
        current mesh and the predicted mesh based on a sequence of the mesh
        configuration and the forces acting on the contact nodes.

        :param sequence: The mesh configuration and forces at the contact nodes.
        :type sequence: collections.deque

        :return: The disparity score
        :rtype: float

        """
        # Predict mesh points
        x_data = np.array(sequence).reshape((
            self.timesteps, self.input_features))
        mesh_points = self.model.predict([x_data])[0]

        # Convert mesh points to a mesh
        predicted_mesh = utils.list_to_mesh(self.goal_mesh, mesh_points)

        x = mesh_utils.mesh_to_points(predicted_mesh)
        target_mesh = mesh_utils.mesh_to_points(self.goal_mesh)

        # TODO: We should account for rotation
        return np.linalg.norm(target_mesh - x)

    def reset_forces(self):
        """
        Resets the contact forces to zero.

        """
        self.sequence.clear()
        self.force_array.data = (np.zeros(self.number_of_nodes * 3)).tolist()

    def reset_component_data(self):
        """
        Clears the data of the component.

        """
        self.target_mesh = None
        self.goal_mesh = None
        self.current_mesh = None
        self.event = None


def main():
    rospy.init_node("shape_controller_node", anonymous=True)
    shape_controller_node = ShapeControllerNode()
    shape_controller_node.start()
