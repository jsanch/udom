#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
This node predicts the configuration of a mesh using deep learning based on
the current configuration of the mesh and the forces applied to it.

**Input(s):**

  * `event_in`: The desired event for the node:

      `e_start`: starts the component.

      `e_stop`: stops the component.

    - *type:* `std_msgs/String`

  * `mesh`: Current configuration of the mesh.

    - *type:* `udom_modeling_msgs/Mesh`

  * `force`: Force array applied to the mesh's nodes.

    - *type:* `std_msgs/Float32MultiArray`


**Output(s):**

  * `mesh`: The predicted configuration of the mesh.

    - *type:* `udom_modeling_msgs/Mesh`

  * `event_out`: The current event of the node.

      `e_running`: when the component is running.

      `e_stopped`: when the component is stopped.

    - *type:* `std_msgs/String`

**Parameter(s):**

  * `loop_rate`: Node cycle rate (in Hz).

  * `timesteps`: Time steps used for the network model (RNN).

  * `model_name`: Name of the model for the trained network.

  * `number_of_nodes`: Number of nodes of the mesh.

  * `target_nodes`: Target nodes used as the input to the network model.

  * `contact_nodes`: Contact nodes (e.g. where the force is applied).

"""

import os
import collections

import numpy as np
import tflearn

import rospy
import std_msgs.msg
import udom_modeling_msgs.msg
import udom_geometric_transformation.pose_extractor_utils as utils
import udom_geometric_transformation.mesh_measurer_utils as mesh_utils


class ShapePredictorNode(object):
    """
    Subscribes to a udom_modeling_msgs/Mesh and a std_msgs/Float32MultiArray topics,
    predicts the next configuration of the mesh and publishes it as a
    udom_modeling_msgs/Mesh message.

    """
    def __init__(self):
        """
        Instantiates a shape predictor node.

        :return: A node that predicts a mesh configuration.
        :rtype: ShapePredictorNode

        """
        # Params
        self.event = None
        self.mesh_in = None
        self.force = None

        # Number of nodes of the mesh.
        self.number_of_nodes = rospy.get_param('~number_of_nodes', 152)
        # Target nodes used as the input to the network model.
        self.target_nodes = rospy.get_param(
            '~target_nodes', [57, 61, 123, 107, 111, 106, 4, 5, 121, 6, 62, 120])
        # Contact nodes (e.g. where the force is applied).
        self.contact_nodes = rospy.get_param('~contact_nodes', [2, 62])

        self.timesteps = rospy.get_param('~timesteps', 20)

        # Name of the model for the trained network.
        self.model_name = rospy.get_param('~model_name', 'time_offset_1_rnn')

        # Input features for the RNN model.
        self.input_features = len(self.target_nodes) * 3 + len(self.contact_nodes) * 3

        # Output features for the RNN model.
        self.output_features = self.number_of_nodes * 3

        # Array to collect the inputs for the network model for the specified time steps.
        self.sequence = collections.deque([], self.timesteps)

        # Relative path (ending with a slash '/') where the network model has been saved.
        self.model_path = rospy.get_param(
            '~model_path', 'config/')

        # Build and load the neural network.
        self.model = self.build_network()
        self.model.load(
            os.path.join(os.path.dirname(__file__), self.model_path + self.model_name + '/' +
                         self.model_name + ".tfl"))

        # Node cycle rate (in Hz).
        self.loop_rate = rospy.Rate(rospy.get_param('~loop_rate', 100))

        # Publishers
        self.event_out = rospy.Publisher("~event_out", std_msgs.msg.String, queue_size=10)
        self.disparity = rospy.Publisher("~disparity", std_msgs.msg.Float32, queue_size=10)
        self.mesh_out = rospy.Publisher(
            "~mesh_out", udom_modeling_msgs.msg.Mesh, queue_size=1, tcp_nodelay=True)

        # Subscribers
        rospy.Subscriber("~event_in", std_msgs.msg.String, self.event_in_cb)
        rospy.Subscriber('~mesh_in', udom_modeling_msgs.msg.Mesh, self.mesh_in_cb)
        rospy.Subscriber('~force', std_msgs.msg.Float32MultiArray, self.force_cb)

    def build_network(self):
        """
        Builds a neural network model.

        :return: Neural network model.
        :rtype: tflearn.DNN

        """
        input_layer = tflearn.input_data(
            shape=[None, self.timesteps, self.input_features], name='input_layer')
        layer_1 = tflearn.lstm(input_layer, 30, return_seq=True, name='layer1')
        layer_2 = tflearn.lstm(layer_1, 30, name='layer2')
        output_layer = tflearn.fully_connected(
            layer_2, self.output_features, name='output_layer')

        regression_layer = tflearn.regression(
            output_layer, optimizer='sgd', loss='mean_square', metric='R2',
            learning_rate=0.01)

        return tflearn.DNN(
            regression_layer, tensorboard_verbose=0,
            checkpoint_path='{}/{}.tfl.ckpt'.format(os.path.join(
                os.path.dirname(__file__), self.model_path + self.model_name + '/'),
                self.model_name))

    def event_in_cb(self, msg):
        """
        Obtains the event for the node (e.g. start, stop).

        :param msg: Event message for the node.
        :type msg: std_msgs.msg.String

        """
        self.event = msg.data

    def mesh_in_cb(self, msg):
        """
        Obtains the current mesh configuration.

        :param msg: Current mesh.
        :type msg: udom_modeling_msgs.msg.Mesh

        """
        self.mesh_in = msg

    def force_cb(self, msg):
        """
        Obtains the force being applied to the mesh.

        :param msg: Force applied to the mesh.
        :type msg: std_msgs.msg.Float32MultiArray

        """
        self.force = msg

    def start(self):
        """
        Starts the node.

        """
        rospy.loginfo("Ready to start...")
        state = 'INIT'

        while not rospy.is_shutdown():

            if state == 'INIT':
                state = self.init_state()
            elif state == 'IDLE':
                state = self.idle_state()
            elif state == 'RUNNING':
                state = self.running_state()

            rospy.logdebug("State: {0}".format(state))
            self.loop_rate.sleep()

    def init_state(self):
        """
        Executes the INIT state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        if self.event == 'e_start':
            return 'IDLE'
        else:
            return 'INIT'

    def idle_state(self):
        """
        Executes the IDLE state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        if self.event == 'e_stop':
            self.event_out.publish('e_stopped')
            self.reset_component_data()
            return 'INIT'
        elif (self.mesh_in is not None) and (self.force is not None):
            self.sequence.append(self.shape_data())
            while len(self.sequence) < self.timesteps:
                self.sequence.append(self.shape_data())
            return 'RUNNING'
        else:
            return 'IDLE'

    def running_state(self):
        """
        Executes the RUNNING state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        if self.event == 'e_stop':
            self.event_out.publish('e_stopped')
            self.reset_component_data()
            return 'INIT'
        else:
            # Convert data to query the model.
            x_data = np.array(self.sequence).reshape((
                self.timesteps, self.input_features))

            # Predict using RNN and convert back to a mesh.
            mesh_points = self.model.predict([x_data])[0]
            updated_mesh = utils.list_to_mesh(self.mesh_in, mesh_points)

            disparity = self.compute_disparity(updated_mesh)

            self.event_out.publish('e_running')
            self.mesh_out.publish(updated_mesh)
            self.disparity.publish(disparity)
            self.reset_component_data()
            return 'IDLE'

    def compute_disparity(self, predicted_mesh):
        """
        Computes the Frobenius norm as a measure of disparity between
        the predicted mesh and the current mesh.

        :param predicted_mesh: The predicted mesh.
        :type predicted_mesh: udom_modeling_msgs.msg.Mesh

        :return: Disparity between the two meshes.
        :rtype: Float

        """
        x = mesh_utils.mesh_to_points(predicted_mesh)
        y = mesh_utils.mesh_to_points(self.mesh_in)

        return np.linalg.norm(x - y)

    def shape_data(self):
        """
        Combines the target nodes of the mesh with the force applied to the
        contact nodes into a single array.

        :return: The array combining the input data for the network model.
        :rtype: numpy.ndarray

        """
        mesh = utils.mesh_to_list(self.mesh_in, self.target_nodes)
        forces = utils.extract_forces(self.force, self.contact_nodes)
        return np.concatenate((mesh, forces))

    def reset_component_data(self):
        """
        Clears the data of the component.

        """
        self.mesh_in = None
        self.force = None
        self.event = None


def main():
    rospy.init_node("shape_predictor_node", anonymous=True)
    shape_predictor_node = ShapePredictorNode()
    shape_predictor_node.start()
