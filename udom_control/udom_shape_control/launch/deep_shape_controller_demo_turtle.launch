<?xml version="1.0"?>
<!-- Note: example launch file. Do not modify! -->
<launch>
  <!-- Global parameters. -->
  <arg name="neo_hookean" default="true"/>
  <arg name="loop_rate" default="100"/>
  <arg name="loop_rate_deformation_model" default="100"/>
  <arg name="wait_for_transform" default="0.1"/>
  <arg name="damping_mass" default="1.0"/>
  <arg name="damping_stiffness" default="0.05"/>
  <arg name="mesh_filename" default="turtle"/>
  <arg name="number_of_nodes" default="347"/>
  <arg name="contact_nodes" default="[75, 296]"/>
  <arg name="target_nodes" default="[
    75, 136, 223, 274, 236, 285, 296, 163, 166, 164, 297, 98]"/>
  <arg name="mesh_tolerance" default="0.001"/>
  <arg name="controller_scale" default="0.001"/>
  <arg name="controller_p_gain" default="0.5"/>
  <arg name="controller_d_gain" default="0.1"/>
  <arg name="timesteps" default="20"/>
  <arg name="model_name" default="time_offset_1_rnn_turtle"/>
  <arg name="reference_frame" default="test_rig"/>
  <arg name="zero_index" default="false"/>
  <arg name="target_mesh_config" default="turtle_rest"/>
  <arg name="object_frame" default="object"/>
  <arg name="constrained_nodes" default="[
    1, 2, 3, 7, 8, 9, 16, 17, 18, 22, 23, 24, 31, 32, 33, 34, 35, 36, 37, 38,
    39, 43, 44, 45, 49, 50, 51, 52, 53, 54, 76, 77, 78, 85, 86, 87, 124, 125,
    126, 133, 134, 135, 139, 140, 141, 145, 146, 147, 172, 173, 174, 175, 176,
    177, 178, 179, 180, 739, 740, 741, 742, 743, 744, 766, 767, 768, 793, 794, 795]"/>
  <arg name="timestep" default="0.01"/>
  <arg name="index_start" default="1"/>
  <!-- How often to publish the transforms (in milliseconds). -->
  <arg name="tf_period" default="1"/>

  <!-- Remap topics to connect components' inputs/outputs. -->
  <!-- Deformation model output(s). -->
  <remap from="/udom_shape_control/deformation_model/mesh"
         to="/udom_shape_control/mesh"/>

  <!-- Shape controller input(s). -->
  <remap from="/udom_shape_control/deep_shape_controller/current_mesh"
         to="/udom_shape_control/mesh"/>
  <remap from="/udom_shape_control/deep_shape_controller/target_mesh"
         to="/udom_shape_control/mesh_publisher_gui/mesh"/>

  <!-- Shape controller output(s). -->
  <remap from="/udom_shape_control/deep_shape_controller/force"
         to="/udom_shape_control/deformation_model/force_info"/>

  <!-- Points extractor input(s). -->
  <remap from="/udom_shape_control/points_extractor/mesh"
         to="/udom_shape_control/mesh"/>

  <!-- Mesh measurer input(s). -->
  <remap from="/udom_shape_control/mesh_measurer/mesh_reference"
         to="/udom_shape_control/mesh"/>
  <remap from="/udom_shape_control/mesh_measurer/mesh_target"
         to="/udom_shape_control/mesh_publisher_gui/mesh"/>

  <!-- Mesh visualizer input(s). -->
  <remap from="/mesh_visualizer/mesh" to="/udom_shape_control/mesh"/>

  <!-- Constrained nodes visualizer input(s). -->
  <remap from="/points_visualizer/points_in"
         to="/udom_shape_control/deformation_model/constrained_nodes_visualization"/>

  <!-- Contact nodes visualizer input(s). -->
  <remap from="/udom_shape_control/points_extractor/points"
         to="/contact_nodes_visualizer/points_in"/>

  <!-- Constrained nodes extractor input(s). -->
  <remap from="udom_shape_control/points_extractor/mesh"
         to="/udom_shape_control/mesh"/>

  <!-- Topic re-publisher input(s). -->
  <remap from="/force_mesh_merger/forces"
         to="/udom_shape_control/deformation_model/force_info"/>
  <remap from="/force_mesh_merger/mesh"
         to="/udom_shape_control/mesh"/>

  <!-- Mesh visualizer output(s). -->
  <remap from="/mesh_visualizer/points" to="/udom_shape_control/points"/>

  <!-- Mesh publisher output(s). -->
  <remap from="/udom_shape_control/mesh_publisher_gui/event_out"
         to="/udom_shape_control/event_in"/>

  <!-- Constrained nodes visualizer output(s). -->
  <remap from="/points_visualizer/points_out"
         to="/udom_shape_control/constrained_points"/>

  <!-- Contact nodes visualizer output(s). -->
  <remap from="/contact_nodes_visualizer/points_out"
         to="/udom_shape_control/contact_nodes"/>

  <!-- Deformation sensing pipeline. -->
  <group ns="udom_shape_control">
    <!-- Points extractor. -->
    <include
      file="$(find udom_geometric_transformation)/launch/points_extractor_example.launch">
      <arg name="loop_rate" default="$(arg loop_rate)"/>
      <arg name="nodes" default="$(arg contact_nodes)"/>
    </include>

    <!-- Shape controller. -->
    <include
      file="$(find udom_shape_control)/launch/deep_shape_controller_example.launch">
      <arg name="loop_rate" default="$(arg loop_rate)"/>
      <arg name="number_of_nodes" default="$(arg number_of_nodes)"/>
      <arg name="contact_nodes" default="$(arg contact_nodes)"/>
      <arg name="target_nodes" default="$(arg target_nodes)"/>
      <arg name="tolerance" default="$(arg mesh_tolerance)"/>
      <arg name="scale" default="$(arg controller_scale)"/>
      <arg name="p_gain" default="$(arg controller_p_gain)"/>
      <arg name="d_gain" default="$(arg controller_d_gain)"/>
      <arg name="timesteps" default="$(arg timesteps)"/>
      <arg name="model_name" default="$(arg model_name)"/>
    </include>

    <!-- Mesh publisher. -->
    <include
      file="$(find udom_visualization)/launch/mesh_publisher_gui_example.launch">
      <arg name="loop_rate" default="$(arg loop_rate)"/>
    </include>

    <!-- Mesh measurer. -->
    <include
      file="$(find udom_geometric_transformation)/launch/mesh_measurer_example.launch">
      <arg name="loop_rate" default="$(arg loop_rate)"/>
    </include>

    <!-- Deformation model. -->
    <group if="$(arg neo_hookean)">
      <include
        file="$(find udom_deformation_modeling)/launch/neo_hookean_model_example.launch">
        <arg name="loop_rate" default="$(arg loop_rate_deformation_model)"/>
        <arg name="damping_mass" default="$(arg damping_mass)"/>
        <arg name="damping_stiffness" default="$(arg damping_stiffness)"/>
        <arg name="mesh_filename" default="$(arg mesh_filename).veg"/>
        <arg name="constrained_nodes" default="$(arg constrained_nodes)"/>
        <arg name="timestep" default="$(arg timestep)"/>
        <arg name="index_start" default="$(arg index_start)"/>
      </include>
    </group>
    <!-- Default deformation model is Co-rotational.-->
    <group unless="$(arg neo_hookean)">
      <include
        file="$(find udom_deformation_modeling)/launch/deformation_model_example.launch">
        <arg name="loop_rate" default="$(arg loop_rate_deformation_model)"/>
        <arg name="damping_mass" default="$(arg damping_mass)"/>
        <arg name="damping_stiffness" default="$(arg damping_stiffness)"/>
        <arg name="mesh_filename" default="$(arg mesh_filename).veg"/>
        <arg name="constrained_nodes" default="$(arg constrained_nodes)"/>
        <arg name="timestep" default="$(arg timestep)"/>
        <arg name="index_start" default="$(arg index_start)"/>
      </include>
    </group>

  <!-- Coordinator. -->
  <node pkg="udom_shape_control" type="coordinator_node"
        name="coordinator" output="screen">
      <!-- Node cycle rate (in Hz). -->
      <param name="loop_rate" type="double" value="$(arg loop_rate)"/>

      <!-- Remap topics to interface with the coordinator. -->
      <remap from="~event_in" to="/udom_shape_control/event_in"/>
      <remap from="~event_out" to="/udom_shape_control/event_out"/>
      <remap from="~mesh" to="/udom_shape_control/mesh"/>

      <!-- Remap topics to trigger components. -->
      <remap from="~start_points_extractor"
             to="/udom_shape_control/points_extractor/event_in"/>
      <remap from="~start_deformation_model"
             to="/udom_shape_control/deformation_model/event_in"/>
      <remap from="~start_shape_controller"
             to="/udom_shape_control/deep_shape_controller/event_in"/>
      <remap from="~start_mesh_measurer"
             to="/udom_shape_control/mesh_measurer/event_in"/>

      <!-- Remap topics to get feedback from components. -->
      <remap from="~points_extractor_status"
             to="/udom_shape_control/points_extractor/event_out"/>
      <remap from="~deformation_model_status"
             to="/udom_shape_control/deformation_model/event_out"/>
      <remap from="~shape_controller_status"
         to="/udom_shape_control/deep_shape_controller/event_out"/>
      <remap from="~mesh_measurer_status"
             to="/udom_shape_control/mesh_measurer/event_out"/>
    </node>
  </group>

  <!-- Object to the reference frame. -->
  <node pkg="tf" type="static_transform_publisher" name="object_frame_broadcaster"
        args="0.559 -0.03 0.487 0 0 1.570796 $(arg reference_frame) $(arg object_frame)
        $(arg tf_period)"/>

  <!-- Visualization of current mesh's nodes. -->
  <include
    file="$(find udom_visualization)/launch/mesh_visualizer_example.launch">
    <arg name="loop_rate" default="$(arg loop_rate)"/>
    <arg name="mesh_frame" default="$(arg object_frame)"/>
    <arg name="scale" default="[0.005, 0.005, 0.005]"/>
    <arg name="color" default="[0.0, 1.0, 0.0, 0.5]"/>
  </include>

  <!-- Visualization of predicted mesh's nodes. -->
  <node pkg="udom_visualization" type="mesh_visualizer"
        name="predicted_mesh_visualizer" output="screen">

    <!-- Node cycle rate (in Hz). -->
    <param name="loop_rate" type="double" value="$(arg loop_rate)"/>

    <!-- Reference frame of the mesh to be converted. -->
    <param name="mesh_frame" type="str" value="$(arg object_frame)"/>

    <!-- Marker's scale for the X, Y and Z axes. -->
    <rosparam param="scale" > [0.005, 0.005, 0.005] </rosparam>

    <!-- Marker's color values for the red, green, blue and alpha. -->
    <rosparam param="color" > [0.0, 0.0, 1.0, 0.5] </rosparam>

    <remap from="~mesh" to="/udom_shape_control/mesh_predicted"/>
  </node>

  <!-- Constrained nodes visualizer. -->
  <include
    file="$(find udom_visualization)/launch/points_visualizer_example.launch">
    <arg name="loop_rate" default="$(arg loop_rate)"/>
    <arg name="points_frame" default="$(arg object_frame)"/>
    <arg name="scale" default="[0.005, 0.005, 0.005]"/>
    <arg name="color" default="[1.0, 0.0, 0.0, 0.9]"/>
  </include>

  <!-- Contact nodes visualizer. -->
  <include
    file="$(find udom_visualization)/launch/points_visualizer_example.launch">
    <arg name="node_name" default="contact_nodes_visualizer"/>
    <arg name="loop_rate" default="$(arg loop_rate)"/>
    <arg name="points_frame" default="$(arg object_frame)"/>
    <arg name="scale" default="[0.005, 0.005, 0.005]"/>
    <arg name="color" default="[1.0, 1.0, 1.0, 0.9]"/>
  </include>

  <!-- Mesh visualizer (current). -->
  <node pkg="ucr_experiment_evaluation" type="tet_mesh_to_mesh"
        name="tet_mesh_to_mesh_node" output="screen">
    <!-- Node cycle rate (in Hz). -->
    <param name="loop_rate" type="double" value="$(arg loop_rate)"/>

    <!-- Transparency of the mesh (0.0 is fully transparent). -->
    <param name="alpha" type="double" value="1.0"/>

    <!-- Mesh color in RGB. -->
    <param name="r_color" type="double" value="0.0"/>
    <param name="g_color" type="double" value="1.0"/>
    <param name="b_color" type="double" value="0.0"/>

    <remap from="~mesh_in" to="/udom_shape_control/mesh"/>
    <remap from="~mesh_out" to="/udom_shape_control/mesh_surface"/>
  </node>

  <!-- Mesh visualizer (predicted). -->
  <node pkg="ucr_experiment_evaluation" type="tet_mesh_to_mesh"
        name="predicted_mesh_visualizer_surface" output="screen">
    <!-- Node cycle rate (in Hz). -->
    <param name="loop_rate" type="double" value="$(arg loop_rate)"/>

    <!-- Transparency of the mesh (0.0 is fully transparent). -->
    <param name="alpha" type="double" value="1.0"/>

    <!-- Mesh color in RGB. -->
    <param name="r_color" type="double" value="0.0"/>
    <param name="g_color" type="double" value="0.0"/>
    <param name="b_color" type="double" value="1.0"/>

    <remap from="~mesh_in" to="/udom_shape_control/deep_shape_controller/mesh_out"/>
    <remap from="~mesh_out" to="/udom_shape_control/mesh_predicted_surface"/>
  </node>

  <!-- Mesh visualizer (target). -->
  <node pkg="ucr_experiment_evaluation" type="tet_mesh_to_mesh"
        name="reference_mesh_visualizer" output="screen">

    <!-- Node cycle rate (in Hz). -->
    <param name="loop_rate" type="double" value="$(arg loop_rate)"/>

    <!-- Transparency of the mesh (0.0 is fully transparent). -->
    <param name="alpha" type="double" value="1.0"/>

    <!-- Mesh color in RGB. -->
    <param name="r_color" type="double" value="1.0"/>
    <param name="g_color" type="double" value="0.0"/>
    <param name="b_color" type="double" value="0.0"/>

    <remap from="~mesh_in" to="/udom_shape_control/mesh_publisher_gui/mesh"/>
    <remap from="~mesh_out" to="/udom_shape_control/mesh_target_surface"/>
  </node>

  <!-- Topic re-publisher (to record topics with the same rate). -->
  <include
    file="$(find udom_topic_tools)/launch/force_mesh_merger_example.launch">
    <arg name="loop_rate" default="$(arg loop_rate)"/>
  </include>
</launch>
