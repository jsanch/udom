#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
Integration test for the 'naive_controller' node.

"""

import rospy
import unittest
import rostest
import std_msgs.msg
import geometry_msgs.msg

PKG = 'udom_shape_control'


class TestNaiveController(unittest.TestCase):
    def setUp(self):
        """
        Sets up the test fixture before exercising it.

        """
        # params
        self.result = None
        self.wait_for_result = None

        # publishers
        self.event_out = rospy.Publisher('~event_out', std_msgs.msg.String, latch=True)
        self.target_poses = rospy.Publisher(
            '~target', geometry_msgs.msg.PoseArray, queue_size=1)
        self.current_poses = rospy.Publisher(
            '~current', geometry_msgs.msg.PoseArray, queue_size=1)

        # subscribers
        self.poses_out = rospy.Subscriber(
            '~command', geometry_msgs.msg.PoseArray, self.result_callback)

    def tearDown(self):
        """
        Deconstructs the test fixture after testing it.

        """
        self.target_poses.unregister()
        self.current_poses.unregister()
        self.event_out.unregister()
        self.poses_out.unregister()

    def test_naive_controller(self):
        """
        Verifies the node's interface is correct.
        Note: this is not a functionality test.

        """
        expected = geometry_msgs.msg.PoseArray()
        expected.poses = [
            geometry_msgs.msg.Pose(position=geometry_msgs.msg.Point(1.0, 0.0, 0.0)),
            geometry_msgs.msg.Pose(position=geometry_msgs.msg.Point(0.0, 1.0, 0.0)),
            geometry_msgs.msg.Pose(position=geometry_msgs.msg.Point(1.0, 1.0, 0.0)),
            geometry_msgs.msg.Pose(position=geometry_msgs.msg.Point(0.0, 1.0, 1.0)),
            geometry_msgs.msg.Pose(position=geometry_msgs.msg.Point(0.0, 0.0, 1.0))]

        target = geometry_msgs.msg.PoseArray()
        target.poses = [
            geometry_msgs.msg.Pose(position=geometry_msgs.msg.Point(1.0, 1.0, 1.0)),
            geometry_msgs.msg.Pose(position=geometry_msgs.msg.Point(1.0, 1.0, 1.0)),
            geometry_msgs.msg.Pose(position=geometry_msgs.msg.Point(1.0, 1.0, 1.0)),
            geometry_msgs.msg.Pose(position=geometry_msgs.msg.Point(1.0, 1.0, 1.0)),
            geometry_msgs.msg.Pose(position=geometry_msgs.msg.Point(1.0, 1.0, 1.0))]

        current = geometry_msgs.msg.PoseArray()
        current.poses = [
            geometry_msgs.msg.Pose(position=geometry_msgs.msg.Point(0.0, 0.0, 0.0)),
            geometry_msgs.msg.Pose(position=geometry_msgs.msg.Point(0.0, 0.0, 0.0)),
            geometry_msgs.msg.Pose(position=geometry_msgs.msg.Point(0.0, 0.0, 0.0)),
            geometry_msgs.msg.Pose(position=geometry_msgs.msg.Point(0.0, 0.0, 0.0)),
            geometry_msgs.msg.Pose(position=geometry_msgs.msg.Point(0.0, 0.0, 0.0))]

        while not self.wait_for_result:
            self.target_poses.publish(target)
            self.current_poses.publish(current)
            self.event_out.publish('e_start')

        self.assertEqual(type(self.result), type(expected))
        self.assertEqual(len(self.result.poses), len(expected.poses))

    def result_callback(self, msg):
        self.result = msg
        self.wait_for_result = True


if __name__ == '__main__':
    rospy.init_node('naive_controller_test')
    rostest.rosrun(PKG, 'naive_controller_test', TestNaiveController)
