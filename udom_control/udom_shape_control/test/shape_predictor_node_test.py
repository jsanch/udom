#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
Integration test for the 'shape_predictor' node.

"""

import numpy as np
import rospy
import unittest
import rostest
import std_msgs.msg
import geometry_msgs.msg
import udom_modeling_msgs.msg

PKG = 'udom_shape_control'


class TestShapePredictor(unittest.TestCase):
    def setUp(self):
        """
        Sets up the test fixture before exercising it.

        """
        # params
        self.result = None
        self.wait_for_result = None

        # publishers
        self.event_out = rospy.Publisher('~event_out', std_msgs.msg.String, latch=True)
        self.force = rospy.Publisher(
            '~force', std_msgs.msg.Float32MultiArray, queue_size=1)
        self.mesh_in = rospy.Publisher(
            '~mesh_in', udom_modeling_msgs.msg.Mesh, queue_size=1)

        # subscribers
        self.mesh_out = rospy.Subscriber(
            '~mesh_out', udom_modeling_msgs.msg.Mesh, self.result_callback)

    def tearDown(self):
        """
        Deconstructs the test fixture after testing it.

        """
        self.force.unregister()
        self.mesh_in.unregister()
        self.event_out.unregister()
        self.mesh_out.unregister()

    def test_shape_predictor(self):
        """
        Verifies the node's interface is correct.
        Note: this is not a functionality test.

        """
        # The mesh has 152 vertices.
        number_of_nodes = 152

        expected_mesh = udom_modeling_msgs.msg.Mesh()
        expected_mesh.vertices = [
            geometry_msgs.msg.Point(0.0, 0.0, 0.0) for _ in range(number_of_nodes)]
        force = std_msgs.msg.Float32MultiArray()

        force.data = list(np.zeros(number_of_nodes * 3))

        # The mesh has to the same size as in the test model.
        # So we fill it with the same points just to get the right size.
        mesh = udom_modeling_msgs.msg.Mesh()
        mesh.vertices = [
            geometry_msgs.msg.Point(0.0, 0.0, 0.0) for _ in range(number_of_nodes)]

        t_1 = udom_modeling_msgs.msg.MeshTetrahedron([0, 2, 1, 4])
        t_2 = udom_modeling_msgs.msg.MeshTetrahedron([6, 5, 1, 3])
        t_3 = udom_modeling_msgs.msg.MeshTetrahedron([6, 7, 5, 3])
        t_4 = udom_modeling_msgs.msg.MeshTetrahedron([3, 5, 1, 2])
        mesh.tetrahedra = [t_1, t_2, t_3, t_4]

        while not self.wait_for_result:
            self.force.publish(force)
            self.mesh_in.publish(mesh)
            self.event_out.publish('e_start')

        self.assertEqual(type(self.result), type(expected_mesh))
        self.assertEqual(len(self.result.vertices), len(expected_mesh.vertices))

    def result_callback(self, msg):
        self.result = msg
        self.wait_for_result = True


if __name__ == '__main__':
    rospy.init_node('shape_predictor_test')
    rostest.rosrun(PKG, 'shape_predictor_test', TestShapePredictor)
