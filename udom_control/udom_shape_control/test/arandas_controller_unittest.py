#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
Unittest for the arandas_controller_utils.py module.

"""

import numpy as np
import unittest
import rosunit
import geometry_msgs.msg
import udom_shape_control.arandas_controller_utils as utils

PKG = 'udom_shape_control'


class TestArandasController(unittest.TestCase):
    """
    Tests the arandas_controller_utils.py module.

    """
    def test_pose_array_to_matrix(self):
        """
        Tests that the function returns the correct matrix based on a pose array.

        """
        pose_array = geometry_msgs.msg.PoseArray()
        pose_array.poses = [
            geometry_msgs.msg.Pose(position=geometry_msgs.msg.Point(1.0, 0.0, 0.0)),
            geometry_msgs.msg.Pose(position=geometry_msgs.msg.Point(1.0, 1.0, 1.0)),
            geometry_msgs.msg.Pose(position=geometry_msgs.msg.Point(1.0, 1.0, 1.0)),
            geometry_msgs.msg.Pose(position=geometry_msgs.msg.Point(1.0, 1.0, 1.0)),
            geometry_msgs.msg.Pose(position=geometry_msgs.msg.Point(0.0, 0.0, 1.0))]

        desired = np.array([
            [1, 0, 0],
            [1, 1, 1],
            [1, 1, 1],
            [1, 1, 1],
            [0, 0, 1]], dtype=float)

        result = utils.pose_array_to_matrix(pose_array)
        np.testing.assert_array_equal(result, desired)

    def test_matrix_to_pose_array(self):
        """
        Tests that the function returns the correct pose array based on a matrix.

        """
        matrix = np.array([
            [1, 0, 0],
            [1, 1, 1],
            [1, 1, 1],
            [1, 1, 1],
            [0, 0, 1]], dtype=float)

        desired = geometry_msgs.msg.PoseArray()
        desired.poses = [
            geometry_msgs.msg.Pose(position=geometry_msgs.msg.Point(1.0, 0.0, 0.0)),
            geometry_msgs.msg.Pose(position=geometry_msgs.msg.Point(1.0, 1.0, 1.0)),
            geometry_msgs.msg.Pose(position=geometry_msgs.msg.Point(1.0, 1.0, 1.0)),
            geometry_msgs.msg.Pose(position=geometry_msgs.msg.Point(1.0, 1.0, 1.0)),
            geometry_msgs.msg.Pose(position=geometry_msgs.msg.Point(0.0, 0.0, 1.0))]

        result = utils.matrix_to_pose_array(matrix)
        np.testing.assert_array_equal(result, desired)


if __name__ == '__main__':
    rosunit.unitrun(PKG, 'test_arandas_controller', TestArandasController)
