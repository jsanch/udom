<?xml version="1.0"?>
<!-- Note: example launch file. Do not modify! -->
<launch>
  <!-- Set to true if the data has been previously recorded. -->
  <arg name="data_recorded" default="false"/>

  <!-- Global parameters. -->
  <arg name="loop_rate" default="500"/>
  <arg name="wait_for_transform" default="0.1"/>
  <arg name="damping_mass" default="10.0"/>
  <arg name="damping_stiffness" default="0.1"/>
  <arg name="mesh_filename" default="block_hard"/>
  <arg name="contact_nodes" default="[305, 91, 348, 209]"/>
  <arg name="collision_distance" default="0.01"/>
  <arg name="propagate_forces" default="true"/>
  <arg name="propagation_distance" default="0.15"/>
  <arg name="add_gravity" default="false"/>
  <arg name="gravity_vector" default="[0, 0, -9.805665]"/>
  <!-- Mass for the bar hard (in Kg). -->
  <arg name="mass" default="0.4320"/>

  <arg name="reference_frame" default="world"/>
  <arg name="object_frame" default="object"/>
  <arg name="constrained_nodes" default="[
    1, 2, 3, 4, 5, 6, 7, 8, 9, 37, 38, 39, 46, 47, 48, 94, 95, 96, 97, 98, 99, 100, 101,
    102, 109, 110, 111, 115, 116, 117, 148, 149, 150, 175, 176, 177, 178, 179, 180, 181,
    182, 183, 205, 206, 207, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225,
    226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241, 242,
    243, 295, 296, 297, 298, 299, 300, 301, 302, 303, 304, 305, 306, 307, 308, 309, 310,
    311, 312, 313, 314, 315, 316, 317, 318, 340, 341, 342, 364, 365, 366, 535, 536, 537,
    541, 542, 543, 574, 575, 576, 577, 578, 579, 580, 581, 582, 583, 584, 585, 586, 587,
    588, 589, 590, 591, 592, 593, 594, 595, 596, 597, 598, 599, 600, 601, 602, 603, 604,
    605, 606, 607, 608, 609, 610, 611, 612, 613, 614, 615, 616, 617, 618, 619, 620, 621,
    622, 623, 624, 649, 650, 651, 673, 674, 675, 865, 866, 867, 874, 875, 876, 877, 878,
    879, 880, 881, 882, 883, 884, 885, 886, 887, 888, 889, 890, 891, 892, 893, 894, 895,
    896, 897, 898, 899, 900, 901, 902, 903, 904, 905, 906, 907, 908, 909, 910, 911, 912,
    958, 959, 960, 961, 962, 963, 964, 965, 966, 994, 995, 996, 1000, 1001, 1002, 1018,
    1019, 1020, 1030, 1031, 1032, 1051, 1052, 1053, 1063, 1064, 1065]"/>

  <!-- Remap topics to connect components' inputs/outputs. -->
  <!-- Nodal force calculator input(s). -->
  <remap from="/udom_deformation_control/force_transformer/force_out"
         to="/udom_deformation_control/nodal_force_calculator/force_in"/>
  <remap from="/udom_deformation_control/nodal_force_calculator/mesh"
         to="/udom_deformation_control/mesh_fast"/>

  <!-- Topic throttle input(s). -->
  <remap from="/udom_deformation_control/topic_throttle/mesh_in"
         to="/udom_deformation_control/mesh"/>

  <!-- Deformation model input(s). -->
  <remap from="/udom_deformation_control/nodal_force_calculator/force_out"
         to="/udom_deformation_control/deformation_model/force_info"/>

  <!-- Deformation model output(s). -->
  <remap from="/udom_deformation_control/deformation_model/mesh"
         to="/udom_deformation_control/mesh"/>

  <!-- Topic throttle output(s). -->
  <remap from="/udom_deformation_control/topic_throttle/mesh_out"
         to="/udom_deformation_control/mesh_fast"/>

  <!-- Mesh visualizer input(s). -->
  <remap from="/mesh_visualizer/mesh" to="/udom_deformation_control/mesh"/>

  <!-- Points visualizer input(s). -->
  <remap from="/points_visualizer/points_in"
         to="/udom_deformation_control/deformation_model/constrained_nodes_visualization"/>

  <!-- Mesh visualizer output(s). -->
  <remap from="/mesh_visualizer/points" to="/udom_deformation_control/points"/>

  <!-- Points visualizer output(s). -->
  <remap from="/points_visualizer/points_out"
         to="/udom_deformation_control/constrained_points"/>

  <!-- Deformation sensing pipeline. -->
  <group ns="udom_deformation_control">
    <!-- Nodal force calculator. -->
    <include
      file="$(find udom_geometric_transformation)/launch/wrench_to_nodal_forces_example.launch">
      <arg name="loop_rate" default="$(arg loop_rate)"/>
      <arg name="contact_nodes" default="$(arg contact_nodes)"/>
      <arg name="collision_distance" default="$(arg collision_distance)"/>
      <arg name="propagate_forces" default="$(arg propagate_forces)"/>
      <arg name="propagation_distance" default="$(arg propagation_distance)"/>
      <arg name="add_gravity" default="$(arg add_gravity)"/>
      <arg name="gravity_vector" default="$(arg gravity_vector)"/>
      <arg name="mass" default="$(arg mass)"/>
      <arg name="object_frame" default="$(arg object_frame)"/>
      <arg name="reference_frame" default="$(arg reference_frame)"/>
      <arg name="wait_for_transform" default="$(arg wait_for_transform)"/>
    </include>

    <!-- Topic throttle. -->
    <include
      file="$(find udom_topic_tools)/launch/topic_throttle_example.launch">
      <arg name="loop_rate" default="$(arg loop_rate)"/>
    </include>

    <!-- Deformation model. -->
    <include
      file="$(find udom_deformation_modeling)/launch/deformation_model_example.launch">
      <arg name="loop_rate" default="$(arg loop_rate)"/>
      <arg name="damping_mass" default="$(arg damping_mass)"/>
      <arg name="damping_stiffness" default="$(arg damping_stiffness)"/>
      <arg name="mesh_filename" default="$(arg mesh_filename).veg"/>
      <arg name="constrained_nodes" default="$(arg constrained_nodes)"/>
    </include>
  </group>
</launch>
