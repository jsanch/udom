<?xml version="1.0"?>
<!-- Note: example launch file. Do not modify! -->
<launch>
  <!-- Global parameters. -->
  <arg name="loop_rate" default="100"/>
  <arg name="mesh_filename" default="bar_hard.veg"/>
  <arg name="reference_frame" default="test_rig"/>
  <arg name="object_frame" default="object"/>
  <arg name="index_start" default="0"/>
  <arg name="constrained_nodes" default="[
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 132, 133, 134, 180, 181, 182, 216, 217, 218, 219,
    220, 221, 264, 265, 266, 267, 268, 269, 324, 325, 326, 327, 328, 329, 372, 373, 374, 375,
    376, 377, 378, 379, 380, 381, 382, 383, 384, 385, 386, 387, 388, 389, 390, 391, 392]"/>
  <arg name="number_of_nodes" default="152"/>
  <arg name="gripped_nodes" default="[4, 16, 45, 57, 62, 55, 28, 6]"/>
  <arg name="contact_nodes" default="[6, 62]"/>
  <arg name="p_gain" default="0.05"/>
  <arg name="d_gain" default="0.0"/>
  <arg name="i_gain" default="0.0"/>
  <!-- How often to publish the transforms (in milliseconds).-->
  <arg name="tf_period" default="1"/>

  <!-- Remap topics to connect components' inputs/outputs. -->
  <!-- Mesh publisher output(s). -->
  <remap from="/udom_deformation_sensing_full/mesh_publisher_gui/event_out"
         to="/udom_deformation_sensing_full/event_in"/>

  <!-- Mesh shifter input(s). -->
  <remap from="/udom_deformation_sensing_full/mesh_shifter/mesh_in"
         to="/udom_deformation_sensing_full/mesh_publisher_gui/mesh"/>
  <remap from="/cartesian_offset_gui/cartesian_offset"
       to="/udom_deformation_sensing_full/mesh_shifter/offset"/>

  <!-- Position based force calculator input(s). -->
  <remap from="/udom_deformation_sensing_full/mesh_shifter/mesh_out"
         to="/udom_deformation_sensing_full/position_based_force_calculator/mesh_position"/>
  <remap from="/udom_deformation_sensing_full/position_based_force_calculator/mesh"
         to="/udom_deformation_sensing_full/mesh"/>

  <!-- Force adder input(s). -->
  <remap from="/udom_deformation_sensing_full/position_based_force_calculator/forces"
         to="/udom_deformation_sensing_full/force_adder/forces_1"/>
  <remap from="/nodal_forces_mock_up_gui/force_array"
         to="/udom_deformation_sensing_full/force_adder/forces_2"/>

  <!-- Deformation model input(s). -->
  <remap from="/udom_deformation_sensing_full/force_adder/force_out"
         to="/udom_deformation_sensing_full/deformation_model/force_info"/>

  <!-- Deformation model output(s). -->
  <remap from="/udom_deformation_sensing_full/deformation_model/mesh"
         to="/udom_deformation_sensing_full/mesh"/>

  <!-- Mesh visualizer input(s). -->
  <remap from="/mesh_visualizer/mesh" to="/udom_deformation_sensing_full/mesh"/>

  <!-- Points visualizer input(s). -->
  <remap from="/points_visualizer/points_in"
         to="/udom_deformation_sensing_full/deformation_model/constrained_nodes_visualization"/>

  <!-- Mesh visualizer output(s). -->
  <remap from="/mesh_visualizer/points" to="/udom_deformation_sensing_full/points"/>

  <!-- Constrained nodes visualizer output(s). -->
  <remap from="/points_visualizer/points_out"
         to="/udom_deformation_sensing_full/constrained_points"/>

  <!-- Contact nodes visualizer output(s). -->
  <remap from="/contact_nodes_visualizer/points_out"
         to="/udom_deformation_sensing_full/contact_nodes"/>

  <!-- Deformation sensing pipeline. -->
  <group ns="udom_deformation_sensing_full">
    <!-- Mesh publisher. -->
    <include
      file="$(find udom_visualization)/launch/mesh_publisher_gui_example.launch">
      <arg name="loop_rate" default="$(arg loop_rate)"/>
    </include>

    <!-- Mesh shifter. -->
    <include
      file="$(find udom_geometric_transformation)/launch/mesh_shifter_example.launch">
      <arg name="loop_rate" default="$(arg loop_rate)"/>
      <arg name="contact_nodes" default="$(arg gripped_nodes)"/>
    </include>

    <!-- Position based force calculator. -->
    <include
      file="$(find udom_geometric_transformation)/launch/position_based_force_calculator_example.launch">
      <arg name="loop_rate" default="$(arg loop_rate)"/>
      <arg name="contact_nodes" default="$(arg gripped_nodes)"/>
      <arg name="p_gain" default="$(arg p_gain)"/>
      <arg name="d_gain" default="$(arg d_gain)"/>
      <arg name="i_gain" default="$(arg i_gain)"/>
    </include>

    <!-- Force adder. -->
    <include file="$(find udom_topic_tools)/launch/force_adder_example.launch">
      <arg name="loop_rate" default="$(arg loop_rate)"/>
      <arg name="number_of_nodes" default="$(arg number_of_nodes)"/>
    </include>

    <!-- Deformation model. -->
    <include
      file="$(find udom_deformation_modeling)/launch/neo_hookean_model_example.launch">
      <arg name="loop_rate" default="$(arg loop_rate)"/>
      <arg name="mesh_filename" default="$(arg mesh_filename)"/>
      <arg name="constrained_nodes" default="$(arg constrained_nodes)"/>
      <arg name="index_start" default="$(arg index_start)"/>
    </include>

    <!-- Coordinator. -->
    <node pkg="udom_deformation_sensing" type="coordinator_kamal_approach_node"
          name="coordinator" output="screen">

      <!-- Node cycle rate (in Hz). -->
      <param name="loop_rate" type="double" value="$(arg loop_rate)"/>

      <!-- Remap topics to interface with the coordinator. -->
      <remap from="~event_in" to="/udom_deformation_sensing_full/event_in"/>
      <remap from="~event_out" to="/udom_deformation_sensing_full/event_out"/>

      <!-- Remap topics to trigger components. -->
      <remap from="~start_mesh_shifter"
             to="/udom_deformation_sensing_full/mesh_shifter/event_in"/>
      <remap from="~start_position_based_force_calculator"
             to="/udom_deformation_sensing_full/position_based_force_calculator/event_in"/>
      <remap from="~start_force_adder"
             to="/udom_deformation_sensing_full/force_adder/event_in"/>
      <remap from="~start_deformation_model"
             to="/udom_deformation_sensing_full/deformation_model/event_in"/>

      <!-- Remap topics to get feedback from components. -->
      <remap from="~mesh_shifter_status"
             to="/udom_deformation_sensing_full/mesh_shifter/event_out"/>
      <remap from="~position_based_force_calculator_status"
             to="/udom_deformation_sensing_full/position_based_force_calculator/event_out"/>
      <remap from="~force_adder_status"
             to="/udom_deformation_sensing_full/force_adder/event_out"/>
      <remap from="~deformation_model_status"
             to="/udom_deformation_sensing_full/deformation_model/event_out"/>
    </node>
  </group>

  <!-- GUI nodes. -->
  <include
    file="$(find udom_visualization)/launch/cartesian_offset_gui_example.launch">
    <arg name="loop_rate" default="$(arg loop_rate)"/>
  </include>

  <!-- Nodal forces mock-up. -->
  <include
    file="$(find udom_visualization)/launch/nodal_forces_mock_up_example.launch">
    <arg name="loop_rate" default="$(arg loop_rate)"/>
    <arg name="number_of_nodes" default="$(arg number_of_nodes)"/>
    <arg name="contact_nodes" default="$(arg contact_nodes)"/>
  </include>

  <!-- Visualization nodes. -->
  <!-- Contact nodes in white.
   TODO: needs points extractor!
   -->
  <include
    file="$(find udom_visualization)/launch/points_visualizer_example.launch">
    <arg name="node_name" default="contact_nodes_visualizer"/>
    <arg name="loop_rate" default="$(arg loop_rate)"/>
    <arg name="points_frame" default="$(arg object_frame)"/>
    <arg name="scale" default="[0.005, 0.005, 0.005]"/>
    <arg name="color" default="[1.0, 1.0, 1.0, 0.9]"/>
  </include>

  <!-- Gripped nodes in blue. -->
  <node pkg="ucr_experiment_evaluation" type="mesh_to_point_cloud"
        name="mesh_to_point_cloud_node" output="screen">
    <!-- Node cycle rate (in Hz). -->
    <param name="loop_rate" type="double" value="$(arg loop_rate)"/>

    <!-- The point cloud will be described with respect to this frame. -->
    <param name="reference_frame" type="str" value="$(arg reference_frame)"/>

    <!-- Nodes to extract. -->
    <rosparam param="nodes" subst_value="True"> [] </rosparam>

    <remap from="~mesh_in"
           to="/udom_deformation_sensing_full/position_based_force_calculator/mesh_position"/>
    <remap from="~point_cloud_out" to="/udom_deformation_sensing_full/gripped_nodes"/>
  </node>

  <!-- Constrained nodes in red. -->
  <include
    file="$(find udom_visualization)/launch/points_visualizer_example.launch">
    <arg name="loop_rate" default="$(arg loop_rate)"/>
    <arg name="points_frame" default="$(arg reference_frame)"/>
    <arg name="scale" default="[0.005, 0.005, 0.005]"/>
    <arg name="color" default="[1.0, 0.0, 0.0, 0.9]"/>
  </include>

  <!-- Mesh visualizer (current). -->
  <node pkg="ucr_experiment_evaluation" type="tet_mesh_to_mesh"
        name="tet_mesh_to_mesh_node" output="screen">
    <!-- Node cycle rate (in Hz). -->
    <param name="loop_rate" type="double" value="$(arg loop_rate)"/>

    <!-- Transparency of the mesh (0.0 is fully transparent). -->
    <param name="alpha" type="double" value="1.0"/>

    <!-- Mesh color in RGB. -->
    <param name="r_color" type="double" value="0.0"/>
    <param name="g_color" type="double" value="1.0"/>
    <param name="b_color" type="double" value="0.0"/>

    <remap from="~mesh_in" to="/udom_deformation_sensing_full/mesh"/>
    <remap from="~mesh_out" to="/udom_deformation_sensing_full/mesh_surface"/>
  </node>

  <!-- Add missing transforms (x y z yaw pitch roll). -->
  <!-- Object to the reference frame. -->
  <node pkg="tf" type="static_transform_publisher" name="object_frame_broadcaster"
        args="0.0 0.0 0.0 0 0 0 $(arg reference_frame) $(arg object_frame)
        $(arg tf_period)"/>
</launch>
